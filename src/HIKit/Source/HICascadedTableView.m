//
//  HICascadeTableView.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 12/28/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

// HIKit
#import "HIKit/HICascadedTableView.h"
#import "HIKit/HIHorizontalTableView.h"
#import "HIKit/HIKitUICommon.h"
#import "HIKit/UITableViewAdditions.h"
#import "HIKit/UITableViewCellAdditions.h"
#import "HIKit/HIAnimationManager.h"
#import "HIKit/HIDirectionPanGestureRecognizer.h"

// Additions
#import "UIView+Helpers.h"
#import "NSObject+Supersequent.h"

//define negative infinity
#define MINFLOAT -MAXFLOAT
static const CGFloat kZetaBounce = 0.68;
static const CGFloat kOmegaBounce = 12;
static const CGFloat kZetaMove = 1.4;
static const CGFloat kOmegaMove = 22;

static const CGFloat kDisplacementRatio = 0.5364;
static const CGFloat kNegativeVisibilityThreshold = 20.0;

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation HITransparentView

///////////////////////////////////////////////////////////////////////////////////////////////////
-(id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) addSubview:(UIView *)view {
    //do nothing
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    return NO;
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation HITransparentTableViewCell  

///////////////////////////////////////////////////////////////////////////////////////////////////
-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) addSubview:(UIView *)view {
    //do nothing
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    return NO;
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PRIVATE INTERFACE

@interface HICascadedTableView () <UIGestureRecognizerDelegate> {

    @private
    struct {
        unsigned int actualDelegateWillDisplayCell:1;
        unsigned int actualDelegateScrollViewDidScroll:1;
        unsigned int actualDelegateWidthForRow:1;
        unsigned int actualDelegateHeightForRow:1;
        unsigned int actualDelegateDidEndDragging:1;
        unsigned int actualDelegateWillBeginDragging:1;
        unsigned int actualDelegateDidEndDecelerating:1;
        unsigned int cascadedDelegateWillAddCellToBackground:1;
        unsigned int cascadedDelegateWillReloadRows:1;
    } _cascadedTableFlags;
    
    struct {
        CGPoint targetContentOffset;
        NSTimeInterval initialTime;
        double A;
        double B;
        double zeta;
        double omega;
        double beta;
        double omegaD;
        double rootpositive;
        double rootnegative;
        NSTimeInterval finalTime;
    } _params;


    HICascadedTableViewStaticBackgroundView* _staticBackgroundView;

    NSInteger _numCells;
    NSInteger _thresholdsCount;
    CGFloat* _thresholds;
    CGFloat* _cellLengths;
    CGFloat* _positiveVisibilityOffsets;
    CGFloat* _negativeVisibilityOffsets;
    NSInteger _numPositiveVisibilityOffsets;
    NSInteger _numNegativeVisibilityOffsets;
    CGFloat _minThreshold;
    CGFloat _maxThreshold;

    NSInteger _numStaticCells;
    NSInteger _numTransparentCells;
    
    CGPoint _dragStartContentOffset;
    CADisplayLink* _bounceDisplayLink;
    
    UIInterfaceOrientation _interfaceOrientation;
    BOOL _disableUpdatesToContentOffset;
}

@property (weak, nonatomic,readonly) id<UITableViewDelegate> actualDelegate;
@property (weak, nonatomic,readonly) id<HICascadedTableViewDataSource> actualDataSource;
@property (weak, nonatomic,readonly) HICascadedTableViewStaticBackgroundView* staticBackgroundView;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell*) transparentCellForRowAtIndexPath:(NSIndexPath*)indexPath;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) initialize;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) handleCustomPan:(UIPanGestureRecognizer*)panGestureRecognizer;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) underDampedScroll:(CADisplayLink*)displayLink;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) criticallyDampedScroll:(CADisplayLink*)displayLink;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) overDampedScroll:(CADisplayLink*)displayLink;
///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)scrollToContentOffset:(CGPoint)contentOffset 
         withInitialVelocity:(CGPoint)initialVelocity 
                        zeta:(CGFloat)zeta 
                       omega:(CGFloat)omega 
             maxOscillations:(CGFloat)maxOscillations;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) bounceToContentOffset:(CGPoint)contentOffset withInitialVelocity:(CGPoint)initialVelocity;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) moveToContentOffset:(CGPoint)contentOffset withInitialVelocity:(CGPoint)initialVelocity;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) invalidateCurrentScroll;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) calculate;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) calculateCellLengths;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) calculateThresholds;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) calculateVisibilityOffsets;
///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)calculateTransparentCellAdjustments;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) updateStaticBackgroundViewIfNeeded;
///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) updateTransparentCellsIfNeeded;
///////////////////////////////////////////////////////////////////////////////////////////////////
-(CGPoint) nextOffsetForOffset:(CGPoint)offset inDirection:(BOOL)positiveDirection;
@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - IMPLEMENTATION

@implementation HICascadedTableView

@synthesize staticCellOffset = _numTransparentCells;
@synthesize actualDelegate = _actualDelegate;
@synthesize cascadeDelegate = _cascadeDelegate;
@synthesize actualDataSource = _actualDataSource;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
	self = [super initWithFrame:frame style:style];
	if (self) {

        [self initialize];

        self.scrollStyle = UITableViewScrollStyleCascaded;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.orientation = UITableViewOrientationVertical;
        
        //disables the built-in pan gesture and add a custom one
        for (UIGestureRecognizer *gesture in self.gestureRecognizers){
            if ([gesture isKindOfClass:[UIPanGestureRecognizer class]]){
                gesture.enabled = NO;
            }
        }
        
        UIPanGestureRecognizer *customPanGestureRecognizer = 
        [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleCustomPan:)];
        [self addGestureRecognizer:customPanGestureRecognizer];
        
        [HIAnimationManager sharedManager];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) dealloc {
    
    [self invalidateCurrentScroll];
    
    FREE_SAFELY(_thresholds);
    FREE_SAFELY(_cellLengths);
    FREE_SAFELY(_positiveVisibilityOffsets);
    FREE_SAFELY(_negativeVisibilityOffsets);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Properties

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setHeaderViewVisibilityRatio:(CGFloat)headerViewVisibilityRatio {

    [self.staticBackgroundView setHeaderViewVisibilityRatio:headerViewVisibilityRatio];
    [self calculateVisibilityOffsets];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(CGFloat) headerViewVisibilityRatio {

    return [self.staticBackgroundView headerViewVisibilityRatio];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setDelegate:(id<UITableViewDelegate>)del {

    _actualDelegate = del;

    //set the callback flags
    _cascadedTableFlags.actualDelegateWillDisplayCell = [_actualDelegate respondsToSelector:@selector(tableView:willDisplayCell:forRowAtIndexPath:)];
    _cascadedTableFlags.actualDelegateScrollViewDidScroll = [_actualDelegate respondsToSelector:@selector(scrollViewDidScroll:)];
    _cascadedTableFlags.actualDelegateHeightForRow = [_actualDelegate respondsToSelector:@selector(tableView:heightForRowAtIndexPath:)];
    _cascadedTableFlags.actualDelegateWillBeginDragging = [_actualDelegate respondsToSelector:@selector(scrollViewWillBeginDragging:)];
    _cascadedTableFlags.actualDelegateDidEndDragging = [_actualDelegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:)];
    _cascadedTableFlags.actualDelegateDidEndDecelerating = [_actualDelegate respondsToSelector:@selector(scrollViewDidEndDecelerating:)];

    [super setDelegate:(del)?(id<UITableViewDelegate>)self:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setCascadeDelegate:(id<HICascadedTableViewDelegate>)cascadeDelegate {
    _cascadeDelegate = cascadeDelegate;
    
    _cascadedTableFlags.cascadedDelegateWillAddCellToBackground = 
    [_cascadeDelegate respondsToSelector:@selector(tableView:willAddCellToBackground:forRowAtIndexPath:)];
    
    _cascadedTableFlags.cascadedDelegateWillReloadRows = 
    [_cascadeDelegate respondsToSelector:@selector(tableView:willReloadRowsAtIndexPaths:)];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setDataSource:(id<HICascadedTableViewDataSource>)dataSource {

    _actualDataSource = dataSource;
    [super setDataSource:(dataSource)?(id<UITableViewDataSource>)self:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setTableHeaderView:(UIView *)tableHeaderView {

    if (tableHeaderView) {
        //Add a transparent view in place of the actual header view
        HITransparentView* transparentHeaderView = [[HITransparentView alloc] initWithFrame:tableHeaderView.frame];
        [transparentHeaderView setBackgroundColor:[UIColor clearColor]];
        [super setTableHeaderView:transparentHeaderView];

    } else {
        [super setTableHeaderView:nil];
    }
    
    if([tableHeaderView respondsToSelector:@selector(addGestureRecognizer:)]){
        HIDirectionPanGestureRecognizer* panGestureRecognizer = [[HIDirectionPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleCustomPan:)];
        [panGestureRecognizer setDelegate:self];
        [panGestureRecognizer setDirection:HIDirectionPanGestureRecognizerHorizontal];
        [panGestureRecognizer addTarget:self action:@selector(handleCustomPan:)];
        [tableHeaderView addGestureRecognizer:panGestureRecognizer];
    }

    //Add the header view to the static background view
    [self.staticBackgroundView setHeaderView:tableHeaderView];

    //Reset the visibility offsets
    [self calculateVisibilityOffsets];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIView*) tableHeaderView {
    return self.staticBackgroundView.headerView;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setTableFooterView:(UIView *)tableFooterView {

    //do nothing
    //footer view is not yet supported for cascaded table view
}

///////////////////////////////////////////////////////////////////////////////////////////////////s
-(void) reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation {
    
    if(_cascadedTableFlags.cascadedDelegateWillReloadRows){
        [_cascadeDelegate tableView:self willReloadRowsAtIndexPaths:indexPaths];
    }
    [super reloadRowsAtIndexPaths:indexPaths withRowAnimation:animation];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - NSObject

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)respondsToSelector:(SEL)aSelector
{
    BOOL result;
    BOOL haveSelector = ([UITableView instanceMethodSignatureForSelector:aSelector]!=nil);
    if ([super respondsToSelector:aSelector]) {
        result= YES;

    } else if (protocol_containsSelector(@protocol(UITableViewDataSource),aSelector)) {
        result= [_actualDataSource respondsToSelector:aSelector]||haveSelector;

    } else if (protocol_containsSelector(@protocol(UITableViewDelegate),aSelector)) {
        result= [_actualDelegate respondsToSelector:aSelector]||haveSelector;

    } else
        result= NO;

    return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)forwardInvocation:(NSInvocation *)anInvocation
{
    if (protocol_containsSelector(@protocol(UITableViewDataSource),[anInvocation selector]))
        [anInvocation invokeWithTarget:_actualDataSource];
    else if (protocol_containsSelector(@protocol(UITableViewDelegate),[anInvocation selector]))
        [anInvocation invokeWithTarget:_actualDelegate];
    else
        [super forwardInvocation:anInvocation];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {

    NSMethodSignature* methodSignature = [UITableView instanceMethodSignatureForSelector:aSelector];
    if ((!methodSignature)&&(protocol_containsSelector(@protocol(UITableViewDataSource),aSelector))) {
        methodSignature = [(NSObject*)_actualDataSource methodSignatureForSelector:aSelector];

    } else if ((!methodSignature)&&(protocol_containsSelector(@protocol(UITableViewDelegate),aSelector))) {
        methodSignature = [(NSObject*)_actualDelegate methodSignatureForSelector:aSelector];
    }
    return methodSignature;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableViewDataSource Proxy

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell;
    if ((indexPath.row>=_numTransparentCells)) {
        cell = [_actualDataSource tableView:tableView cellForRowAtIndexPath:indexPath];
    } else {
        cell = [self transparentCellForRowAtIndexPath:indexPath];
    }
    return cell;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath reuse:(BOOL)reuse
{
    UITableViewCell* cell;
    if ((indexPath.row>=_numTransparentCells)) {
            cell = [_actualDataSource tableView:tableView cellForRowAtIndexPath:indexPath reuse:reuse];
    } else {
        cell = [self transparentCellForRowAtIndexPath:indexPath];
    }
    return cell;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    return nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {

    return nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UITableViewDelegate Proxy

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    return nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {

    return nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_cascadedTableFlags.actualDelegateHeightForRow)
        return [_actualDelegate tableView:tableView heightForRowAtIndexPath:indexPath];
    else
        return kDefaultRowHeight;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    [self updateTransparentCellsIfNeeded];

    if (_cascadedTableFlags.actualDelegateScrollViewDidScroll) {
        [_actualDelegate scrollViewDidScroll:scrollView];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)calculateTransparentCellAdjustments {
    
    CGFloat contentOffsetHeight = self.contentOffset.y;
    CGFloat headerAdjustment = self.staticBackgroundView.headerView.height - self.staticBackgroundView.headerOffset;
    
#ifdef DEBUGTRANSPARENTLENGTHS
    DPRINT(@"ContentOffset - %f",contentOffsetHeight);
    DPRINT(@"Num Transparent Cells - %d",_numTransparentCells);
    DPRINT(@"Lower Threshold - %f",_thresholds[_numTransparentCells]);
    DPRINT(@"Adjusted Lower Threshold - %f",_thresholds[_numTransparentCells]+headerAdjustment);
    DPRINT(@"Upper Threshold - %f",_thresholds[_numTransparentCells+1]);
    DPRINT(@"Adjusted Upper Threshold - %f\n\n",_thresholds[_numTransparentCells+1]+headerAdjustment);
#endif
    
    while ((contentOffsetHeight>=_thresholds[_numTransparentCells+1]+headerAdjustment)&&(_numTransparentCells<_numCells)) {
        _numTransparentCells++;
    }
    
    while ((contentOffsetHeight<=_thresholds[_numTransparentCells]+headerAdjustment)&&(_numTransparentCells>0)) {
        _numTransparentCells--;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(BOOL) updateTransparentCellsIfNeeded {
    
    CGFloat initialNumTransparentCells = _numTransparentCells;
    [self calculateTransparentCellAdjustments];
    if (_numTransparentCells!=initialNumTransparentCells) {

        NSMutableArray* indexPaths=[NSMutableArray arrayWithCapacity:abs(_numTransparentCells-initialNumTransparentCells)];
        for (int index=MIN(_numTransparentCells,initialNumTransparentCells);index<MAX(_numTransparentCells,initialNumTransparentCells);index++)
            [indexPaths addObject:[NSIndexPath indexPathForRow:index inSection:0]];
        [self reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
        return YES;

    } else
        return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)didMoveToSuperview {
    [super didMoveToSuperview];

    [self.staticBackgroundView setFrame:self.frame];
    [[self superview] addSubview:self.staticBackgroundView];
    [[self superview] bringSubviewToFront:self];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) layoutSubviews{

    [super layoutSubviews];
    [self updateStaticBackgroundViewIfNeeded];
    
    if(_interfaceOrientation!=HIInterfaceOrientation()){
        
        //Recalculate
        [self calculate];
        
        //Bounce to make the current cell completely visible
        CGPoint nextContentOffset = [self nextOffsetForOffset:self.contentOffset inDirection:YES];
        [self bounceToContentOffset:nextContentOffset withInitialVelocity:CGPointMake(0,0)];
        _interfaceOrientation=HIInterfaceOrientation();
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableView

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setContentOffset:(CGPoint)contentOffset {
    if(!_disableUpdatesToContentOffset) {
        [super setContentOffset:contentOffset];
    }    
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) reloadData {
    [self calculate];
    
    //Content Offsets are disabled as the call to super tries to change the contentoffset
    //which can cause unpredictable behaviour
    _disableUpdatesToContentOffset = YES;
    [super reloadData];
    _disableUpdatesToContentOffset = NO;
    
    //Reset the number of transparent cells and recalculate it
    _numTransparentCells = 0;
    [self calculateTransparentCellAdjustments];
    
    //Adjust the offset if its not valid and we are not already bouncing
    if(!_bounceDisplayLink) {
        
        if(self.contentOffset.y<_minThreshold) {
            
            [self setContentOffset:CGPointMake(self.contentOffset.x,_minThreshold)];
        } else if (self.contentOffset.y>_maxThreshold) {

            if(_numCells==1) {
                CGFloat headerAdjustment = 
                self.staticBackgroundView.headerView.height - self.staticBackgroundView.headerOffset;
                [self setContentOffset:CGPointMake(self.contentOffset.x,_maxThreshold+headerAdjustment)];
            } else {
                [self setContentOffset:CGPointMake(self.contentOffset.x,_maxThreshold)];
            }
        }       
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated {
    
    if (!animated) {
        [super setContentOffset:contentOffset animated:NO];
    } else {
        [self moveToContentOffset:contentOffset withInitialVelocity:CGPointMake(0,0)];
    }    
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PRIVATE METHODS

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)transparentCellForRowAtIndexPath:(NSIndexPath*)indexPath {

    static NSString *CellIdentifier = @"TransparentCell";
    HITransparentTableViewCell *cell = [self dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[HITransparentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    return cell;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) initialize {

    _numTransparentCells=0;
    _numStaticCells=0;

    _numCells=0;
    _thresholdsCount=0;
    _actualDelegate=nil;
    _actualDataSource=nil;
    _thresholds=nil;
    _cellLengths=nil;
    _positiveVisibilityOffsets=nil;
    _negativeVisibilityOffsets=nil;
    
    _interfaceOrientation = HIInterfaceOrientation();
    _disableUpdatesToContentOffset = NO;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)handleCustomPan:(UIPanGestureRecognizer*)panGestureRecognizer {
    
    switch (panGestureRecognizer.state) {
        
        case UIGestureRecognizerStateBegan:
        {    
            [self invalidateCurrentScroll];
            _dragStartContentOffset = self.contentOffset;
            break;
        }
            
        case UIGestureRecognizerStateChanged:
        {   
            CGPoint currentContentOffset = self.contentOffset;
            if((currentContentOffset.y==_dragStartContentOffset.y)
               &&(_cascadedTableFlags.actualDelegateWillBeginDragging)) {
                [_actualDelegate scrollViewWillBeginDragging:self];
            }
            
            CGPoint translation = [panGestureRecognizer translationInView:self.staticBackgroundView];
            CGFloat targetContentOffsetY = _dragStartContentOffset.y-(translation.y*kDisplacementRatio);
            [self setContentOffset:CGPointMake(currentContentOffset.x,targetContentOffsetY)];
            break;
        } 
            
        case UIGestureRecognizerStateEnded:
        {     
            CGPoint currentContentOffset = self.contentOffset;
            if((currentContentOffset.y!=_dragStartContentOffset.y)&&
               (_cascadedTableFlags.actualDelegateDidEndDragging)) 
            {
                [_actualDelegate scrollViewDidEndDragging:self willDecelerate:NO];
            }
                
            CGPoint velocity = [panGestureRecognizer velocityInView:self];
            CGPoint nextContentOffset = [self nextOffsetForOffset:self.contentOffset inDirection:(velocity.y<0)];
            [self bounceToContentOffset:nextContentOffset withInitialVelocity:velocity];
            break;
        }    
        default:
            break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Display Link Methods

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) invalidateCurrentScroll {
    
    if (_bounceDisplayLink) {
        [_bounceDisplayLink invalidate];
        _bounceDisplayLink = nil;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [[HIAnimationManager sharedManager] setAnimating:NO];
        });
       
        if(_cascadedTableFlags.actualDelegateDidEndDecelerating){
            [_actualDelegate scrollViewDidEndDecelerating:self];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) moveToContentOffset:(CGPoint)contentOffset withInitialVelocity:(CGPoint)initialVelocity {
    [self scrollToContentOffset:contentOffset withInitialVelocity:initialVelocity zeta:kZetaMove omega:kOmegaMove maxOscillations:1.0];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) bounceToContentOffset:(CGPoint)contentOffset withInitialVelocity:(CGPoint)initialVelocity {
    [self scrollToContentOffset:contentOffset withInitialVelocity:initialVelocity zeta:kZetaBounce omega:kOmegaBounce maxOscillations:3.0];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)scrollToContentOffset:(CGPoint)contentOffset withInitialVelocity:(CGPoint)initialVelocity zeta:(CGFloat)zeta omega:(CGFloat)omega maxOscillations:(CGFloat)maxOscillations{
    
    [self invalidateCurrentScroll];
    
    _params.targetContentOffset = contentOffset;
    
    double initialPosition = self.contentOffset.y-_params.targetContentOffset.y;
    
    if(initialPosition==0)
        return;
    
    _params.zeta = zeta;
    _params.omega = omega;
    
    if((zeta>=0)&&(zeta<1)){
        //under-damping
        _params.beta = sqrt(1 - _params.zeta * _params.zeta);
        _params.omegaD = _params.beta*_params.omega;
        _params.A = initialPosition;
        _params.B = ((_params.zeta*_params.omega*initialPosition) - initialVelocity.y)/_params.omegaD;
        _params.finalTime = (atan(_params.B/_params.A)+maxOscillations*M_PI_2)/_params.omegaD;
        _bounceDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(underDampedScroll:)];
    }else if(zeta==1){
        //critical damping
        _params.A = initialPosition;
        _params.B = _params.omega*initialPosition - initialVelocity.y;
        _bounceDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(criticallyDampedScroll:)];
    } else if(zeta>1){
        //over damping
        _params.beta = sqrt(_params.zeta * _params.zeta-1);
        _params.rootpositive = (-_params.zeta+_params.beta)*_params.omega;
        _params.rootnegative = (-_params.zeta-_params.beta)*_params.omega;
        _params.A=initialPosition+(((_params.rootpositive*initialPosition)+initialVelocity.y)/(_params.rootnegative-_params.rootpositive));
        _params.B=-(((_params.rootpositive*initialPosition)+initialVelocity.y)/(_params.rootnegative-_params.rootpositive));
        _bounceDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(overDampedScroll:)];
    }
    
    _params.initialTime = 0;
    [[HIAnimationManager sharedManager] setAnimating:YES];
    [_bounceDisplayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) criticallyDampedScroll:(CADisplayLink*)displayLink {
    
    if (_params.initialTime==0) {
        _params.initialTime = [displayLink timestamp];
        return;
    }
    NSTimeInterval time = [displayLink timestamp]-_params.initialTime;
    double exponentialFactor = exp(-_params.omega* time);
    double result = exponentialFactor * (_params.A  + _params.B*time);
    CGFloat nextPosition = result+_params.targetContentOffset.y;
        
    static const CGFloat kEpsilon = 0.00001f;
    CGFloat currentPosition = self.contentOffset.y;
    if(fabs(nextPosition-currentPosition)<kEpsilon) {
        [self setContentOffset:CGPointMake(self.contentOffset.x,_params.targetContentOffset.y)];
        [self invalidateCurrentScroll];
    } else {
        [self setContentOffset:CGPointMake(self.contentOffset.x,nextPosition)];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) overDampedScroll:(CADisplayLink*)displayLink {
    
    if (_params.initialTime==0) {
        _params.initialTime = [displayLink timestamp];
        return;
    }
    NSTimeInterval time = [displayLink timestamp]-_params.initialTime;
    double result = _params.A*exp(_params.rootpositive*time)+_params.B*exp(_params.rootnegative*time);
    CGFloat nextPosition = result+_params.targetContentOffset.y;
        
    static const CGFloat kEpsilon = 0.00001f;
    CGFloat currentPosition = self.contentOffset.y;
    if(fabs(nextPosition-currentPosition)<kEpsilon) {
        [self setContentOffset:CGPointMake(self.contentOffset.x,_params.targetContentOffset.y)];
        [self invalidateCurrentScroll];
    } else {
        [self setContentOffset:CGPointMake(self.contentOffset.x,nextPosition)];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) underDampedScroll:(CADisplayLink*)displayLink {
    
    if (_params.initialTime==0) {
        _params.initialTime = [displayLink timestamp];
        return;
    }
    
    //Perform calculations
    NSTimeInterval time = [displayLink timestamp]-_params.initialTime;
    double exponentialFactor = exp(-_params.zeta * _params.omega* time);
	double result = exponentialFactor * (_params.A * cos(_params.omegaD * time) + _params.B * sin(_params.omegaD * time));
    CGFloat nextPosition = result+_params.targetContentOffset.y;
    
    if(time>=_params.finalTime) {
        [self setContentOffset:CGPointMake(self.contentOffset.x,_params.targetContentOffset.y)];
        [self invalidateCurrentScroll];
    } else {
        [self setContentOffset:CGPointMake(self.contentOffset.x,nextPosition)];
    }    
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(CGPoint) nextOffsetForOffset:(CGPoint)offset inDirection:(BOOL)positiveDirection {

    CGFloat nextOffset;
    
    //If the offset is negative then unconditionally return 0
    if (offset.y<0) {
        return CGPointMake(offset.x,0);
    }
    
    if (positiveDirection) {

        //Move to the next possible visibility offset so that the next cell is completely visible
        nextOffset = _maxThreshold;
        for (int index=0;index<_numPositiveVisibilityOffsets;index++) {
            if (_positiveVisibilityOffsets[index]>offset.y) {
                nextOffset = MIN(_maxThreshold,_positiveVisibilityOffsets[index]);
                break;
            }
        }

    } else {
        
        //Move to the previous possible visibility offset so that the previous cell is completely visible
        nextOffset = _minThreshold;
        for(int index=1;index<_numNegativeVisibilityOffsets;index++) {
            if (_negativeVisibilityOffsets[index]>offset.y) {
                nextOffset = MAX(_minThreshold,_negativeVisibilityOffsets[index-1]);
                break;
            }
        }
    }

#ifdef DEBUGOFFSETS
    if(positiveDirection) {
        DPRINT(@"Next Offset for offset %f in positive direction is %f",offset.y,nextOffset);
    } else {
        DPRINT(@"Next Offset for offset %f in negative direction is %f",offset.y,nextOffset);
    }
#endif
    
    return CGPointMake(0,nextOffset);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Getter Overrides

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIView*) staticBackgroundView {

    if (!_staticBackgroundView) {
        _staticBackgroundView = [[HICascadedTableViewStaticBackgroundView alloc] initWithFrame:self.frame];
        _staticBackgroundView.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [[self superview] addSubview:_staticBackgroundView];
        [[self superview] bringSubviewToFront:self];
        
        
        UIPanGestureRecognizer* panGestureRecognizer = 
        [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleCustomPan:)];
        [_staticBackgroundView addGestureRecognizer:panGestureRecognizer];
        [panGestureRecognizer setDelegate:self];
        [panGestureRecognizer setCancelsTouchesInView:YES];
        [panGestureRecognizer setDelaysTouchesBegan:YES];
        [panGestureRecognizer setDelaysTouchesEnded:YES];
    }
    return _staticBackgroundView;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private Methods

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) calculate {

    [self calculateCellLengths];
    [self calculateVisibilityOffsets];
    [self calculateThresholds];

#ifdef DEBUGTHRESHOLDS
    DPRINT(@"The number of cells are %d",_numCells);
    DPRINT(@"The Cell Lengths are %@",NSStringFromCFloatArray(_cellLengths,_numCells));
    DPRINT(@"The positive visibility offsets are %@",NSStringFromCFloatArray(_positiveVisibilityOffsets,_numPositiveVisibilityOffsets));
    DPRINT(@"The negative visibility offsets are %@",NSStringFromCFloatArray(_negativeVisibilityOffsets,_numNegativeVisibilityOffsets));
    DPRINT(@"Min Threshold is %f,Max threshold is %f",_minThreshold,_maxThreshold);
    DPRINT(@"Threshold count is - %d",_thresholdsCount);
    DPRINT(@"The thresholds are %@",NSStringFromCFloatArray(_thresholds,_thresholdsCount));
#endif
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) calculateCellLengths {

    //Free if previously allocated
    if (_cellLengths)FREE_SAFELY(_cellLengths);

    //If delegate or datasource have not been assigned then bail
    if ((!_actualDelegate)||(!_actualDataSource))return;

    _numCells = [_actualDataSource tableView:self numberOfRowsInSection:0];
    _cellLengths = (CGFloat*)malloc((_numCells)*sizeof(CGFloat));
    for (int i=0;i<_numCells;i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        _cellLengths[i] = [self tableView:self heightForRowAtIndexPath:indexPath];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)calculateVisibilityOffsets {

    //Free if previously allocated
    if (_positiveVisibilityOffsets)FREE_SAFELY(_positiveVisibilityOffsets);
    _numPositiveVisibilityOffsets = 0;
    if (_negativeVisibilityOffsets)FREE_SAFELY(_negativeVisibilityOffsets);
    _numNegativeVisibilityOffsets = 0;
    
    if (!_cellLengths)return;

    CGFloat viewLength = (self.orientation==UITableViewOrientationVertical)?self.height:self.width;
    CGFloat headerLength = self.tableHeaderView?self.tableHeaderView.height:0;
    CGFloat effectiveViewLength = viewLength-headerLength;
    CGFloat headerOffset = roundf(headerLength*self.headerViewVisibilityRatio);
    
    CGFloat totalPositiveLength = 0;
    _positiveVisibilityOffsets = (CGFloat*)malloc((_numCells)*sizeof(CGFloat));
    for (int i=0;i<_numCells;i++) {
        totalPositiveLength+=_cellLengths[i];
        _positiveVisibilityOffsets[i] = totalPositiveLength - effectiveViewLength;
    }
    _numPositiveVisibilityOffsets = _numCells;
    
    CGFloat totalNegativeLength = 0;
    int pointer = 0;
    _negativeVisibilityOffsets = (CGFloat*)malloc((_numCells*2)*sizeof(CGFloat));
    for (int i=0;i<_numCells;i++) {
        totalNegativeLength+=_cellLengths[i];
        _negativeVisibilityOffsets[pointer++] = totalNegativeLength - effectiveViewLength;
        
        //For special considerations while going negative insert a middle value
        if(i>0&&i==_numCells-1){
            CGFloat negativeVisibility = _cellLengths[i]+_cellLengths[i-1]+headerOffset-viewLength;
            if(negativeVisibility>kNegativeVisibilityThreshold){
                pointer = pointer-1;
                CGFloat offset = _negativeVisibilityOffsets[pointer];
                _negativeVisibilityOffsets[pointer++] = offset-negativeVisibility+1;
                _negativeVisibilityOffsets[pointer++] = offset;
            }
        }
    }
    _numNegativeVisibilityOffsets = pointer;

    _minThreshold = 0;
    _maxThreshold = (totalPositiveLength+headerLength>viewLength)?(totalPositiveLength+headerLength-viewLength):0;
    
#ifdef DEBUGLENGTHS
    DPRINT(@"ViewLength - %f,Header Length - %f,Effective Length - %f",viewLength,headerLength,effectiveViewLength);
#endif
}



///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) calculateThresholds {

    //Free previous allocated threshold Data
    if (_thresholds)FREE_SAFELY(_thresholds);

    if (!_cellLengths)return;

    if (_numCells<=1) {

        _thresholdsCount = 2;
        _thresholds = (CGFloat*)malloc((2)*sizeof(CGFloat));
        _thresholds[0]=MINFLOAT;
        _thresholds[1]=MAXFLOAT;

    } else {

        _thresholdsCount = _numCells+3;
        _thresholds = (CGFloat*)malloc((_thresholdsCount)*sizeof(CGFloat));
        //Now create the thresholds array
        _thresholds[0]=MINFLOAT;
        _thresholds[1]=0;
        for (int i=2;i<_thresholdsCount;i++) {
            _thresholds[i]=_cellLengths[i-2]+_thresholds[i-1];
        }
        _thresholds[_thresholdsCount-1]=MAXFLOAT;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) updateStaticBackgroundViewIfNeeded {

    if ((!_actualDelegate)||(!_actualDataSource)||!(_cellLengths))
        return NO;

    if (_numTransparentCells==_numStaticCells)
        return NO;

    if (_numTransparentCells>0) {

        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:_numTransparentCells-1 inSection:0];
        UITableViewCell* cell = [_actualDataSource tableView:self cellForRowAtIndexPath:indexPath reuse:NO];
        [self.staticBackgroundView setCellViewHeight:_cellLengths[_numTransparentCells-1]];
        
        if(_cascadedTableFlags.cascadedDelegateWillAddCellToBackground) {
            [_cascadeDelegate tableView:self willAddCellToBackground:cell forRowAtIndexPath:indexPath];
        }
        [self.staticBackgroundView setCellView:cell];

    } else {

        [self.staticBackgroundView setCellView:nil];
    }

    //Reset the number of static cells
    _numStaticCells=_numTransparentCells;
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    
    //See if the view is in the subviews somewhere
    for (UIView *view in self.subviews) {
        if (!view.hidden&&[view pointInside:[self convertPoint:point toView:view] withEvent:event]) {
            return YES;
        }    
    }
    
    //If the touch is inside the headerview then return NO
    UIView* headerView = [self.staticBackgroundView headerView];    
    if (headerView&&[headerView pointInside:[self convertPoint:point toView:headerView] withEvent:event]){ 
        return NO;
    } 
    
    //If the 
    UIView* cellView = [self.staticBackgroundView cellView];    
    if (cellView&&[cellView pointInside:[self convertPoint:point toView:cellView] withEvent:event]){
        return NO;
    }

    return YES;
}

#pragma mark -
#pragma mark UIGestureRecognizerDelegate

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return TRUE;
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@interface HIHorizontalCascadedTableView () {

    @private
    struct {
        unsigned int delegateWidthForRow:1;
    } _horizontalCascadedTableFlags;

    UIInterfaceOrientation _currentOrientation;
    BOOL _firstTime;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) applyHorizontalTransformationIfNeeded;
///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) applyHorizontalTransformation;
///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) resetHorizontalTransformation;
@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark  -
#pragma mark  HORIZONTAL TABLE IMPLEMENTATION
@implementation HIHorizontalCascadedTableView

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
	self = [super initWithFrame:frame style:style];
	if (self) {
        self.orientation = UITableViewOrientationHorizontal;
        _firstTime = YES;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Public

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setDelegate:(id<HIHorizontalTableViewDelegate>)delegate {

    [super setDelegate:delegate];

    //set the callback flags
    _horizontalCascadedTableFlags.delegateWidthForRow = [self.actualDelegate respondsToSelector:@selector(tableView:widthForRowAtIndexPath:)];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(id<HIHorizontalTableViewDelegate>)delegate {
    return (id<HIHorizontalTableViewDelegate>)[super delegate];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setTableHeaderView:(UIView *)tableHeaderView {

    if (tableHeaderView) {
        //Add a transparent view in place of the actual header view
        HITransparentView* transparentHeaderView = [[HITransparentView alloc] initWithFrame:tableHeaderView.frame];
        tableHeaderView.transform = CGAffineTransformMakeRotation(M_PI/2);
        transparentHeaderView.transform = CGAffineTransformMakeRotation(M_PI/2);
        [super setTableHeaderView:transparentHeaderView];

    } else {
        [super setTableHeaderView:nil];
    }
    
    if([tableHeaderView respondsToSelector:@selector(addGestureRecognizer:)]){
        HIDirectionPanGestureRecognizer* panGestureRecognizer = [[HIDirectionPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleCustomPan:)];
        [panGestureRecognizer setDelegate:self];
        [panGestureRecognizer setDirection:HIDirectionPanGestureRecognizerHorizontal];
        [panGestureRecognizer addTarget:self action:@selector(handleCustomPan:)];
        [tableHeaderView addGestureRecognizer:panGestureRecognizer];
    }

    //Add the header view to the static background view
    [self.staticBackgroundView setHeaderView:tableHeaderView];

    //Reset the visibility offsets
    [self calculateVisibilityOffsets];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIView

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) layoutSubviews {

    [super layoutSubviews];
    [self applyHorizontalTransformationIfNeeded];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableViewDataSource Proxy

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.orientation = UITableViewCellOrientationVertical;
    cell.contentView.transform = CGAffineTransformMakeRotation(M_PI/2);
    return cell;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableViewDataSource Proxy

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_horizontalCascadedTableFlags.delegateWidthForRow)
        return [(id<HIHorizontalTableViewDelegate>)self.actualDelegate tableView:tableView widthForRowAtIndexPath:indexPath];
    else
        return kDefaultRowWidth;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(BOOL)updateStaticBackgroundViewIfNeeded {

    BOOL isUpdated = [super updateStaticBackgroundViewIfNeeded];
    if (isUpdated) {

        UIView* cellView = self.staticBackgroundView.cellView;
        if ([cellView isKindOfClass:[UITableViewCell class]]) {

            UITableViewCell* cell = (UITableViewCell*)cellView;
            cell.orientation = UITableViewCellOrientationVertical;
            cell.contentView.transform = CGAffineTransformMakeRotation(M_PI/2);
        }
    }
    return isUpdated;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Private Methods

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) applyHorizontalTransformationIfNeeded {

    if (_firstTime) {
       
        [self applyHorizontalTransformation];
        _currentOrientation = HIInterfaceOrientation();
        _firstTime = NO;
    
    } else if (HIInterfaceOrientation()!=_currentOrientation) {
        
        [self resetHorizontalTransformation];
        [self applyHorizontalTransformation];
        _currentOrientation = HIInterfaceOrientation();
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) resetHorizontalTransformation {
    
    //Apply reverse transformation
    CGFloat rotationOffset = (self.height-self.width)/2;
    CGAffineTransform translationTransform = CGAffineTransformMakeTranslation(self.transform.tx+rotationOffset,self.transform.ty-rotationOffset);
    self.transform = CGAffineTransformConcat(CGAffineTransformIdentity,translationTransform);
    self.frame = HIRectInvert(self.frame);
    
    //Make sure the frame is same
    self.staticBackgroundView.frame = self.frame;
    
    rotationOffset = (self.staticBackgroundView.height-self.staticBackgroundView.width)/2;
    translationTransform =CGAffineTransformMakeTranslation(self.staticBackgroundView.transform.tx+rotationOffset,self.staticBackgroundView.transform.ty-rotationOffset);
    self.staticBackgroundView.transform = CGAffineTransformConcat(CGAffineTransformIdentity,translationTransform);
    self.staticBackgroundView.frame = HIRectInvert(self.staticBackgroundView.frame);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) applyHorizontalTransformation {
    
    self.frame = HIRectInvert(self.frame);
    CGFloat rotationOffset = (self.width-self.height)/2;
    CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(-M_PI/2);
    CGAffineTransform translationTransform = CGAffineTransformMakeTranslation(self.transform.tx-rotationOffset,self.transform.ty+rotationOffset);
    self.transform = CGAffineTransformConcat(rotationTransform,translationTransform);
    
    //Make sure the frame is same
    self.staticBackgroundView.frame = self.frame;
    
    //Apply transformation on backgroundView
    self.staticBackgroundView.frame = HIRectInvert(self.staticBackgroundView.frame);
    rotationOffset = (self.staticBackgroundView.width-self.staticBackgroundView.height)/2;
    translationTransform = CGAffineTransformMakeTranslation(self.staticBackgroundView.transform.tx-rotationOffset,self.staticBackgroundView.transform.ty+rotationOffset);
    self.staticBackgroundView.transform = CGAffineTransformConcat(rotationTransform,translationTransform);
}


@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark  -
#pragma mark  STATIC BACKGROUND VIEW IMPLEMENTATION

@implementation HICascadedTableViewStaticBackgroundView

@synthesize headerView = _headerView;
@synthesize cellView = _cellView;
@synthesize cellViewHeight = _cellViewHeight;
@synthesize headerViewVisibilityRatio = _headerViewVisibilityRatio;

///////////////////////////////////////////////////////////////////////////////////////////////////
-(id) initWithFrame:(CGRect)frame {
    self=[super initWithFrame:frame];
    if (self) {

        _headerViewVisibilityRatio = 0;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setHeaderViewVisibilityRatio:(CGFloat)headerViewVisibilityRatio {

    if ((headerViewVisibilityRatio!=_headerViewVisibilityRatio&&
        headerViewVisibilityRatio>=0&&
        headerViewVisibilityRatio<=1))
    {
        _headerViewVisibilityRatio = headerViewVisibilityRatio;
        self.cellView.top = self.headerOffset;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setHeaderView:(UIView *)headerView {

    if (_headerView!=headerView) {
        if(_headerView.superview==self)[_headerView removeFromSuperview];        
        _headerView = headerView;
        [self addSubview:_headerView];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setCellView:(UIView *)cellView {

    if (_cellView!=cellView) {
        
        if(_cellView.superview==self)[_cellView removeFromSuperview];        
        _cellView = cellView;
        [self addSubview:_cellView];
        [self setNeedsLayout];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) layoutSubviews {

    [super layoutSubviews];
    _headerView.frame = CGRectMake(0,0,self.boundWidth,_headerView.height);
    _cellView.frame = CGRectMake(0,self.headerOffset,self.boundWidth,self.cellViewHeight);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(CGFloat) headerOffset {

    CGFloat headerOffset;
    headerOffset = _headerView?_headerView.height:0;
    headerOffset *= _headerViewVisibilityRatio;
    return roundf(headerOffset);
}

@end


