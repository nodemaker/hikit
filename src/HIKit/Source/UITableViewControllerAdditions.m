//
//  UITableViewControllerAdditions.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 12/30/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import <objc/runtime.h>

//HIKit
#import "HIKit/UITableViewControllerAdditions.h"
#import "HIKit/HICascadedTableView.h"
#import "HIKit/HIHorizontalTableView.h"

static const char* tableViewOrientationKey = "_tableViewOrientationKey";
static const char* tableViewScrollStyleKey = "_tableViewScrollStyleKey";

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark - PRIVATE INTERFACE
@interface UITableViewController (HICategoryPrivate)
@property (nonatomic,assign) UITableViewOrientation tableViewOrientation;
@property (nonatomic,assign) UITableViewScrollStyle tableViewScrollStyle;
@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark - IMPLEMENTATION

@implementation UITableViewController (HICategory)

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithOrientation:(UITableViewOrientation)orientation {

    self.tableViewOrientation=orientation;
    self=[self initWithStyle:UITableViewStylePlain];
    if (self){
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithStyle:(UITableViewStyle)style orientation:(UITableViewOrientation)orientation {

    self.tableViewOrientation=orientation;
    self=[self initWithStyle:style];
    if (self){
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithScrollStyle:(UITableViewScrollStyle)scrollStyle {

    self.tableViewScrollStyle = scrollStyle;
    self=[self initWithStyle:UITableViewStylePlain];
    if (self) {
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithStyle:(UITableViewStyle)style scrollStyle:(UITableViewScrollStyle)scrollStyle {

    self.tableViewScrollStyle = scrollStyle;
    self=[self initWithStyle:style];
    if (self) {
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithOrientation:(UITableViewOrientation)orientation scrollStyle:(UITableViewScrollStyle)scrollStyle {

    self.tableViewScrollStyle = scrollStyle;
    self=[self initWithOrientation:orientation];
    if (self) {
    }
    return self;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithStyle:(UITableViewStyle)style orientation:(UITableViewOrientation)orientation scrollStyle:(UITableViewScrollStyle)scrollStyle;
{
    self.tableViewScrollStyle = scrollStyle;
    self=[self initWithStyle:style orientation:orientation];
    if (self) {
    }
    return self;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Setters

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setTableViewOrientation:(UITableViewOrientation)tableViewOrientation {

    NSValue *tableViewOrientationValue = [NSNumber numberWithInt:tableViewOrientation];
    objc_setAssociatedObject(self,(void*)tableViewOrientationKey,tableViewOrientationValue,OBJC_ASSOCIATION_RETAIN);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setTableViewScrollStyle:(UITableViewScrollStyle)tableViewScrollStyle {

    NSValue *tableViewScrollStyleValue = [NSNumber numberWithInt:tableViewScrollStyle];
    objc_setAssociatedObject(self,(void*)tableViewScrollStyleKey,tableViewScrollStyleValue,OBJC_ASSOCIATION_RETAIN);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Getters

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UITableViewOrientation)tableViewOrientation {

    UITableViewOrientation tableViewOrientation;
    id tableViewOrientationValue = objc_getAssociatedObject(self,(void*)tableViewOrientationKey);

    if (!tableViewOrientationValue) {

        //default value is vertical
        tableViewOrientation = UITableViewOrientationVertical;
        [self setTableViewOrientation:tableViewOrientation];

    } else {
        tableViewOrientation = [tableViewOrientationValue intValue];
    }
    return tableViewOrientation;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UITableViewScrollStyle)tableViewScrollStyle {

    UITableViewScrollStyle tableViewScrollStyle;
    id tableViewScrollStyleValue = objc_getAssociatedObject(self,(void*)tableViewScrollStyleKey);

    if (!tableViewScrollStyleValue) {

        //default value is scrollStylePlain
        tableViewScrollStyle = UITableViewScrollStylePlain;
        [self setTableViewScrollStyle:tableViewScrollStyle];

    } else {
        tableViewScrollStyle = [tableViewScrollStyleValue intValue];
    }
    return tableViewScrollStyle;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController Overrides

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) loadView {

    if (self.tableViewScrollStyle==UITableViewScrollStyleCascaded) {

        if (self.tableViewOrientation == UITableViewOrientationVertical) {

            [super loadView];
            UITableViewStyle tableViewStyle = [[self valueForKey:@"_tableViewStyle"] intValue];
            UITableView* tableView = [[HICascadedTableView alloc] initWithFrame:self.view.frame style:tableViewStyle];
            tableView.delegate = self;
            tableView.dataSource = self;
            tableView.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            self.view = tableView;

        } else {

            [super loadView];
            UITableViewStyle tableViewStyle = [[self valueForKey:@"_tableViewStyle"] intValue];
            UITableView* tableView = [[HIHorizontalCascadedTableView alloc] initWithFrame:self.view.frame style:tableViewStyle];
            tableView.delegate = self;
            tableView.dataSource = self;
            tableView.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            self.view = tableView;
        }

    } else {

        if (self.tableViewOrientation == UITableViewOrientationVertical) {

	    [super loadView];
	    UITableViewStyle tableViewStyle = [[self valueForKey:@"_tableViewStyle"] intValue];
	    UITableView* tableView = [[UITableView alloc] initWithFrame:self.view.frame style:tableViewStyle];
	    tableView.delegate = self;
	    tableView.dataSource = self;
	    tableView.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	    self.view = tableView;

        } else {

            [super loadView];
            UITableViewStyle tableViewStyle = [[self valueForKey:@"_tableViewStyle"] intValue];
            UITableView* tableView = [[HIHorizontalTableView alloc] initWithFrame:self.view.frame style:tableViewStyle];
            tableView.delegate = self;
            tableView.dataSource = self;
            tableView.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            self.view = tableView;
        }
    }
}

@end
