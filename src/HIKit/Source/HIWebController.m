//
//  HIWebController.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 1/9/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

//Quartz
#import <QuartzCore/QuartzCore.h>

//MobileCoreServices
#import <MobileCoreServices/MobileCoreServices.h>

//HIKit
#import "HIKit/HIWebController.h"
#import "HIKit/HIKitUICommon.h"
#import "HIKit/UIViewControllerAdditions.h"

//Additions
#import "UIView+Helpers.h"
#import "UIToolbar+ReplaceItem.h"

//Animations
#import "HIAnimationManager.h"

static const CGFloat kViewControllerLengthPortraitPad = 600;
static const CGFloat kViewControllerLengthLandscapePad = 840;

static const CGFloat kViewControllerLengthPortraitPhone = 320;
static const CGFloat kViewControllerLengthLandscapePhone = 480;

static const CGFloat kTitleLabelHorizontalInset = 80;
static const CGFloat kTitleLabelVerticalInset = 5;
static const CGFloat kTitleLabelNavigationHeight = 30;
static const CGFloat kTitleLabelNavigationWidth = 230;

@interface HIWebController (){
    struct {
        unsigned int dataSourceNavigationItemForURL:1;
    } _dataSourceFlags;
    
    struct {
        unsigned int delegateShouldStartLoad:1;
        unsigned int delegateDidStartLoad:1;
        unsigned int delegateDidFinishLoad:1;
        unsigned int delegateDidFailLoadWithError:1;
    } _delegateFlags;
    volatile BOOL _goingNext;
    volatile BOOL _goingPrevious;
}
@property (atomic,strong) NSMutableDictionary*          redirectMap;
@property (atomic,strong) NSMutableDictionary*          navigationItemMap;
@property (atomic,strong) NSURL* nextURL;
@property (atomic,strong) NSURL* previousURL;
@property (nonatomic,strong) UIBarButtonItem*           rightButtonItem;
@property (nonatomic,strong) UIActivityIndicatorView*   spinner;
@property (nonatomic,strong) UIBarButtonItem*           activityItem;
@property (nonatomic,strong) NSURL*                     loadingURL;
@property (nonatomic,strong) UIActionSheet*             actionSheet;

@property (strong,nonatomic,readonly) UIWebView*                 webView;
@property (strong,nonatomic,readonly) UILabel*                   titleLabel;
@end

@implementation HIWebNavigationItem
@end

@implementation HIWebController

@synthesize delegate    = _delegate;
@synthesize dataSource = _dataSource;
@synthesize baseURL     = _baseURL;
@synthesize webView     = _webView;
@synthesize bottomBar   = _bottomBar;
@synthesize titleLabel  = _titleLabel;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _loadingURL = nil;
        _goingNext = NO;
        _goingPrevious = NO;
        
        self.hidesBottomBarWhenPushed = YES;
        self.showsNavigationBarInCascadeNavigation = YES;
        self.redirectMap = [[NSMutableDictionary alloc] init];
        self.navigationItemMap = [[NSMutableDictionary alloc] init];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //Just in case dealloc is called before view unloads
    [self.webView loadHTMLString:@"" baseURL:nil];
    self.webView.delegate = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Public

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setDelegate:(id<HIWebControllerDelegate>)delegate{
    _delegate = delegate;
    
    _delegateFlags.delegateShouldStartLoad = [_delegate respondsToSelector:@selector(webController:webView:shouldStartLoadWithRequest:navigationType:)];
    _delegateFlags.delegateDidStartLoad = [_delegate respondsToSelector:@selector(webController:webViewDidStartLoad:)];
    _delegateFlags.delegateDidFinishLoad = [_delegate respondsToSelector:@selector(webController:webViewDidFinishLoad:)];
    _delegateFlags.delegateDidFailLoadWithError = [_delegate respondsToSelector:@selector(webController:webView:didFailLoadWithError:)];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setDataSource:(id<HIWebControllerDataSource>)dataSource{
    if(_dataSource!=dataSource){
        _dataSource = dataSource;
        _dataSourceFlags.dataSourceNavigationItemForURL =
        [_dataSource respondsToSelector:@selector(webController:navigationItemForURL:)];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSURL*)currentURL {
    NSURL* currentURL = _loadingURL ? _loadingURL : self.webView.request.URL;
    if(![[currentURL absoluteString] length]){
        currentURL = _baseURL;
    }
    while(self.redirectMap[currentURL]&&![currentURL isEqual:self.redirectMap[currentURL]]){
        currentURL = self.redirectMap[currentURL];
    }
    return currentURL;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString*)currentTitle{
    NSString* title = [[self navigationItemForURL:self.currentURL] title];
    if(!title){
        title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    }
    return title;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString*)currentIndexedTitle{
    NSString* title;
    HIWebNavigationItem* navigationItem = [self navigationItemForURL:self.currentURL];
    if(navigationItem){
        if(navigationItem.index>0){
            title = [NSString stringWithFormat:@"%d.%@",navigationItem.index,navigationItem.title];
        }else{
            title = navigationItem.title;
        }
    }
    
    if(!title){
        title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    }
    return title;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString*)currentHashtag{
   return [[self navigationItemForURL:self.currentURL] hashtag];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)openURL:(NSURL*)URL {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
        [self openRequest:request];
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)openRequest:(NSURLRequest*)request {
    _baseURL=[request URL];
    HIPerformBlockAfterAnimationsEnd(^{
        self.nextURL = [[self navigationItemForURL:_baseURL] nextURL];
        self.previousURL = [[self navigationItemForURL:_baseURL] previousURL];
        self.title = [[self navigationItemForURL:_baseURL] title];
        
        if (!request.URL.scheme) {
            NSURL* correctedURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",request.URL.absoluteString]];
            [self.webView loadRequest:[NSMutableURLRequest requestWithURL:correctedURL]];
        }else {
            [self.webView loadRequest:request];
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self navigationItemForURL:self.nextURL];
            [self navigationItemForURL:self.previousURL];
        });
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setBottomToolbarTintColor:(UIColor*)color {
    self.bottomBar.tintColor = color;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setTitleFont:(UIFont*)font {
    self.titleLabel.font = font;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setTitle:(NSString *)title {
    [self view];
    if(title==nil||[title isKindOfClass:[NSString class]]){
        self.titleLabel.text = title;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString*)title {
    return self.titleLabel.text;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIViewController

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)loadView {
    
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.titleView = self.titleLabel;
    [self bottomBar];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidUnload {
    [super viewDidUnload];

    _delegate = nil;
    _webView.delegate = nil;
    [_webView stopLoading];
    [_webView loadHTMLString:@"" baseURL:nil];

    _bottomBar = nil;
    _backButton = nil;
    _forwardButton = nil;
    _refreshButton = nil;
    _twitterButton = nil;
    _stopButton = nil;
    _actionButton = nil;
    _activityItem = nil;
    _nextButton = nil;
    _previousButton = nil;
    _webView = nil;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateToolbarWithOrientation:self.interfaceOrientation];
    [self.navigationItem setRightBarButtonItem:_rightButtonItem];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewWillDisappear:(BOOL)animated {
    // If the browser launched the media player, it steals the key window and never gives it
    // back, so this is a way to try and fix that
    [self.view.window makeKeyWindow];
    [super viewWillDisappear:animated];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return HIIsSupportedOrientation(interfaceOrientation);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateToolbarWithOrientation:toInterfaceOrientation];
    
    for (UIScrollView *scroll in [self.webView subviews]) { //we get the scrollview 
        // Make sure it really is a scroll view and reset the zoom scale.
        if ([scroll respondsToSelector:@selector(setZoomScale:)]){
            [scroll setZoomScale:1.0];
            [scroll setContentInset:UIEdgeInsetsMake(0,0,0,0)];
            [scroll setContentOffset:CGPointMake(0,0)];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView*)rotatingFooterView {
    return self.bottomBar;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(CGFloat) viewControllerWidthForOrientation:(UIInterfaceOrientation)orientation isHeaderViewController:(BOOL)isHeaderViewController {
    
    if (isHeaderViewController) {
        return [super viewControllerWidthForOrientation:orientation isHeaderViewController:YES];
        
    } else {
        
        if(HIIsPad()){
            if(UIInterfaceOrientationIsLandscape(orientation)){
                return kViewControllerLengthLandscapePad;
            } else {
                return kViewControllerLengthPortraitPad;
            }
        }else{
            if(UIInterfaceOrientationIsLandscape(orientation)){
                return kViewControllerLengthLandscapePhone;
            } else {
                return kViewControllerLengthPortraitPhone;
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) willBeRemovedFromTableNavigationController{
    HIPerformBlockAfterAnimationsEnd(^{
        [self.webView stopLoading];
        [self.webView loadHTMLString:@"" baseURL:nil];
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIWebViewDelegate

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request
 navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *url = request.URL;
    NSURL* currentURL = self.currentURL;
    
    if (![url.scheme isEqual:@"http"] && ![url.scheme isEqual:@"https"]) {
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
            return NO;
        }
    }
    
    if (_delegateFlags.delegateShouldStartLoad &&
        ![_delegate webController:self webView:webView shouldStartLoadWithRequest:request navigationType:navigationType]) {
            return NO;
    }
    
    //Capture redirects
    if( (![url.absoluteString isEqualToString:@"about:blank"])&&
        (!_goingNext) &&
        (!_goingPrevious) &&
        (currentURL) &&
        (![currentURL.absoluteString isEqualToString:@"about:blank"])&&
        (navigationType==UIWebViewNavigationTypeOther) &&
        (!self.navigationItemMap[url]) &&
        (![self.nextURL isEqual:url]) &&
        (![self.baseURL isEqual:url]) &&
        (![self.previousURL isEqual:url]) &&
        (![url isEqual:self.redirectMap[currentURL]]))
    {
        self.redirectMap[url] =  currentURL;
    }
    
    if(_goingNext||_goingPrevious){
        _goingNext = NO;
        _goingPrevious = NO;
    }
    
    _backButton.enabled = [self.webView canGoBack];
    _forwardButton.enabled = [self.webView canGoForward];
    _nextButton.enabled = self.nextURL!=nil;
    _previousButton.enabled = self.previousURL!=nil;
    
    _loadingURL = url;
    
    return YES;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webViewDidStartLoad:(UIWebView*)webView {
    if (_delegateFlags.delegateDidStartLoad) {
        [_delegate webController:self webViewDidStartLoad:webView];
    }
    
    NSString* title = self.currentIndexedTitle;
    if(title){
        self.title = title;
    }else{
        self.title = NSLocalizedString(@"Loading...", @"");
    }
    
    [self.navigationItem setRightBarButtonItem:_activityItem animated:YES];
    
    [self.bottomBar replaceItemWithTag:3 withItem:_stopButton];
    _backButton.enabled = [self.webView canGoBack];
    _forwardButton.enabled = [self.webView canGoForward];
    _nextButton.enabled = self.nextURL!=nil;
    _previousButton.enabled = self.previousURL!=nil;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webViewDidFinishLoad:(UIWebView*)webView {
    if (_delegateFlags.delegateDidFinishLoad) {
        [_delegate webController:self webViewDidFinishLoad:webView];
    }
    
    _loadingURL = nil;
    self.title = self.currentIndexedTitle;
    
    if (self.navigationItem.rightBarButtonItem == _activityItem) {
        [self.navigationItem setRightBarButtonItem:_rightButtonItem animated:YES];
    }
    
    [self.bottomBar replaceItemWithTag:3 withItem:_refreshButton];

    _backButton.enabled = [self.webView canGoBack];
    _forwardButton.enabled = [self.webView canGoForward];
    _nextButton.enabled = self.nextURL!=nil;
    _previousButton.enabled = self.previousURL!=nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    // Ignore NSURLErrorDomain error -999.
    // Ignore "Fame Load Interrupted" errors. Seen after app store links.
    if ((error.code == NSURLErrorCancelled)||
        (error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"])) {
    
        self.title = self.currentIndexedTitle;
    } else {
        self.title = HIDescriptionForError(error);
    }
    
    if (_delegateFlags.delegateDidFailLoadWithError) {
        [_delegate webController:self webView:self.webView didFailLoadWithError:error];
    }
    
    _loadingURL = nil;
    
    if (self.navigationItem.rightBarButtonItem == _activityItem) {
        [self.navigationItem setRightBarButtonItem:_rightButtonItem animated:YES];
    }
    
    [self.bottomBar replaceItemWithTag:3 withItem:_refreshButton];
    _backButton.enabled = [self.webView canGoBack];
    _forwardButton.enabled = [self.webView canGoForward];
    _nextButton.enabled = self.nextURL!=nil;
    _previousButton.enabled = self.previousURL!=nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Accessors

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIWebView*)webView{
    if(!_webView){
        CGRect webviewFrame = CGRectMake(0, 0, self.view.width, self.view.height - HIToolbarHeight());
        _webView = [[UIWebView alloc] initWithFrame:webviewFrame];
        _webView.delegate = self;
        _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _webView.scalesPageToFit = YES;
        [self.view addSubview:_webView];
    }
    return _webView;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIToolbar*)bottomBar{
    if(!_bottomBar){
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:
                    UIActivityIndicatorViewStyleWhite];
        [_spinner startAnimating];
        _activityItem = [[UIBarButtonItem alloc] initWithCustomView:_spinner];
        
        UIView* blankView = [[UIView alloc] initWithFrame:CGRectMake(0,0,20,20)];
        _rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:blankView];
        
        _backButton =
        [[UIBarButtonItem alloc] initWithImage:HIImageFromBundle(@"backIcon.png")
                                         style:UIBarButtonItemStylePlain
                                        target:self
                                        action:@selector(backAction)];
        _backButton.tag = 2;
        _backButton.enabled = NO;
        _forwardButton =
        [[UIBarButtonItem alloc] initWithImage:HIImageFromBundle(@"forwardIcon.png")
                                         style:UIBarButtonItemStylePlain
                                        target:self
                                        action:@selector(forwardAction)];
        _forwardButton.tag = 1;
        _forwardButton.enabled = NO;
        _refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                          UIBarButtonSystemItemRefresh target:self action:@selector(refreshAction)];
        _refreshButton.tag = 3;
        _stopButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                       UIBarButtonSystemItemStop target:self action:@selector(stopAction)];
        _stopButton.tag = 3;
        _actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                         UIBarButtonSystemItemAction target:self action:@selector(shareAction)];
        
        UIBarItem* space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                            UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        _bottomBar = [[UIToolbar alloc] initWithFrame:
                      CGRectMake(0, self.view.height - HIToolbarHeight(),
                                 self.view.width, HIToolbarHeight())];
        _bottomBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        _bottomBar.tintColor = HIRGBCOLOR(109, 132, 162);
        
        _nextButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                       UIBarButtonSystemItemFastForward target:self action:@selector(nextAction)];
        _nextButton.tag = 4;
        _nextButton.enabled = NO;
        _previousButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                           UIBarButtonSystemItemRewind target:self action:@selector(previousAction)];
        _previousButton.tag = 5;
        _previousButton.enabled = NO;
        _bottomBar.items = @[_backButton,
                             space,
                             _forwardButton,
                             space,
                             _actionButton,
                             space,
                             _refreshButton,
                             space,
                             _nextButton,
                             space,
                             _previousButton];
        
        [self.view addSubview:_bottomBar];
    }
    return _bottomBar;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UILabel*)titleLabel{
    if(!_titleLabel){
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = UITextAlignmentCenter;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
        _titleLabel.shadowOffset = CGSizeMake(0.0, -1.0);
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont boldSystemFontOfSize:12];
        
        if(HIIsPad()){
            _titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        }
        
        if(self.tableNavigationController) {
            self.cascadeNavigationBarTitleViewInset = CGPointMake(kTitleLabelHorizontalInset,kTitleLabelVerticalInset);
        } else {
            _titleLabel.frame = CGRectMake(0,0,kTitleLabelNavigationWidth,kTitleLabelNavigationHeight);
        }
    }
    return _titleLabel;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Private

///////////////////////////////////////////////////////////////////////////////////////////////////
-(HIWebNavigationItem*)navigationItemForURL:(NSURL*)URL{
    
    if(!URL){
        return nil;
    }
    HIWebNavigationItem* navigationItem = nil;
    if(_dataSourceFlags.dataSourceNavigationItemForURL){
        if(self.navigationItemMap[URL]){
            navigationItem = self.navigationItemMap[URL];
        }else {
            navigationItem = [_dataSource webController:self navigationItemForURL:URL];
            if(navigationItem){
                @synchronized(self.navigationItemMap){
                    if(!self.navigationItemMap[URL]){
                        self.navigationItemMap[URL]=navigationItem;
                    }
                }
            }
        }
    }
    DASSERT(navigationItem==nil||[navigationItem isKindOfClass:[HIWebNavigationItem class]]);
    return navigationItem;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)backAction {
    [self.webView goBack];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)forwardAction {
    [self.webView goForward];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)refreshAction {
    if(self.webView.request.URL.absoluteString.length){
        [self.webView reload];
    }else{
        [self openURL:_baseURL];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)stopAction {
    [self.webView stopLoading];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)shareAction {
    if (nil != _actionSheet && [_actionSheet isVisible]) {
        //should only happen on the iPad
        assert(HIIsPad());
        [_actionSheet dismissWithClickedButtonIndex:-1 animated:YES];
        return;
    }
    
    if (nil == _actionSheet) {
        _actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:NSLocalizedString(@"Open in Safari", @""),
                        NSLocalizedString(@"Copy Link", @""),
                        nil];
        
        NSString* title = [self.currentURL absoluteString];
        if(!title||!title.length){
            title = [_baseURL absoluteString];
        }
        [_actionSheet setTitle:title];
        if (HIIsPad()) {
            [_actionSheet showFromBarButtonItem:_actionButton animated:YES];
            
        }  else {
            [_actionSheet showInView: self.view];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)nextAction{
    _goingNext = YES;
    [self.redirectMap removeAllObjects];
    [self.webView stopLoading];
    [self openRequest:[NSMutableURLRequest requestWithURL:self.nextURL]];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)previousAction{
    _goingPrevious = YES;
    [self.redirectMap removeAllObjects];
    [self.webView stopLoading];
    [self openRequest:[NSMutableURLRequest requestWithURL:self.previousURL]];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)updateToolbarWithOrientation:(UIInterfaceOrientation)interfaceOrientation {
    self.bottomBar.height = HIToolbarHeight();
    self.bottomBar.top = self.view.height - self.bottomBar.height;
    _webView.height = self.view.height - self.bottomBar.height;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIActionSheetDelegate

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
        {
            [[UIApplication sharedApplication] openURL:self.currentURL];
            break;
        }
            
        case 1:
        {
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            NSDictionary* item = @{(NSString*)kUTTypeUTF8PlainText: [self.currentURL absoluteString]
                                  ,(NSString*)kUTTypeURL: self.currentURL};
            [pasteboard setItems:@[item]];
            break;
        }
            
        default:
            break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    _actionSheet = nil;
}

@end
