//
//  HIDirectionPanGestureRecognizer.m
//  HIKit
//
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import "HIDirectionPanGestureRecognizer.h"

int const static kHIDirectionPanThreshold = 5;

@implementation HIDirectionPanGestureRecognizer
@synthesize direction = _direction;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [super touchesMoved:touches withEvent:event];
    if (self.state == UIGestureRecognizerStateFailed) return;
    CGPoint nowPoint = [[touches anyObject] locationInView:self.view];
    CGPoint prevPoint = [[touches anyObject] previousLocationInView:self.view];
    _moveX += prevPoint.x - nowPoint.x;
    _moveY += prevPoint.y - nowPoint.y;
    if (!_drag) {
        if (abs(_moveX) > kHIDirectionPanThreshold) {
            if (_direction == HIDirectionPangestureRecognizerVertical) {
                self.state = UIGestureRecognizerStateFailed;
            }else {
                _drag = YES;
            }
        }else if (abs(_moveY) > kHIDirectionPanThreshold) {
            if (_direction == HIDirectionPanGestureRecognizerHorizontal) {
                self.state = UIGestureRecognizerStateFailed;
            }else {
                _drag = YES;
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)reset {
    [super reset];
    _drag = NO;
    _moveX = 0;
    _moveY = 0;
}

@end
