//
//  HISlidingNavigationController.m
//  HIKit
//
//  Created by samyzee on 2/9/13.
//
//

#import "HIKit/HISlidingNavigationController.h"
#import "HIKit/UIViewControllerAdditions.h"

//Additions
#import "UIView+Helpers.h"

@interface HISlidingNavigationController ()

@end

@implementation HISlidingNavigationController

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithLeftHeaderViewController:(UIViewController *)leftHeaderViewController{
    if(self=[super initWithNibName:nil bundle:nil]){
        [self setLeftHeaderViewController:leftHeaderViewController];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) loadView {
    
    [super loadView];
    [self.view setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setLeftHeaderViewController:(UIViewController *)leftHeaderViewController {
    if (leftHeaderViewController!=_leftHeaderViewController) {
        
        if(_leftHeaderViewController) {
            
            [_leftHeaderViewController removeFromParentViewController];
            _leftHeaderViewController.slidingNavigationController = nil;
            _leftHeaderViewController = nil;
            //[self.tableView setTableHeaderView:nil];
        }
        
        if(leftHeaderViewController) {
            
            [self addChildViewController:leftHeaderViewController];
            leftHeaderViewController.slidingNavigationController = self;
            _leftHeaderViewController = leftHeaderViewController;
            
            CGFloat width =
            [leftHeaderViewController headerViewControllerWidthInSlidingNavigationControllerForOrientation:self.interfaceOrientation];
            leftHeaderViewController.view.frame = CGRectMake(0,0,width,self.view.boundHeight);
            leftHeaderViewController.view.autoresizingMask = UIViewAutoresizingNone;
            //[self.tableView setTableHeaderView:headerViewController.view];
        }
    }
}


@end
