//
//  HIHorizontalTableView.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 12/20/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

//HIKit
#import "HIKit/HIHorizontalTableView.h"
#import "HIKit/HIKitUICommon.h"
#import "HIKit/UITableViewAdditions.h"
#import "HIKit/UITableViewCellAdditions.h"

//Additions
#import "UIView+Helpers.h"
#import "NSObject+Supersequent.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@interface HIHorizontalTableView () {
@private
    struct {
        unsigned int actualDelegateWillBeginDragging:1;
        unsigned int actualDelegateDidEndDecelerating:1;
        unsigned int actualDelegateWidthForRow:1;
        unsigned int actualDataSourceTitleForHeaderInSection:1;
        unsigned int actualDataSourceTitleForFooterInSection:1;
        unsigned int headerAndFooterViewsVisible:1;
    } _horizontalTableFlags;
}

@property (weak, nonatomic,readonly) id<HIHorizontalTableViewDelegate> actualDelegate;
@property (weak, nonatomic,readonly) id<UITableViewDataSource> actualDataSource;
@property (nonatomic,assign) UIInterfaceOrientation currentOrientation;
@property (nonatomic,assign) BOOL firstTime;

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) applyHorizontalTransformationIfNeeded;
///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) applyHorizontalTransformation;
///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)resetHorizontalTransformation;
@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation HIHorizontalTableView

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
	self = [super initWithFrame:frame style:style];
	if (self) {
        self.orientation = UITableViewOrientationHorizontal;
        _firstTime = YES;
    }
	return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Accessor Overrides


///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setDelegate:(id<HIHorizontalTableViewDelegate>)del {

    _actualDelegate = del;

    //set the flags
    _horizontalTableFlags.actualDelegateWillBeginDragging = [_actualDelegate respondsToSelector:@selector(scrollViewWillBeginDragging:)];
    _horizontalTableFlags.actualDelegateDidEndDecelerating = [_actualDelegate respondsToSelector:@selector(scrollViewDidEndDecelerating:)];
    _horizontalTableFlags.actualDelegateWidthForRow = [_actualDelegate respondsToSelector:@selector(tableView:widthForRowAtIndexPath:)];

    [super setDelegate:(del)?(id<UITableViewDelegate>)self:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(id<HIHorizontalTableViewDelegate>)delegate {
    return (id<HIHorizontalTableViewDelegate>)[super delegate];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setDataSource:(id<UITableViewDataSource>)dataSource {

    _actualDataSource = dataSource;

    //set the flags
    _horizontalTableFlags.actualDataSourceTitleForHeaderInSection = [_actualDataSource respondsToSelector:@selector(tableView:titleForHeaderInSection:)];
    _horizontalTableFlags.actualDataSourceTitleForFooterInSection = [_actualDataSource respondsToSelector:@selector(tableView:titleForFooterInSection:)];

    [super setDataSource:(dataSource)?(id<UITableViewDataSource>)self:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setTableHeaderView:(UIView *)tableHeaderView {
    tableHeaderView.transform = CGAffineTransformMakeRotation(M_PI/2);
    [super setTableHeaderView:tableHeaderView];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setTableFooterView:(UIView *)tableFooterView {

    tableFooterView.transform = CGAffineTransformMakeRotation(M_PI/2);
    [super setTableHeaderView:tableFooterView];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark NSObject

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)respondsToSelector:(SEL)aSelector
{
    BOOL result;
    BOOL haveSelector = ([HIHorizontalTableView instanceMethodSignatureForSelector:aSelector]!=nil);
    if ([super respondsToSelector:aSelector]) {
        result= YES;

    } else if (protocol_containsSelector(@protocol(UITableViewDataSource),aSelector)) {
        result= [_actualDataSource respondsToSelector:aSelector]||haveSelector;

    } else if (protocol_containsSelector(@protocol(HIHorizontalTableViewDelegate),aSelector)) {
        result= [_actualDelegate respondsToSelector:aSelector]||haveSelector;

    } else
        result= NO;

    return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)forwardInvocation:(NSInvocation *)anInvocation
{
    if (protocol_containsSelector(@protocol(UITableViewDataSource),[anInvocation selector]))
        [anInvocation invokeWithTarget:_actualDataSource];
    else if (protocol_containsSelector(@protocol(HIHorizontalTableViewDelegate),[anInvocation selector]))
        [anInvocation invokeWithTarget:_actualDelegate];
    else
        [super forwardInvocation:anInvocation];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {

    NSMethodSignature* methodSignature = [HIHorizontalTableView instanceMethodSignatureForSelector:aSelector];
    if ((!methodSignature)&&(protocol_containsSelector(@protocol(UITableViewDataSource),aSelector))) {
        methodSignature = [(NSObject*)_actualDataSource methodSignatureForSelector:aSelector];

    } else if ((!methodSignature)&&(protocol_containsSelector(@protocol(HIHorizontalTableViewDelegate),aSelector))) {
        methodSignature = [(NSObject*)_actualDelegate methodSignatureForSelector:aSelector];
    }
    return methodSignature;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UITableViewDataSource Proxy

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [_actualDataSource tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.orientation = UITableViewCellOrientationVertical;
    cell.contentView.transform = CGAffineTransformMakeRotation(M_PI/2);
    return cell;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UITableViewDelegate Proxy

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_horizontalTableFlags.actualDelegateWidthForRow)
        return [(id<HIHorizontalTableViewDelegate>)_actualDelegate tableView:tableView widthForRowAtIndexPath:indexPath];
    else
        return kDefaultRowWidth;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIView

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) layoutSubviews {

    [super layoutSubviews];
    [self applyHorizontalTransformationIfNeeded];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Private Methods

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) applyHorizontalTransformationIfNeeded {
    
    if (_firstTime) {
        
        [self applyHorizontalTransformation];
        _currentOrientation = HIInterfaceOrientation();
        _firstTime = NO;
        
    } else if (HIInterfaceOrientation()!=_currentOrientation) {
            
        [self resetHorizontalTransformation];
        [self applyHorizontalTransformation];
        _currentOrientation = HIInterfaceOrientation();
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) resetHorizontalTransformation {
    
    //Apply reverse transformation
    CGFloat rotationOffset = (self.height-self.width)/2;
    CGAffineTransform translationTransform = CGAffineTransformMakeTranslation(self.transform.tx+rotationOffset,self.transform.ty-rotationOffset);
    CGAffineTransform combinedTransform = CGAffineTransformConcat(CGAffineTransformIdentity,translationTransform);
    self.transform =combinedTransform;
    self.frame = HIRectInvert(self.frame);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) applyHorizontalTransformation {
    
    //Apply transformation
    self.frame = HIRectInvert(self.frame);
    CGFloat rotationOffset = (self.width-self.height)/2;
    CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(-M_PI/2);
    CGAffineTransform translationTransform = CGAffineTransformMakeTranslation(self.transform.tx-rotationOffset,self.transform.ty+rotationOffset);
    CGAffineTransform combinedTransform = CGAffineTransformConcat(rotationTransform,translationTransform);
    self.transform = combinedTransform;
}

@end

