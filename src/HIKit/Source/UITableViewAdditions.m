//
//  UITableViewAdditions.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 1/31/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

#import <objc/runtime.h>

//HIKit
#import "HIKit/UITableViewAdditions.h"

static const char* scrollStyleKey = "_scrollStyleKey";
static const char* orientationKey = "_orientationKey";

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation UITableView (HICategory)

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setOrientation:(UITableViewOrientation)orientation {

    NSValue *orientationValue = [NSNumber numberWithInt:orientation];
    objc_setAssociatedObject(self,(void*)orientationKey,orientationValue,OBJC_ASSOCIATION_RETAIN);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UITableViewOrientation)orientation {

    UITableViewOrientation orientation;
    id orientationValue = objc_getAssociatedObject(self,(void*)orientationKey);

    if (!orientationValue) {

        //default value is vertical
        orientation = UITableViewOrientationVertical;
        [self setOrientation:orientation];

    } else {
        orientation = [orientationValue intValue];
    }
    return orientation;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setScrollStyle:(UITableViewScrollStyle)scrollStyle {

    NSValue *scrollStyleValue = [NSNumber numberWithInt:scrollStyle];
    objc_setAssociatedObject(self,(void*)scrollStyleKey,scrollStyleValue,OBJC_ASSOCIATION_RETAIN);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UITableViewScrollStyle)scrollStyle {

    UITableViewScrollStyle scrollStyle;
    id scrollStyleValue = objc_getAssociatedObject(self,(void*)scrollStyleKey);

    if (!scrollStyleValue) {

        //defaut value is plain
        scrollStyle = UITableViewScrollStylePlain;
        [self setScrollStyle:scrollStyle];

    } else {
        scrollStyle = [scrollStyleValue intValue];
    }
    return scrollStyle;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) scrollToBottom:(BOOL)animated {
    
    NSInteger indexOflastSection = [self numberOfSections]-1;
    NSInteger indexOfLastRow = [self numberOfRowsInSection:indexOflastSection]-1;
    NSIndexPath* lastCellIndexPath = [NSIndexPath indexPathForRow:indexOfLastRow inSection:indexOflastSection];
    [self scrollToRowAtIndexPath:lastCellIndexPath atScrollPosition:UITableViewScrollPositionTop animated:animated];
}

@end
