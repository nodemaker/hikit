//
//  UITableNavigationController.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 1/28/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "HIKit/HITableNavigationController.h"
#import "HIKit/HIKitUICommon.h"
#import "HIKit/HIHorizontalTableView.h"
#import "HIKit/HICascadedTableView.h"
#import "HIKit/UITableViewAdditions.h"
#import "HIKit/UIViewControllerAdditions.h"

//Additions
#import "UIView+Helpers.h"

static const CGFloat kGroupedTableViewInsetX = 10.0;
static const CGFloat kGroupedTableViewInsetY = 10.0;
static const CGFloat kViewTableCellCornerRadius = 6.0;
static const CGFloat kShadowLength = 400.0;
static const CGFloat kShadowInset = 6.0;
static const CGFloat kHeaderViewVisibilityRatioIPad = 0.26;
static const CGFloat kHeaderViewVisibilityRatioIPhone = 0;

static const CGFloat kDetachImageWidth = 49.0f;
static const CGFloat kDetachImageHeight = 78.0f;
static const CGFloat kDetachImageOffset = 20.0;
static const CGFloat kDynamicDetachImageOffset = 15.0; 
static const CGFloat kDetachContentOffset = 120;
static const CGFloat kDynamicImageMovementX = 50;
static const CGFloat kDynamicImageMovementY = 0;
static const CGFloat kDynamicImageRotation = 0.20f;

typedef enum {
    UITableViewCellPositionMiddle=0,
    UITableViewCellPositionTop,
    UITableViewCellPositionBottom,
    UITableViewCellPositionTopAndBottom,
    UITableViewCellPositionNone,
} UITableViewCellPosition;

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@interface HITableNavigationViewCell : UITableViewCell
@property (nonatomic,weak)              UIViewController*                       viewController;

@property (nonatomic,assign)            CGSize                                  cellViewInset;
@property (nonatomic,assign)            UITableViewCellPosition                 position;
@property (nonatomic,assign)            BOOL                                    showBeginningShadow;
@property (nonatomic,assign)            BOOL                                    showEndShadow;

@property (strong, nonatomic,readonly)    UIView*                               navigationCellView;
@property (strong, nonatomic,readonly)    CAGradientLayer*                      beginningDropShadow;
@property (strong, nonatomic,readonly)    CAGradientLayer*                      endDropShadow;
@property (strong, nonatomic,readonly)    UINavigationBar*                      navigationBar;

-(void) setRoundedMaskIfNeeded;
@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - PRIVATE INTERFACE
@interface HITableNavigationController () <HICascadedTableViewDelegate>{
    __strong NSMutableArray* _viewControllers;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSArray*) removeAllViewControllersAfterViewController:(UIViewController*)viewController;
///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) updateDetachImagesIfNeeded;

@property (nonatomic,assign) BOOL detachOffsetOverridden;

@property (weak, nonatomic) UIViewController* activeController;

@property (strong, nonatomic,readonly) UITableView* tableView;
@property (strong, nonatomic,readonly) UIImageView* staticDetachImage;
@property (strong, nonatomic,readonly) UIImageView* dynamicDetachImage;
@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - IMPLEMENTATION
@implementation HITableNavigationController

@synthesize tableNavigationViewStyle  = _tableNavigationViewStyle;
@synthesize viewControllers = _viewControllers;
@synthesize headerViewController = _headerViewController;
@synthesize activeController = _activeController;
@synthesize detachImage = _detachImage;
@synthesize detachEnabled = _detachEnabled;
@synthesize detachOffsetOverridden = _detachOffsetOverridden;
@synthesize tableView = _tableView;
@synthesize dynamicDetachImage = _dynamicDetachImage;
@synthesize staticDetachImage = _staticDetachImage;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _tableNavigationViewStyle = UITableNavigationViewStylePlain;
        _viewControllers = [[NSMutableArray alloc] init];
        _detachEnabled = NO;
    }

    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(id) init {
   return [self initWithNibName:nil bundle:nil];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithStyle:(UITableNavigationViewStyle)style {
	self = [self initWithNibName:nil bundle:nil];
    if (self) {
        _tableNavigationViewStyle = style;
    }

    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithHeaderViewController:(UIViewController *)headerViewController style:(UITableNavigationViewStyle)style {
    self=[self initWithStyle:style];
    if (self) {
        [self setHeaderViewController:headerViewController];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) dealloc {
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public Methods

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)pushViewControllerFromActiveController:(UIViewController *)viewController animated:(BOOL)animated {
    [self pushViewController:viewController fromViewController:self.activeController animated:animated];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {

    //Add the view controller the the end of the array
    if(viewController&&![self.viewControllers containsObject:viewController]){
        
        [_viewControllers addObject:viewController];
        viewController.tableNavigationController = self;
        
        [self.tableView reloadData];
        
        if(animated) {
            [self.tableView scrollToBottom:animated];
        }
        
        [self updateDetachImagesIfNeeded];
        
        [self addChildViewController:viewController];
    }    
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)pushViewController:(UIViewController *)viewController fromViewController:(UIViewController*)existingViewController animated:(BOOL)animated {

    //If existingViewController is in the array then
    if ([self.viewControllers containsObject:existingViewController]) {

        [self removeAllViewControllersAfterViewController:existingViewController];
        [self pushViewController:viewController animated:YES];

    } else if (existingViewController == _headerViewController) {

        [self setRootViewController:viewController];

    } else {

        [self pushViewController:existingViewController animated:animated];
        [self pushViewController:viewController animated:animated];
    }

}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIViewController *)popViewControllerAnimated:(BOOL)animated {

    UIViewController* lastViewController = nil;
    if (_viewControllers.count) {

        lastViewController = [_viewControllers lastObject];
        [_viewControllers removeLastObject];
        
        [self.tableView reloadData];
        
        if(animated) {
            [self.tableView scrollToBottom:animated];
        }
        
        [lastViewController removeFromParentViewController];
    }
    return lastViewController;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSArray *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated {

    NSArray* poppedViewControllers = [self removeAllViewControllersAfterViewController:viewController];

    if (poppedViewControllers&&poppedViewControllers.count){
        [self.tableView reloadData];
    }
    
    if(animated) {
        [self.tableView scrollToBottom:animated];
    }
    
    for(UIViewController* controller in poppedViewControllers){
        [controller removeFromParentViewController];
    }
    
    [self setActiveController:viewController];
    return poppedViewControllers;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIViewController

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) loadView {

    [super loadView];
    [self.view setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
    [self tableView];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIScrollViewDelegate

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self updateDetachImagesIfNeeded];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if(self.viewControllers.count&&(!self.staticDetachImage.hidden)&&(!self.dynamicDetachImage.hidden)){
        self.detachOffsetOverridden = ((-scrollView.contentOffset.y)>kDetachContentOffset);
    }    
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if(self.detachOffsetOverridden) {
        
        [UIView animateWithDuration:0.1f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState 
                        
                        animations:^
                        {
                            CGPoint oldCenter = self.dynamicDetachImage.center;
                            self.dynamicDetachImage.center = CGPointMake(oldCenter.x,
                                                                         oldCenter.y+130);
                            _dynamicDetachImage.transform = CGAffineTransformMakeRotation(1.4f);
                        }
                        
                         completion:^(BOOL finish) 
                        {
                            [self popToViewController:self.rootViewController animated:NO];
                        }];
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableViewDataSource

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _viewControllers.count;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath reuse:(BOOL)reuse    
{
    NSString *CellIdentifier = @"Cell";
    HITableNavigationViewCell *cell = nil;
    
    if(reuse){
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    } else {
        CellIdentifier = nil;
    }
    
    if (cell == nil) {
        cell = [[ HITableNavigationViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if (self.tableNavigationViewStyle==UITableNavigationViewStyleGrouped) {
            cell.cellViewInset = CGSizeMake(kGroupedTableViewInsetX,kGroupedTableViewInsetY);
        }
    }
    
    NSInteger rows = [self.tableView numberOfRowsInSection:indexPath.section];
    //Set Rounded corners for plain and cascaded tables
    if((self.tableNavigationViewStyle==UITableNavigationViewStylePlain)||(self.tableNavigationViewStyle==UITableNavigationViewStyleCascaded)) {
        
        if(rows==1) {
            
            cell.position = UITableViewCellPositionTopAndBottom;
            cell.showBeginningShadow = YES;
            cell.showEndShadow = NO;
            
        } else if (indexPath.row==0) {
            
            cell.position = UITableViewCellPositionTop;  
            cell.showBeginningShadow = YES;
            cell.showEndShadow = YES;
        
        } else if (indexPath.row==rows-1) {
            
            cell.position = UITableViewCellPositionBottom;
            cell.showBeginningShadow = NO;
            cell.showEndShadow = NO;
            
        } else {
            
            cell.position = UITableViewCellPositionMiddle;
            cell.showBeginningShadow = NO;
            cell.showEndShadow = YES;
        }    
    }
    
    //Make sure there is a shadow on the first non static cell
    if(self.tableNavigationViewStyle==UITableNavigationViewStyleCascaded) {
        NSInteger staticOffset = [(HICascadedTableView*)self.tableView staticCellOffset];
        if(indexPath.row==staticOffset)
        {
            cell.showBeginningShadow=YES;
        }
    }
    
    [cell setViewController:_viewControllers[indexPath.row]];
    return cell;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableView:tableView cellForRowAtIndexPath:indexPath reuse:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)tableView:(UITableView *)tableView widthForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController* viewController = _viewControllers[indexPath.row];
    return [viewController viewControllerWidthForOrientation:self.interfaceOrientation isHeaderViewController:NO];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Properties

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableView*)tableView {
    if (nil == _tableView) {
        
        UITableViewStyle tableViewStyle =  (_tableNavigationViewStyle==UITableNavigationViewStyleGrouped)?UITableViewStyleGrouped:UITableViewStylePlain;
        
        if ((_tableNavigationViewStyle==UITableNavigationViewStylePlain)||(_tableNavigationViewStyle==UITableNavigationViewStyleGrouped)) {
            _tableView = [[HIHorizontalTableView alloc] initWithFrame:self.view.bounds style:tableViewStyle];
            
        } else {
            _tableView = [[HIHorizontalCascadedTableView alloc] initWithFrame:self.view.bounds];
            if(HIIsPad()){
                [(HIHorizontalCascadedTableView*)_tableView
                 setHeaderViewVisibilityRatio:kHeaderViewVisibilityRatioIPad];
            }else{
                [(HIHorizontalCascadedTableView*)_tableView
                 setHeaderViewVisibilityRatio:kHeaderViewVisibilityRatioIPhone];
            }
            [(HIHorizontalCascadedTableView*)_tableView setCascadeDelegate:self];
        }
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = (_tableNavigationViewStyle==UITableNavigationViewStyleGrouped);
        _tableView.showsHorizontalScrollIndicator = NO;
        
        if (([self.tableView respondsToSelector:@selector(backgroundView)])&&
            (tableViewStyle==UITableNavigationViewStyleGrouped))
        {
            _tableView.backgroundView = nil;
            [_tableView setBackgroundView:[[UIView alloc] init]];
            [_tableView setBackgroundColor:UIColor.clearColor];
        }
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIImageView*)dynamicDetachImage {
    
    if((!self.detachEnabled)||(!self.detachImage)) {
        [_dynamicDetachImage removeFromSuperview];
        _dynamicDetachImage = nil;
        return nil;
    }    
    
    if (nil==_dynamicDetachImage) {
        
        CGRect staticDetachImageRect = self.staticDetachImage.frame;
        CGRect dynamicDetachImageRect = 
        CGRectOffset(staticDetachImageRect, kDynamicDetachImageOffset, kDynamicDetachImageOffset);
        _dynamicDetachImage = [[UIImageView alloc] initWithFrame:dynamicDetachImageRect];
        [_dynamicDetachImage setAutoresizingMask: UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin];
        [_dynamicDetachImage setImage:self.detachImage];
        [self.view insertSubview:_dynamicDetachImage atIndex:1];
    }
    
    return _dynamicDetachImage;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIImageView*)staticDetachImage {
    
    if((!self.detachEnabled)||(!self.detachImage)) {
        [_staticDetachImage removeFromSuperview];
        _staticDetachImage = nil;
        return nil;
    } 
    
    if(nil==_staticDetachImage) {
        
        CGFloat headerWidth = [self.headerViewController 
                               viewControllerWidthForOrientation:self.interfaceOrientation 
                               isHeaderViewController:YES];
        CGFloat detachImagePosition = headerWidth + kDetachImageOffset;
        CGRect staticDetachImageRect = 
        CGRectMake(detachImagePosition,
                   self.view.boundHeight/2-self.detachImage.size.height/2,
                   kDetachImageWidth, kDetachImageHeight);
        _staticDetachImage = [[UIImageView alloc] initWithFrame:staticDetachImageRect];
        [_staticDetachImage setAutoresizingMask: UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin];
        [_staticDetachImage setImage:self.detachImage];
        [self.view insertSubview:_staticDetachImage atIndex:0];
    }
    return _staticDetachImage;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setDetachOffsetOverridden:(BOOL)detachOffsetOverridden {
    if(detachOffsetOverridden!=_detachOffsetOverridden) {
        _detachOffsetOverridden = detachOffsetOverridden;
        
        if(_detachOffsetOverridden) {
            [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:
             ^ {
                 CGPoint oldCenter = self.dynamicDetachImage.center; 
                 self.dynamicDetachImage.center = CGPointMake(oldCenter.x+kDynamicImageMovementX,
                                                              oldCenter.y+kDynamicImageMovementY);
                 CGAffineTransform transform = 
                 CGAffineTransformMakeRotation(kDynamicImageRotation);
                 _dynamicDetachImage.transform = transform;
                 _dynamicDetachImage.alpha = 0.5;
             } completion:^(BOOL finish) {}];
            
        } else {
            
            [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:
             ^ {
                 CGPoint staticCenter = self.staticDetachImage.center; 
                 self.dynamicDetachImage.center = CGPointMake(staticCenter.x+kDynamicDetachImageOffset,
                                                              staticCenter.y+kDynamicDetachImageOffset);
                 CGAffineTransform transform = 
                 CGAffineTransformMakeRotation(0.0);
                 _dynamicDetachImage.transform = transform;
                 _dynamicDetachImage.alpha = 1.0;
             } completion:^(BOOL finish) {}];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIViewController*) rootViewController {
    return self.viewControllers.count?(self.viewControllers)[0]:nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIViewController*) topViewController {
    return self.viewControllers.count?(self.viewControllers)[self.viewControllers.count-1]:nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setActiveController:(UIViewController *)activeController {
    _activeController = activeController;
    [self controllerBecameActive:activeController];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)controllerBecameActive:(UIViewController*)controller {
    //Default implementation does nothing
    //To be overridden
}


///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setRootViewController:(UIViewController *)rootViewController {

    for (UIViewController* controller in _viewControllers) {
        [controller removeFromParentViewController];
    }
    
    [_viewControllers makeObjectsPerformSelector:@selector(willBeRemovedFromTableNavigationController)];
    [_viewControllers removeAllObjects];
    
    [self pushViewController:rootViewController animated:YES];
    [self setActiveController:rootViewController];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setHeaderViewController:(UIViewController *)headerViewController {
    if (headerViewController!=_headerViewController) {
        
        if(_headerViewController) {
            
            [_headerViewController removeFromParentViewController];
            _headerViewController.tableNavigationController = nil;
            _headerViewController = nil;
            
            [self.tableView setTableHeaderView:nil];
        }
        
        if(headerViewController) {
            
            [self addChildViewController:headerViewController];
            headerViewController.tableNavigationController = self;
            _headerViewController = headerViewController;
            
            CGFloat width = [headerViewController 
                             viewControllerWidthForOrientation:self.interfaceOrientation 
                             isHeaderViewController:YES];
            headerViewController.view.frame = CGRectMake(0,0,width,self.view.boundHeight);
            headerViewController.view.autoresizingMask = UIViewAutoresizingNone;
            [self.tableView setTableHeaderView:headerViewController.view];
        }     
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - HICascadeTableViewDelegate

//////////////////////////////////////////////////////////////////////////////////////////////////
- (void)tableView:(UITableView *)tableView willAddCellToBackground:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HITableNavigationViewCell* navigationCell = (HITableNavigationViewCell*)cell;
    navigationCell.position = UITableViewCellPositionTop;  
    navigationCell.showBeginningShadow = YES;
    navigationCell.showEndShadow = NO;
    
    //Set a beginning shadow on the next cell
    NSIndexPath* nextIndexPath = 
    [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
    HITableNavigationViewCell* nextCell = 
    (HITableNavigationViewCell*)[self.tableView cellForRowAtIndexPath:nextIndexPath];
    [nextCell setShowBeginningShadow:YES];
    [nextCell setNeedsLayout];    
}

//////////////////////////////////////////////////////////////////////////////////////////////////
- (void)tableView:(UITableView *)tableView willReloadRowsAtIndexPaths:(NSArray *)indexPaths {
    
    NSInteger staticOffset = [(HICascadedTableView*)tableView staticCellOffset];
    for (NSIndexPath* indexPath in indexPaths) {
        UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if([cell isKindOfClass:[HITableNavigationViewCell class]]){
            
            HITableNavigationViewCell* navigationCell = (HITableNavigationViewCell*)cell;
            
            [navigationCell setViewController:nil];
            [navigationCell setShowEndShadow:NO];
            [navigationCell setShowBeginningShadow:NO];
            [navigationCell setNeedsLayout];
        
        } 
        
        if(indexPath.row>=staticOffset){
                
            //Remove a beginning shadow on the next cell
            NSIndexPath* nextIndexPath = 
            [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
            HITableNavigationViewCell* nextCell = 
            (HITableNavigationViewCell*)[self.tableView cellForRowAtIndexPath:nextIndexPath];
            if([nextCell isKindOfClass:[HITableNavigationViewCell class]]){
                [nextCell setShowBeginningShadow:NO];
                [nextCell setNeedsLayout];
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIViewController

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return HIIsSupportedOrientation(interfaceOrientation);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    self.tableView.frame = self.view.bounds;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private Methods

///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSArray*) removeAllViewControllersAfterViewController:(UIViewController *)viewController {

    NSArray* removedViewControllers = nil;
    NSInteger index = [_viewControllers indexOfObject:viewController];
    if (index!=NSNotFound) {

        //Add all the view controllers to the poppedViewControllers array and remove them from viewControllers
        NSRange overflowRange = NSMakeRange(index+1,_viewControllers.count-index-1);
        removedViewControllers = [_viewControllers objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:overflowRange]];
        [removedViewControllers makeObjectsPerformSelector:@selector(willBeRemovedFromTableNavigationController)];
        [_viewControllers removeObjectsInRange:overflowRange];
    }
    return removedViewControllers;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) updateDetachImagesIfNeeded {
    
    if(self.detachEnabled&&self.detachImage) {
        
        self.dynamicDetachImage.alpha = 1.0;
        _dynamicDetachImage.transform = CGAffineTransformIdentity;
        _dynamicDetachImage.center = CGPointMake(self.staticDetachImage.center.x+kDynamicDetachImageOffset,
                                                 self.staticDetachImage.center.y+kDynamicDetachImageOffset);
        
        NSInteger rows = [self.tableView numberOfRowsInSection:0];
        self.staticDetachImage.hidden = (rows<=1);
        self.dynamicDetachImage.hidden = (rows<=1);
    }    
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation HITableNavigationViewCell
@synthesize navigationCellView = _navigationCellView;
@synthesize cellViewInset = _cellViewInset;
@synthesize position = _position;
@synthesize viewController = _viewController;
@synthesize showEndShadow = _showEndShadow;
@synthesize showBeginningShadow = _showBeginningShadow;
@synthesize navigationBar = _navigationBar;
@synthesize beginningDropShadow = _beginningDropShadow;
@synthesize endDropShadow = _endDropShadow;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _cellViewInset = CGSizeMake(0, 0);
        _position = UITableViewCellPositionNone;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _showBeginningShadow = NO;
        _showEndShadow = NO;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) dealloc {
    [_navigationBar removeFromSuperview];
    [_navigationCellView removeFromSuperview];    
    [_endDropShadow removeFromSuperlayer];    
    [_beginningDropShadow removeFromSuperlayer];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setViewController:(UIViewController *)viewController{
    
    if (_viewController!=viewController){
        [[self.navigationCellView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)] ;
        _viewController = viewController;
    }   
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) layoutSubviews {
    
    [super layoutSubviews];
    self.navigationCellView.frame = self.contentView.bounds;
    self.beginningDropShadow.frame = CGRectMake(-kShadowLength,0,
                                                kShadowLength+kShadowInset,
                                                self.contentView.boundHeight);
    self.endDropShadow.frame = CGRectMake(self.contentView.boundWidth-kShadowLength,
                                          0,kShadowLength,self.contentView.boundHeight);
    
    if(self.viewController.view.superview!=self.navigationCellView) {
                
        [self.viewController.view removeFromSuperview];
        CGRect insetFrame = CGRectInset(self.navigationCellView.bounds,
                                        self.cellViewInset.width,
                                        self.cellViewInset.height);
        [self.navigationCellView addSubview:self.viewController.view];
        
        if(self.viewController.showsNavigationBarInCascadeNavigation){
            self.navigationBar.frame = CGRectMake(insetFrame.origin.x,insetFrame.origin.y,insetFrame.size.width,HIToolbarHeight());
            self.viewController.view.frame = CGRectMake(insetFrame.origin.y,insetFrame.origin.y+HIToolbarHeight(),
                                                        insetFrame.size.width,insetFrame.size.height-HIToolbarHeight());
            [self.navigationBar popNavigationItemAnimated:NO];
            [self.navigationBar pushNavigationItem:_viewController.navigationItem animated:NO];
            CGPoint titleViewInset = self.viewController.cascadeNavigationBarTitleViewInset;
            [[_viewController.navigationItem titleView] setFrame:
             CGRectInset(self.navigationBar.frame,titleViewInset.x,titleViewInset.y)];
            [self.navigationBar setTintColor:self.viewController.cascadeNavigationBarTintColor];
            [self.navigationCellView addSubview:self.navigationBar];
        }else{            
            self.viewController.view.frame = insetFrame;
            if(_navigationBar){
                [_navigationBar removeFromSuperview];
                _navigationBar = nil;
            }
        }
    }
    
    [self setRoundedMaskIfNeeded];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setRoundedMaskIfNeeded {
    
    //Add a rounded mask to cell wrapper view if needed
    self.navigationCellView.layer.mask =nil;
    if((self.position!=UITableViewCellPositionMiddle)&&(self.position!=UITableViewCellPositionNone))
    {
        UIRectCorner corners;
        
        switch (self.position) {
            case UITableViewCellPositionTop:
                corners = UIRectCornerTopLeft|UIRectCornerBottomLeft;
                break;
                
            case UITableViewCellPositionBottom:
                corners = UIRectCornerBottomRight|UIRectCornerTopRight;
                break;     
                
            case UITableViewCellPositionTopAndBottom:
                corners = UIRectCornerBottomLeft|UIRectCornerBottomRight|UIRectCornerTopLeft|UIRectCornerTopRight;
                break;
        }
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.navigationCellView.bounds 
                                                       byRoundingCorners:corners
                                                             cornerRadii:CGSizeMake(kViewTableCellCornerRadius,
                                                                                    kViewTableCellCornerRadius)];
        // Create the shape layer and set its path
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = self.navigationCellView.bounds;
        maskLayer.path = maskPath.CGPath;
        self.navigationCellView.layer.mask = maskLayer;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIView*) navigationCellView {
    
    if(!self.viewController) {
        [_navigationCellView removeFromSuperview];
        _navigationCellView = nil;
        return nil;
    }
    
    if(nil == _navigationCellView) {
        
        _navigationCellView  =  [[UIView alloc] initWithFrame:self.contentView.bounds];
        _navigationCellView.backgroundColor = [UIColor clearColor];
        _navigationCellView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _navigationCellView.autoresizesSubviews = YES;
        [self.contentView addSubview:_navigationCellView];
    }
    return _navigationCellView;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(CAGradientLayer*)beginningDropShadow {
    
    if(!self.showBeginningShadow) {
        [_beginningDropShadow removeFromSuperlayer];
        _beginningDropShadow = nil;
        return nil;
    }
    
    if(nil==_beginningDropShadow) {
        
        _beginningDropShadow = [CAGradientLayer layer];
        _beginningDropShadow.frame = CGRectMake(-kShadowLength,0,kShadowLength+kShadowInset,self.contentView.boundHeight);
        _beginningDropShadow.shouldRasterize =YES;
        _beginningDropShadow.colors = @[(id)[UIColor colorWithWhite:0.0f alpha:0.5f].CGColor,
                                 (id)[UIColor colorWithWhite:0.0f alpha:0.4f].CGColor,
                                 (id)[UIColor colorWithWhite:0.0f alpha:0.2f].CGColor,
                                 (id)[UIColor colorWithWhite:0.0f alpha:0.1f].CGColor,
                                 (id)[UIColor colorWithWhite:0.0f alpha:0.05f].CGColor,
                                 (id)[UIColor clearColor].CGColor];
        _beginningDropShadow.locations = @[@0.0f,
                                    @0.01f,
                                    @0.04f,
                                    @0.07f,
                                    @0.2f,
                                    @1.0f];
        _beginningDropShadow.endPoint = CGPointMake(0,0.5);
        _beginningDropShadow.startPoint = CGPointMake(1,0.5);
        [self.contentView.layer insertSublayer:_beginningDropShadow atIndex:0]; 
    }
    return _beginningDropShadow;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UINavigationBar*)navigationBar{
    if(nil==_navigationBar){
        _navigationBar = 
        [[UINavigationBar alloc] initWithFrame:CGRectMake(0,0, self.contentView.boundWidth,HIToolbarHeight())];
        _navigationBar.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    }
    return _navigationBar;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(CAGradientLayer*)endDropShadow {
    
    if(!self.showEndShadow) {
        [_endDropShadow removeFromSuperlayer];
        _endDropShadow = nil;
        return nil;
    }
    
    if(nil==_endDropShadow) {
        
        _endDropShadow = [CAGradientLayer layer];
        _endDropShadow.frame = CGRectMake(self.contentView.boundWidth-kShadowLength,0,
                                          kShadowLength,self.contentView.boundHeight);
        _endDropShadow.shouldRasterize =YES;
        _endDropShadow.colors = @[(id)[UIColor colorWithWhite:0.0f alpha:0.5f].CGColor,
                                       (id)[UIColor colorWithWhite:0.0f alpha:0.4f].CGColor,
                                       (id)[UIColor colorWithWhite:0.0f alpha:0.2f].CGColor,
                                       (id)[UIColor colorWithWhite:0.0f alpha:0.1f].CGColor,
                                       (id)[UIColor colorWithWhite:0.0f alpha:0.05f].CGColor,
                                       (id)[UIColor clearColor].CGColor];
        _endDropShadow.locations = @[@0.0f,
                                          @0.01f,
                                          @0.04f,
                                          @0.07f,
                                          @0.2f,
                                          @1.0f];
        _endDropShadow.endPoint = CGPointMake(0,0.5);
        _endDropShadow.startPoint = CGPointMake((kShadowInset+kShadowLength)/kShadowLength,0.5);
        NSInteger layers = [self.contentView.layer.sublayers count];
        [self.contentView.layer insertSublayer:_endDropShadow atIndex:layers]; 
    }
    return _endDropShadow;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setSelected:(BOOL)selected{

}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setHighlighted:(BOOL)highlighted {
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    if(!self.hidden&&self.viewController.tableNavigationController) {
        HITableNavigationController* tableNavigationController =
        (HITableNavigationController*)self.viewController.tableNavigationController;
        [tableNavigationController setActiveController:self.viewController];
    }
    return [super hitTest:point withEvent:event];
}

@end

