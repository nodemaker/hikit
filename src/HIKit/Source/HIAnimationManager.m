//
//  HIAnimationManager.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 12/23/11.
//

#import "HIKitUICommon.h"
#import "HIAnimationManager.h"

NSString *const HIAnimationStartedNotification = @"HIAnimationStarted";
NSString *const HIAnimationEndedNotification = @"HIAnimationEnded";

///////////////////////////////////////////////////////////////////////////////////////////////////
void HIPerformBlockAfterAnimationsEnd(PostAnimationBlock block){
    [[HIAnimationManager sharedManager] performBlockAfterAnimationsEnd:block];
}

@interface HIAnimationManager ()
@property (nonatomic,strong) NSMutableArray* blocks;
@end

@implementation HIAnimationManager
@synthesize animating = _animating;

///////////////////////////////////////////////////////////////////////////////////////////////////
+ (HIAnimationManager*)sharedManager {
    static HIAnimationManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(id)init{
    self = [super init];
    if(self){
        _animating = NO;
        self.blocks = [[NSMutableArray alloc] init];
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)performBlockAfterAnimationsEnd:(PostAnimationBlock)block{
    if(!self.animating){
        if([NSThread isMainThread]){
            block();
        }else{
            dispatch_async(dispatch_get_main_queue(),block);
        }
    }else{
        @synchronized(self.blocks){
            [self.blocks addObject:block];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setAnimating:(BOOL)animating{
    if(_animating!=animating){
        _animating = animating;
        if(_animating){
            [[NSNotificationCenter defaultCenter] postNotificationName:HIAnimationStartedNotification object:nil];
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:HIAnimationEndedNotification object:nil];
            [self executePendingBlocks];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(BOOL)animating{
    return _animating;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)executePendingBlocks{
    
    NSMutableArray* pendingBlocks = [NSMutableArray array];
    @synchronized(self.blocks){
        [pendingBlocks addObjectsFromArray:self.blocks];
        [self.blocks removeAllObjects];
    }
   
    for(int i=0;i<pendingBlocks.count;i++){
        PostAnimationBlock block = pendingBlocks[i];
        dispatch_async(dispatch_get_main_queue(),block);
    }
}

@end
