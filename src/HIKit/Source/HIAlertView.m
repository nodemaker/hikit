//
//  HIAlertView.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 5/4/12.
//  Inspired by UIAlertView,TSAlertView and HIAlertView(https://github.com/eaigner/HIAlertView)
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

#import "HIAlertView.h"
#import "HIKitUICommon.h"
#import "UIView+Helpers.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
@interface HIAlertOverlayWindow:UIWindow
@property (nonatomic,retain) UIWindow* oldKeyWindow;
@end

///////////////////////////////////////////////////////////////////////////////////////////////////
@interface HIAlertViewTextField:UITextField
@end

///////////////////////////////////////////////////////////////////////////////////////////////////
@interface HIAlertViewButton:UIButton
@property (nonatomic,assign) BOOL whitened;
@end

///////////////////////////////////////////////////////////////////////////////////////////////////
@interface HIAlertViewController : UIViewController
@property (nonatomic,assign) BOOL supportsLandscape;
@end

///////////////////////////////////////////////////////////////////////////////////////////////////
@interface HIAlertView () {
    struct 
    {
        CGRect titleRect;
        CGRect subtitleRect;
        CGRect accessoryRect;
        CGRect textFieldsRect;
        CGRect buttonRect;
    } layout;
    
    NSMutableArray* _buttons;
    NSMutableArray* _textFields;
    BOOL _keyboardShown;
    UIView* _accessoryView;
    UIView* _contentView;
    UIWindow* _oldKeyWindow;
    
    struct {
        unsigned int delegateClickedButtonAtIndex:1;
        unsigned int delegateWillPresentAlertView:1;
        unsigned int delegateDidPresentAlertView:1;
        unsigned int delegatewillDismissWithButtonIndex:1;
        unsigned int delegateDidDismissWithButtonIndex:1;
    } _flags;
}
-(void) _initialize;
-(void) dismissWithButtonIndex:(NSInteger)index;
-(void) drawDialogBackgroundInRect:(CGRect)rect;
-(void) drawSymbolInRect:(CGRect)rect;
-(void) drawButtonInRect:(CGRect)rect title:(NSString *)title whitened:(BOOL)whitened down:(BOOL)down;
-(void) drawTitleInRect:(CGRect)rect isSubtitle:(BOOL)isSubtitle;
-(UIView *) makeAccessoryView;
-(void)layoutComponents;
@property (nonatomic,assign)   BOOL keyboardShown;
@property (nonatomic,retain) UIView* contentView;
@property (nonatomic,retain) UIWindow* oldKeywindow;
@property (nonatomic,readonly) NSMutableArray* buttons;
@property (nonatomic,readonly) NSMutableArray* textFields;
@end

static const CGFloat kHIAlertViewAnimationDuration =0.15;
static const CGFloat kHIAlertViewPopScale = 0.5;
static const CGFloat kHIAlertViewPadding = 8.0;
static const CGFloat kHIAlertViewFrameInset = 8.0;
static const CGFloat kHIAlertViewgButtonHeight = 44.0;
static const CGFloat kHIAlertViewTextFieldHeight = 29.0;
static const CGFloat kHIAlertViewBackgroundCircleInset = 30;
static const CGFloat kDefaultAlertViewWidth = 290.0f;
static const CGFloat kDefaultAlertViewHeight = 158.0f;


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation HIAlertView
@synthesize title = _title;
@synthesize message = _message;
@synthesize delegate = _delegate;
@synthesize oldKeywindow = _oldKeyWindow;
@synthesize alertViewStyle = _alertViewStyle;
@synthesize keyboardShown = _keyboardShown;
@synthesize titleFont=_titleFont;
@synthesize borderColor=_borderColor;
@synthesize textColor=_textColor;
@synthesize tintColor=_tintColor;
@synthesize customView=_customView;
@synthesize contentView=_contentView;
@synthesize accessoryView = _accessoryView;
@synthesize subtitleFont = _subtitleFont;
@synthesize supportsLandscape = _supportsLandscape;
@synthesize backgroundPatternImage = _backgroundPatternImage;
@synthesize completionBlock = _completionBlock;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id) init 
{   
    self = [super init];
	if (self)
	{
		[self _initialize];
	}
	return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
	if (self)
	{
		[self _initialize];
	}
	return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id) initWithTitle: (NSString *)title message:(NSString *)message delegate:(id)delegate {
	
    self = [super init];
    if (self) // will call into initWithFrame, thus _initialize is called
	{
		self.title = title;
		self.message = message;
		self.delegate = delegate;
    }
	return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithTitle:(NSString *)title message:(NSString *)message onCompletion:(CompletionBlock)completion {
    self = [super init];
    if (self) // will call into initWithFrame, thus _initialize is called
	{
		self.title = title;
		self.message = message;
		self.completionBlock = completion;
    }
	return self;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)_initialize{
    // Register for keyboard notifications
    
    self.titleFont = [UIFont boldSystemFontOfSize:18.0];
    self.textColor = [UIColor whiteColor];
    self.borderColor = [UIColor whiteColor];
    self.subtitleFont = [UIFont systemFontOfSize:16.0];
    self.tintColor =  [UIColor colorWithRed:0.047 green:0.141 blue:0.329 alpha:1.0];
    self.supportsLandscape = YES;
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(onKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(onKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)dealloc {
    
    
    RELEASE_SAFELY(_title);
    RELEASE_SAFELY(_message);
    RELEASE_SAFELY(_buttons);
    RELEASE_SAFELY(_textFields);
    RELEASE_SAFELY(_titleFont);
    RELEASE_SAFELY(_subtitleFont);
    RELEASE_SAFELY(_accessoryView);
    RELEASE_SAFELY(_customView);
    RELEASE_SAFELY(_contentView);
    RELEASE_SAFELY(_oldKeyWindow);
    RELEASE_SAFELY(_tintColor);
    RELEASE_SAFELY(_backgroundPatterImage);

#if !__has_feature(objc_arc)
    Block_release(_completionBlock);
#endif
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)addButtonWithTitle:(NSString *)title {
    return [self addButtonWithTitle:title whitened:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)addButtonWithTitle:(NSString *)title whitened:(BOOL)whitened {    
    
    HIAlertViewButton *button = [HIAlertViewButton buttonWithType:UIButtonTypeCustom];
    button.whitened = whitened;
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttons addObject:button];
    return [self.buttons indexOfObject:button];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)addTextFieldWithPlaceholder:(NSString *)placeholder secure:(BOOL)secure {
    
    for (UITextField *field in self.textFields) {
        field.returnKeyType = UIReturnKeyNext;
    }
    
    for (UITextField *field in self.textFields) {
        field.returnKeyType = UIReturnKeyNext;
    }
    
    HIAlertViewTextField *field = [[HIAlertViewTextField alloc] initWithFrame:CGRectMake(0, 0, 200, kHIAlertViewTextFieldHeight)];
    field.returnKeyType = UIReturnKeyDone;
    field.placeholder = placeholder;
    field.secureTextEntry = secure;
    field.font = [UIFont systemFontOfSize:kHIAlertViewTextFieldHeight - 12.0];
    field.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    field.textColor = [UIColor blackColor];
    field.textAlignment = UITextAlignmentCenter;
    field.keyboardAppearance = UIKeyboardAppearanceAlert;
    field.delegate = (id)self;
    
    [self.textFields addObject:field];
    return [self.textFields indexOfObject:field];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSString *)textForTextFieldAtIndex:(NSUInteger)index {
    UITextField *field = [self.textFields objectAtIndex:index];
    return [field text];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSString *)buttonTitleAtIndex:(NSInteger)index {
    UIButton* button = [self.buttons objectAtIndex:index];
    return [button titleForState:UIControlStateNormal];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfButtons{
    return self.buttons.count;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSInteger)numberOfTextFields{
    return self.textFields.count;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) removeAllButtons {
    [self.buttons removeAllObjects];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) removeAllTextFields {
    [self.textFields removeAllObjects];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIButton*)buttonAtIndex:(NSInteger)buttonIndex
{
    // avoid a NSRange exception
    if (buttonIndex < 0 || buttonIndex >= [self.buttons count])
        return nil;
    
	return [self.buttons objectAtIndex:buttonIndex];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITextField*)textFieldAtIndex:(NSInteger)index {
    
    //avoid a NSRange Exception
    if (index < 0 || index >= [self.textFields count])
        return nil;
    
    return [self.textFields objectAtIndex:index];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)show {
    
    
    if(_flags.delegateWillPresentAlertView){
        [(id<HIAlertViewDelegate>)_delegate willPresentAlertView:self];
    }
    
    NSAssert(dispatch_get_current_queue() == dispatch_get_main_queue(), 
             @"%@ must be called on main queue", NSStringFromSelector(_cmd));
    [[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode beforeDate:[NSDate date]];
    self.oldKeywindow = [[UIApplication sharedApplication] keyWindow];
    
    HIAlertViewController* viewController = [[[HIAlertViewController alloc] init] autorelease];
	viewController.view.backgroundColor = [UIColor clearColor];
    viewController.supportsLandscape = self.supportsLandscape;
    
    HIAlertOverlayWindow* overlayWindow = [[HIAlertOverlayWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
	overlayWindow.alpha = 0.0;
    overlayWindow.opaque = NO;
    overlayWindow.windowLevel = UIWindowLevelAlert;
	overlayWindow.backgroundColor = [UIColor clearColor];
	overlayWindow.rootViewController = viewController;
	[overlayWindow makeKeyAndVisible];
    
    // fade in the window
	[UIView animateWithDuration: 0.2 animations: ^{
		overlayWindow.alpha = 1;
	}];
    
    //Add self to the view and pulse
    [viewController.view addSubview: self];
    self.bounds = CGRectMake(0,0,kDefaultAlertViewWidth,kDefaultAlertViewHeight);
	self.center = CGPointMake(CGRectGetMidX(viewController.view.bounds), CGRectGetMidY(viewController.view.bounds ) );;
	self.frame = CGRectIntegral( self.frame );
        
    self.backgroundColor = [UIColor clearColor];
    
    [self layoutComponents];
	
    //show pulsing animation
    self.transform = CGAffineTransformMakeScale(0.6, 0.6);
	[UIView animateWithDuration: 0.2 
					 animations: ^{
						 self.transform = CGAffineTransformMakeScale(1.1, 1.1);
					 }
					 completion: ^(BOOL finished1){
						 [UIView animateWithDuration:1.0/15.0
										  animations: ^{
											  self.transform = CGAffineTransformMakeScale(0.9, 0.9);
										  }
										  completion: ^(BOOL finished2){
											  [UIView animateWithDuration:1.0/7.5
															   animations: ^{
																   self.transform = CGAffineTransformIdentity;
															   }
                                                               completion:^(BOOL finished3){
                                                                   if(_flags.delegateDidPresentAlertView){
                                                                       [(id<HIAlertViewDelegate>)_delegate didPresentAlertView:self];
                                                                   }
                                                               }];
										  }];
					 }];
	
}
    
///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView *)makeAccessoryView {
    if (self.alertViewStyle == HIAlertViewStyleActivity) {
        UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activityView startAnimating];
        
        return activityView;
    } else if (self.alertViewStyle == HIAlertViewStyleProgress) {
        UIProgressView *progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
        progressView.frame = CGRectMake(0, 0, 200.0, 88.0);
        
        return progressView;
    } else if (self.alertViewStyle == HIAlertViewStyleError||
               self.alertViewStyle == HIAlertViewStyleSuccess) {
        CGSize iconSize = CGSizeMake(64, 64);
        UIGraphicsBeginImageContextWithOptions(iconSize, NO, 0);
        
        [self drawSymbolInRect:(CGRect){CGPointZero, iconSize}];
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:UIGraphicsGetImageFromCurrentImageContext()];
        UIGraphicsEndImageContext();
        
        return imageView;
    } else if (self.alertViewStyle == HIAlertViewStyleCustom) {
        return self.customView;
    }
    return nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)drawSymbolInRect:(CGRect)rect { 
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    // General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Color Declarations
    UIColor *grey = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1.0];
    UIColor *black50 = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    
    // Gradient Declarations
    NSArray *gradientColors = [NSArray arrayWithObjects: 
                               (id)[UIColor whiteColor].CGColor, 
                               (id)grey.CGColor, nil];
    CGFloat gradientLocations[] = {0, 1};
    
#if __has_feature(objc_arc)
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
#else
     CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)gradientColors, gradientLocations);
#endif
    
    // Shadow Declarations
    CGColorRef shadow = black50.CGColor;
    CGSize shadowOffset = CGSizeMake(0, 3);
    CGFloat shadowBlurRadius = 3;
    
    // Bezier Drawing
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    if (self.alertViewStyle == HIAlertViewStyleSuccess) {
        [bezierPath moveToPoint:CGPointMake(16, 23)];
        [bezierPath addLineToPoint:CGPointMake(27, 34)];
        [bezierPath addLineToPoint:CGPointMake(56, 5)];
        [bezierPath addLineToPoint:CGPointMake(63, 12)];
        [bezierPath addLineToPoint:CGPointMake(27, 48)];
        [bezierPath addLineToPoint:CGPointMake(9, 30)];
        [bezierPath addLineToPoint:CGPointMake(16, 23)];
    } else {
        [bezierPath moveToPoint: CGPointMake(11, 17)];
        [bezierPath addLineToPoint: CGPointMake(19, 9)];
        [bezierPath addLineToPoint: CGPointMake(33, 23)];
        [bezierPath addLineToPoint: CGPointMake(47, 9)];
        [bezierPath addLineToPoint: CGPointMake(55, 17)];
        [bezierPath addLineToPoint: CGPointMake(41, 31)];
        [bezierPath addLineToPoint: CGPointMake(55, 45)];
        [bezierPath addLineToPoint: CGPointMake(47, 53)];
        [bezierPath addLineToPoint: CGPointMake(33, 39)];
        [bezierPath addLineToPoint: CGPointMake(19, 53)];
        [bezierPath addLineToPoint: CGPointMake(11, 45)];
        [bezierPath addLineToPoint: CGPointMake(25, 31)];
        [bezierPath addLineToPoint: CGPointMake(11, 17)];
    }
    
    [bezierPath closePath];
    
    // Determine scale (the default side is 64)
    CGPoint offset = CGPointMake((CGRectGetWidth(rect) - 64.0) / 2.0, (CGRectGetHeight(rect) - 64.0) / 2.0);
    
    [bezierPath applyTransform:CGAffineTransformMakeTranslation(offset.x, offset.y)];
    
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, shadow);
    CGContextSetFillColorWithColor(context, shadow);
    
    [bezierPath fill];
    [bezierPath addClip];
    
    CGRect bounds = bezierPath.bounds;
    
    CGContextDrawLinearGradient(context,
                                gradient,
                                CGPointMake(CGRectGetMidX(bounds), CGRectGetMinY(bounds)),
                                CGPointMake(CGRectGetMidX(bounds), CGRectGetMaxY(bounds)),
                                0);
    CGContextRestoreGState(context);
    
    // Cleanup
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    
    CGContextRestoreGState(ctx);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) reset {
    [self layoutComponents];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)layoutComponents {
    [self setNeedsDisplay];
    
    // Compute frames of components
    CGFloat layoutFrameInset = kHIAlertViewFrameInset + kHIAlertViewPadding;
    CGRect layoutFrame = CGRectInset(self.bounds, layoutFrameInset, layoutFrameInset);
    CGFloat layoutWidth = CGRectGetWidth(layoutFrame);
    
    // Title frame
    CGFloat titleHeight = 0;
    CGFloat minY = CGRectGetMinY(layoutFrame);
    if (self.title.length > 0) {
        titleHeight = [self.title sizeWithFont:self.titleFont
                             constrainedToSize:CGSizeMake(layoutWidth, MAXFLOAT)
                                 lineBreakMode:UILineBreakModeWordWrap].height;
        minY += kHIAlertViewPadding;
    }
    layout.titleRect = CGRectMake(CGRectGetMinX(layoutFrame), minY, layoutWidth, titleHeight);
    
    // Subtitle frame
    CGFloat subtitleHeight = 0;
    minY = CGRectGetMaxY(layout.titleRect);
    if (self.message.length > 0) {
        subtitleHeight = [self.message sizeWithFont:self.subtitleFont
                                  constrainedToSize:CGSizeMake(layoutWidth, MAXFLOAT)
                                      lineBreakMode:UILineBreakModeWordWrap].height;
        minY += kHIAlertViewPadding;
    }
    layout.subtitleRect = CGRectMake(CGRectGetMinX(layoutFrame), minY, layoutWidth, subtitleHeight);
    
    // Accessory frame (note that views are in the content view coordinate system)
    self.accessoryView = [self makeAccessoryView];
    self.accessoryView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    
    CGFloat accessoryHeight = 0;
    CGFloat accessoryWidth = CGRectGetWidth(layoutFrame);
    CGFloat accessoryLeft = 0;
    
    minY = CGRectGetMaxY(layout.subtitleRect) - layoutFrameInset;
    
    if (self.accessoryView != nil) {
        accessoryHeight = CGRectGetHeight(self.accessoryView.frame);
        accessoryWidth = CGRectGetWidth(self.accessoryView.frame);
        accessoryLeft = (CGRectGetWidth(layoutFrame) - accessoryWidth) / 2.0;
        minY += kHIAlertViewPadding;
    }
    layout.accessoryRect = CGRectMake(accessoryLeft, minY, accessoryWidth, accessoryHeight);
    
    // Text fields frame (note that views are in the content view coordinate system)
    CGFloat textFieldsHeight = 0;
    NSUInteger numTextFields = self.textFields.count;
    
    minY = CGRectGetMaxY(layout.accessoryRect);
    if (numTextFields > 0) {
        textFieldsHeight = kHIAlertViewTextFieldHeight * (CGFloat)numTextFields + kHIAlertViewPadding * ((CGFloat)numTextFields - 1.0);
        minY += kHIAlertViewPadding;
    }
    layout.textFieldsRect = CGRectMake(CGRectGetMinX(layoutFrame), minY, layoutWidth, textFieldsHeight);
    
    // Buttons frame (note that views are in the content view coordinate system)
    CGFloat buttonsHeight = 0;
    minY = CGRectGetMaxY(layout.textFieldsRect);
    if (self.buttons.count > 0) {
        buttonsHeight = kHIAlertViewgButtonHeight;
        minY += kHIAlertViewPadding;
    }
    layout.buttonRect = CGRectMake(CGRectGetMinX(layoutFrame), minY+kHIAlertViewPadding, layoutWidth, buttonsHeight);
    
    // Adjust layout frame
    layoutFrame.size.height = CGRectGetMaxY(layout.buttonRect);
    
    // Create new content view
    UIView *newContentView = [[UIView alloc] initWithFrame:layoutFrame];
    newContentView.contentMode = UIViewContentModeRedraw;
    
    // Layout accessory view
    self.accessoryView.frame = layout.accessoryRect;
    
    [newContentView addSubview:self.accessoryView];
    
    // Layout text fields
    if (numTextFields > 0) {
        for (int i=0; i<numTextFields; i++) {
            CGFloat offsetY = (kHIAlertViewTextFieldHeight + kHIAlertViewPadding) * (CGFloat)i;
            CGRect fieldFrame = CGRectMake(0,
                                           CGRectGetMinY(layout.textFieldsRect) + offsetY,
                                           layoutWidth,
                                           kHIAlertViewTextFieldHeight);
            
            UITextField *field = [self.textFields objectAtIndex:i];
            field.frame = fieldFrame;
            field.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
            
            [newContentView addSubview:field];
        }
    }
    
    // Layout buttons
    NSUInteger count = self.buttons.count;
    if (count > 0) {
        CGFloat buttonWidth = (CGRectGetWidth(layout.buttonRect) - kHIAlertViewPadding * ((CGFloat)count - 1.0)) / (CGFloat)count;
        
        for (int i=0; i<count; i++) {
            CGFloat left = (kHIAlertViewPadding + buttonWidth) * (CGFloat)i;
            CGRect buttonFrame = CGRectIntegral(CGRectMake(left, CGRectGetMinY(layout.buttonRect), buttonWidth, CGRectGetHeight(layout.buttonRect)));
            
            HIAlertViewButton *button = [self.buttons objectAtIndex:i];
            button.frame = buttonFrame;
            button.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
            
            NSString *title = [button titleForState:UIControlStateNormal];
            
            // Set default image
            UIGraphicsBeginImageContextWithOptions(buttonFrame.size, NO, 0);
            
            [self drawButtonInRect:(CGRect){CGPointZero, buttonFrame.size} title:title whitened:button.whitened down:NO];
            
            [button setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
            
            UIGraphicsEndImageContext();
            
            // Set alternate image
            UIGraphicsBeginImageContextWithOptions(buttonFrame.size, NO, 0);
            
            [self drawButtonInRect:(CGRect){CGPointZero, buttonFrame.size} title:title whitened:NO down:YES];
            [button setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateHighlighted];
            
            UIGraphicsEndImageContext();
            
            [newContentView addSubview:button];
        }
    }
    
    
    CGRect dialogFrame = 
    CGRectInset(layoutFrame,-layoutFrameInset,-layoutFrameInset);
    
    // Fade content views
    if (self.contentView.superview != nil) {
        [UIView transitionFromView:self.contentView
                            toView:newContentView
                          duration:kHIAlertViewAnimationDuration
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        completion:^(BOOL finished) {
                            self.contentView = newContentView;
                        }];
        
        [UIView animateWithDuration:kHIAlertViewAnimationDuration 
                              delay:0 
                            options:UIViewAnimationOptionLayoutSubviews 
                         animations:^{
                             self.bounds = 
                             CGRectMake(0, 0,dialogFrame.size.width,dialogFrame.size.height);
                         } completion:^(BOOL finished) {
                             [self setNeedsDisplay];
                         }];
        
    } else {
        self.contentView = newContentView;
        [self addSubview:newContentView];
        self.bounds = CGRectMake(0, 0,dialogFrame.size.width,dialogFrame.size.height);
        [self setNeedsDisplay];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)drawRect:(CGRect)rect {
    [self drawDialogBackgroundInRect:rect];
    [self drawTitleInRect:layout.subtitleRect isSubtitle:YES];
    [self drawTitleInRect:layout.titleRect isSubtitle:NO];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)drawTitleInRect:(CGRect)rect isSubtitle:(BOOL)isSubtitle {
    NSString *title = (isSubtitle ? self.message : self.title);
    if (title.length > 0) {
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSaveGState(ctx);
        
        CGContextSetShadowWithColor(ctx, CGSizeMake(0.0, -1.0), 0.0, [UIColor blackColor].CGColor);
        
        UIFont *font = (isSubtitle ? self.subtitleFont : self.titleFont);
        
        [self.textColor set];
        
        [title drawInRect:rect withFont:font lineBreakMode:UILineBreakModeMiddleTruncation alignment:UITextAlignmentCenter];
        
        CGContextRestoreGState(ctx);
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)drawDialogBackgroundInRect:(CGRect)rect {
    // General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set alpha
    CGContextSaveGState(context);
    
    if(!self.backgroundPatternImage){
        CGContextSetAlpha(context, 0.65);
    }
    
    // Abstracted Graphic Attributes
    CGFloat cornerRadius = 8.0;
    CGFloat strokeWidth = 2.0;
    CGColorRef dialogShadow = [UIColor blackColor].CGColor;
    CGSize shadowOffset = CGSizeMake(0, 4);
    CGFloat shadowBlurRadius = kHIAlertViewFrameInset - 2.0;
    
    CGRect frame = CGRectInset(CGRectIntegral(self.bounds), kHIAlertViewFrameInset, kHIAlertViewFrameInset);
    
    // Rounded Rectangle Drawing
    UIBezierPath *roundedRectanglePath = [UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:cornerRadius];
    
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, shadowOffset, shadowBlurRadius, dialogShadow);
    
    if(!self.backgroundPatternImage){
        
        [self.tintColor setFill];
        [roundedRectanglePath fill];
    } else {
        UIColor *fillColor = [UIColor colorWithPatternImage:self.backgroundPatternImage];
        [fillColor setFill];
        [roundedRectanglePath fill];
    }
    
    CGContextRestoreGState(context);
    
    // Set clip path
    [roundedRectanglePath addClip];
    
    // Bezier Drawing
    CGFloat mx = CGRectGetMinX(frame);
    CGFloat my = CGRectGetMinY(frame);
    CGFloat w = CGRectGetWidth(frame);
    CGFloat w2 = w / 2.0;
    CGFloat w4 = w / 4.0;
    CGFloat h1 = 25;
    CGFloat h2 = 35;
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(mx, h1)];
    [bezierPath addCurveToPoint:CGPointMake(mx + w2, h2) controlPoint1:CGPointMake(mx, h1) controlPoint2:CGPointMake(mx + w4, h2)];
    [bezierPath addCurveToPoint:CGPointMake(mx + w, h1) controlPoint1:CGPointMake(mx + w2 + w4, h2) controlPoint2:CGPointMake(mx + w, h1)];
    [bezierPath addCurveToPoint:CGPointMake(mx + w, my) controlPoint1:CGPointMake(mx + w, h1) controlPoint2:CGPointMake(mx + w, my)];
    [bezierPath addCurveToPoint:CGPointMake(mx, my) controlPoint1:CGPointMake(mx + w, my) controlPoint2:CGPointMake(mx, my)];
    [bezierPath addLineToPoint:CGPointMake(mx, h1)];
    [bezierPath closePath];
    
    CGContextSaveGState(context);
    
    [bezierPath addClip];
    
    if(!self.backgroundPatternImage){
    
        CGFloat locations[3]	= { 0.0,0.7,1};
        CGFloat components[24]	= {	1,1,1,0.9,1,1,1,0.3,1,1,1,0.1};
        CGGradientRef gradient2 = CGGradientCreateWithColorComponents(colorSpace, components, locations, 3);
        CGContextDrawLinearGradient(context, gradient2, CGPointMake(w2, 0), CGPointMake(w2, h2), 0);
        CGGradientRelease(gradient2);
    }
    
    CGContextRestoreGState(context);
    
    // Stroke
    [self.borderColor setStroke];  
    UIBezierPath *strokePath = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(frame, strokeWidth / 2.0, strokeWidth / 2.0)
                                                          cornerRadius:cornerRadius - strokeWidth / 2.0];
    strokePath.lineWidth = strokeWidth;
    
    [strokePath stroke];
    
    // Cleanup
    CGColorSpaceRelease(colorSpace);
    CGContextRestoreGState(context);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)drawButtonInRect:(CGRect)rect title:(NSString *)title whitened:(BOOL)whitened down:(BOOL)down {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    CGFloat radius = 5.0;
    CGFloat strokeWidth = 1.0;
    
    CGRect frame = CGRectIntegral(rect);
    CGRect buttonFrame = CGRectInset(frame, 0, 1);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Color declarations
    CGGradientRef gradient;
    if(whitened){
        
        CGFloat locations[6]	= { 0.0,0.25,0.5,0.5,0.75,1};
        CGFloat components[24]	= {	1,1,1,0.5,
            1,1,1,0.25,
            1,1,1,0.1,
            1,1,1,0,
            1,1,1,0.05,
            1,1,1,0.1};
        
        gradient = CGGradientCreateWithColorComponents(colorSpace, components, locations, 6);
    
    } else if(down) {
        
        CGFloat locations[4]	= { 0.0,0.5,0.5,1};
        CGFloat components[16]	= {	1,1,1,0.9,1,1,1,0,1,1,1,0,1,1,1,0.1};
        
        gradient = CGGradientCreateWithColorComponents(colorSpace, components, locations, 4);
    
    } else {
        
        CGFloat locations[6]	= { 0.0,0.25,0.5,0.5,0.75,1};
        CGFloat components[24]	= {	1,1,1,0.5,
            1,1,1,0.25,
            1,1,1,0.1,
            1,1,1,0,
            1,1,1,0.05,
            1,1,1,0.1};
        
        gradient = CGGradientCreateWithColorComponents(colorSpace, components, locations, 6);
    }
    
    CGColorSpaceRelease(colorSpace);
    // Bottom shadow
    UIBezierPath *fillPath = [UIBezierPath bezierPathWithRoundedRect:buttonFrame cornerRadius:radius];
    
    UIBezierPath *clipPath = [UIBezierPath bezierPathWithRect:frame];
    [clipPath appendPath:fillPath];
    [clipPath setUsesEvenOddFillRule:YES];
    
    CGContextSaveGState(ctx);
    
    [clipPath addClip];
    [[UIColor blackColor] setFill];
    
    CGContextSetShadowWithColor(ctx, CGSizeMake(0, 1), 0, [UIColor colorWithWhite:1.0 alpha:0.25].CGColor);
    
    [fillPath fill];
    
    CGContextRestoreGState(ctx);
    
    // Top shadow
    CGContextSaveGState(ctx);
    
    [fillPath addClip];
    [[UIColor blackColor] setFill];
    
    CGContextSetShadowWithColor(ctx, CGSizeMake(0, 2), 0, [UIColor colorWithWhite:1.0 alpha:0.25].CGColor);
    
    [clipPath fill];
    
    CGContextRestoreGState(ctx);
    
    // Button gradient
    CGContextSaveGState(ctx);
    [fillPath addClip];
    
    CGContextDrawLinearGradient(ctx,
                                gradient,
                                CGPointMake(CGRectGetMidX(buttonFrame), CGRectGetMinY(buttonFrame)),
                                CGPointMake(CGRectGetMidX(buttonFrame), CGRectGetMaxY(buttonFrame)), 0);
    CGContextRestoreGState(ctx);
    CGGradientRelease(gradient);
    // Draw whitened or down state
    if (whitened) {
        CGContextSaveGState(ctx);
        
        [[UIColor colorWithWhite:1.0 alpha:0.25] setFill];
        [fillPath fill];
        
        CGContextRestoreGState(ctx);
    } else if (down) {
        CGContextSaveGState(ctx);
        
        [[UIColor colorWithWhite:0.0 alpha:0.35] setFill];
        [fillPath fill];
        
        CGContextRestoreGState(ctx);
    }
    
    // Button stroke
    UIBezierPath *strokePath = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(buttonFrame, strokeWidth / 2.0, strokeWidth / 2.0)
                                                          cornerRadius:radius - strokeWidth / 2.0];
    
    [[UIColor colorWithWhite:0.0 alpha:0.8] setStroke];
    [strokePath stroke];
    
    // Draw title
    CGFloat fontSize = 18.0;
    CGRect textFrame = CGRectIntegral(CGRectMake(0, (CGRectGetHeight(rect) - fontSize) / 2.0 - 1.0, CGRectGetWidth(rect), fontSize));
    
    CGContextSaveGState(ctx);
    CGContextSetShadowWithColor(ctx, CGSizeMake(0.0, -1.0), 0.0, [UIColor blackColor].CGColor);
    
    [self.textColor set];
    [title drawInRect:textFrame withFont:self.titleFont lineBreakMode:UILineBreakModeMiddleTruncation alignment:UITextAlignmentCenter];
    
    CGContextRestoreGState(ctx);
    
    // Restore
    CGContextRestoreGState(ctx);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex{
    [self dismissWithButtonIndex:buttonIndex];
}

#pragma mark -
#pragma mark Accessors

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setDelegate:(id)delegate {
    
    if(_delegate!=delegate){
        _delegate=delegate;
        
        _flags.delegateClickedButtonAtIndex =
        [_delegate respondsToSelector:@selector(alertView:clickedButtonAtIndex:)];
        _flags.delegateWillPresentAlertView =
        [_delegate respondsToSelector:@selector(willPresentAlertView:)];
        _flags.delegateDidPresentAlertView = 
        [_delegate respondsToSelector:@selector(didPresentAlertView:)];
        _flags.delegatewillDismissWithButtonIndex = 
        [_delegate respondsToSelector:@selector(alertView:willDismissWithButtonIndex:)];
        _flags.delegateDidDismissWithButtonIndex = 
        [_delegate respondsToSelector:@selector(alertView:didDismissWithButtonIndex:)];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSMutableArray*)buttons {
    if(!_buttons){
        _buttons = [[NSMutableArray alloc] init];
    }
    return _buttons;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSMutableArray*)textFields {
    if(!_textFields){
        _textFields = [[NSMutableArray alloc] init];
    }
    return _textFields;
}

#if !__has_feature(objc_arc)
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setCompletionBlock:(CompletionBlock)completionBlock {
    if (_completionBlock) {
        Block_release(_completionBlock);
        _completionBlock = nil;
    }
    _completionBlock = Block_copy(completionBlock);
}
#endif

#pragma mark - UITextFieldDelegate

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // Cylce through text fields
    NSUInteger index = [self.textFields indexOfObject:textField];
    NSUInteger count = self.textFields.count;
    
    if (index < (count - 1)) {
        UITextField *nextField = [self.textFields objectAtIndex:index + 1];
        [nextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark -
#pragma mark Keyboard Notifications

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) onKeyboardWillShow: (NSNotification*) note
{
	NSValue* v = [note.userInfo objectForKey: UIKeyboardFrameEndUserInfoKey];
	CGRect kbframe = [v CGRectValue];
	kbframe = [self.superview convertRect: kbframe fromView: nil];
	
	if ( CGRectIntersectsRect( self.frame, kbframe) )
	{
		CGPoint c = self.center;
		
		if ( self.frame.size.height > kbframe.origin.y - 20 )
		{
			//self.maxHeight = kbframe.origin.y - 20;
			[self sizeToFit];
			[self layoutSubviews];
		}
		
		c.y = kbframe.origin.y / 2;
		
		[UIView animateWithDuration: 0.2 
						 animations: ^{
							 self.center = c;
							 self.frame = CGRectIntegral(self.frame);
						 }];
	}
    _keyboardShown = YES;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) onKeyboardWillHide: (NSNotification*) note
{
	[UIView animateWithDuration: 0.2 
					 animations: ^{
						 self.center = CGPointMake( CGRectGetMidX( self.superview.bounds ), CGRectGetMidY( self.superview.bounds ));
						 self.frame = CGRectIntegral(self.frame);
					 }];
    _keyboardShown = NO;
}

#pragma mark -
#pragma mark Button Touched

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)buttonTouched:(UIButton*)button {
    
    NSInteger index = [self.buttons indexOfObject:button];
    
    if(_flags.delegateClickedButtonAtIndex){
        [(id<HIAlertViewDelegate>)_delegate alertView:self clickedButtonAtIndex:index];
    }
    
    [self dismissWithButtonIndex:index];
}

#pragma mark -
#pragma mark Private

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) dismissWithButtonIndex:(NSInteger)index {
    
    for(HIAlertViewTextField* textField in self.textFields){
        [textField resignFirstResponder];
    }
    
    if(_flags.delegatewillDismissWithButtonIndex){
        BOOL shouldDismiss = [(id<HIAlertViewDelegate>)_delegate alertView:self willDismissWithButtonIndex:index];
    
        if(!shouldDismiss){
            return;
        }
    }
    
    self.window.backgroundColor = [UIColor clearColor];
    self.window.alpha = 1;
    
    [UIView animateWithDuration: 0.2 
                     animations: ^{
                         self.alpha = 0;
                     }];
    
    [UIView animateWithDuration: 0.4
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations: ^{
                         self.window.alpha = 0;
                     }
                     completion: ^(BOOL finished) {
                         
                         //cleanup
                         [self.oldKeywindow makeKeyAndVisible];
                         [self.window release];
                         self.alpha = 1;
                         [self.contentView removeFromSuperview];
                         self.contentView = nil;
                         if(_flags.delegateDidDismissWithButtonIndex){
                             [(id<HIAlertViewDelegate>)_delegate alertView:self didDismissWithButtonIndex:index];
                         }
                         if(_completionBlock){
                             _completionBlock(index);
                         }
                     }];
    
    [UIView commitAnimations];
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation HIAlertViewController
@synthesize supportsLandscape;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if(HIIsSupportedOrientation(toInterfaceOrientation)){
        if((UIInterfaceOrientationIsLandscape(toInterfaceOrientation))&&(!self.supportsLandscape)){
            return NO;
        } else {
            return YES;
        }
    }
    return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
	HIAlertView* alertView = [self.view.subviews lastObject];
	if (!alertView || ![alertView isKindOfClass:[HIAlertView class]])
		return;
    
    if(alertView.keyboardShown) {
        CGRect diffFrame = self.view.bounds;
        diffFrame.size.height-=HIKeyboardHeight();
        alertView.center =  CGPointMake(CGRectGetMidX(diffFrame),CGRectGetMidY(diffFrame));
    } else {
        alertView.center = CGPointMake( CGRectGetMidX( self.view.bounds ), CGRectGetMidY( self.view.bounds ) );
    }
    alertView.frame = CGRectIntegral(alertView.frame );
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation HIAlertOverlayWindow
@synthesize oldKeyWindow;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void) drawRect: (CGRect) rect
{ 
    // General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat locations[3]	= { 0.0,0.45,1.0};
	CGFloat components[12]	= {	1,1,1,0.45,0,0,0,0.4,0,0,0,0.6};
	
	CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
	CGGradientRef backgroundGradient = CGGradientCreateWithColorComponents(colorspace, components, locations, 3);
    // Rectangle Drawing
    CGPoint mid = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
    CGFloat endRadius = sqrt(pow(CGRectGetMidY(rect),2)+pow(CGRectGetMidX(rect),2));
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect:rect];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    CGContextDrawRadialGradient(context,
                                backgroundGradient,
                                mid, 0,
                                mid, endRadius, 
                                kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
    CGContextRestoreGState(context);
    
    // Cleanup
    CGGradientRelease(backgroundGradient);
    CGColorSpaceRelease(colorSpace);
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation HIAlertViewTextField

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 4.0, 4.0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)drawRect:(CGRect)rect {
    // General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    // Color Declarations
    UIColor *white10 = [UIColor colorWithWhite:1.0 alpha:0.1];
    UIColor *grey40 = [UIColor colorWithWhite:0.4 alpha:1.0];
    
    // Shadow Declarations
    CGColorRef innerShadow = grey40.CGColor;
    CGSize innerShadowOffset = CGSizeMake(0, 2);
    CGFloat innerShadowBlurRadius = 2;
    CGColorRef outerShadow = white10.CGColor;
    CGSize outerShadowOffset = CGSizeMake(0, 1);
    CGFloat outerShadowBlurRadius = 0;
    
    // Rectangle Drawing
    UIBezierPath *rectanglePath = [UIBezierPath bezierPathWithRect: CGRectIntegral(rect)];
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, outerShadowOffset, outerShadowBlurRadius, outerShadow);
    [[UIColor whiteColor] setFill];
    [rectanglePath fill];
    
    // Rectangle Inner Shadow
    CGRect rectangleBorderRect = CGRectInset([rectanglePath bounds], -innerShadowBlurRadius, -innerShadowBlurRadius);
    rectangleBorderRect = CGRectOffset(rectangleBorderRect, -innerShadowOffset.width, -innerShadowOffset.height);
    rectangleBorderRect = CGRectInset(CGRectUnion(rectangleBorderRect, [rectanglePath bounds]), -1, -1);
    
    UIBezierPath* rectangleNegativePath = [UIBezierPath bezierPathWithRect: rectangleBorderRect];
    [rectangleNegativePath appendPath: rectanglePath];
    rectangleNegativePath.usesEvenOddFillRule = YES;
    
    CGContextSaveGState(context);
    {
        CGFloat xOffset = innerShadowOffset.width + round(rectangleBorderRect.size.width);
        CGFloat yOffset = innerShadowOffset.height;
        CGContextSetShadowWithColor(context,
                                    CGSizeMake(xOffset + copysign(0.1, xOffset), yOffset + copysign(0.1, yOffset)),
                                    innerShadowBlurRadius,
                                    innerShadow);
        
        [rectanglePath addClip];
        CGAffineTransform transform = CGAffineTransformMakeTranslation(-round(rectangleBorderRect.size.width), 0);
        [rectangleNegativePath applyTransform: transform];
        [[UIColor grayColor] setFill];
        [rectangleNegativePath fill];
    }
    
    CGContextRestoreGState(context);
    CGContextRestoreGState(context);
    
    [[UIColor blackColor] setStroke];
    rectanglePath.lineWidth = 1;
    [rectanglePath stroke];
    
    CGContextRestoreGState(context);
}

@end

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation HIAlertViewButton;
@synthesize whitened;
@end
