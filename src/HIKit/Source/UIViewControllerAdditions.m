//
//  UIViewControllerAdditions.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 2/1/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

#import <objc/runtime.h>

#import "HIKit/HIKitUICommon.h"
#import "HIKit/HITableNavigationController.h"
#import "HIKit/UIViewControllerAdditions.h"

static const CGFloat kDefaultPortraitWidthForHeaderViewControllerInSlidingNavigationController = 294;
static const CGFloat kDefaultLandscapeWidthForHeaderViewControllerInSlidingNavigationController = 294;

static const CGFloat kDefaultWidthForHeaderViewController = 294;
static const CGFloat kDefaultWidthForViewController = 475;

static const CGFloat kDefaultWidthForHeaderViewControllerPortraitIPhone = 198;
static const CGFloat kDefaultWidthForViewControllerPortraitIPhone = 320;

static const CGFloat kDefaultWidthForHeaderViewControllerLandscapeIPhone = 297;
static const CGFloat kDefaultWidthForViewControllerLandscapeIPhone = 480;

static const CGFloat kDefaultTitleViewHorizontalInset = 100;
static const CGFloat kDefaultTitleViewVerticalInset = 10;

static const char* tableNavigationControllerKey = "_tableNavigationController";
static const char* slidingNavigationControllerKey = "_slidingNavigationController";
static const char* showsNavigationBarInCascadeNavigationKey = "_showsNavigationBarInCascadeNavigation";
static const char* cascadeNavigationBarTintColorKey = "_cascadeNavigationBarTintColor";
static const char* cascadeNavigationBarTitleViewInsetKey = "_cascadeNavigationBarTitleViewInset";

@implementation UIViewController (HICategory)

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIViewController*)tableNavigationController {
    return objc_getAssociatedObject(self,(void*)tableNavigationControllerKey);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setTableNavigationController:(HITableNavigationController *)tableNavigationController{
    objc_setAssociatedObject(self,(void*)tableNavigationControllerKey,tableNavigationController,OBJC_ASSOCIATION_ASSIGN);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIViewController*)slidingNavigationController {
    return objc_getAssociatedObject(self,(void*)slidingNavigationControllerKey);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setSlidingNavigationController:(HISlidingNavigationController *)slidingNavigationController{
    objc_setAssociatedObject(self,(void*)slidingNavigationControllerKey,slidingNavigationController,OBJC_ASSOCIATION_ASSIGN);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(CGFloat) viewControllerWidthForOrientation:(UIInterfaceOrientation)orientation isHeaderViewController:(BOOL)isHeaderViewController {

    if(HIIsPad()){
        if (isHeaderViewController) {
            return kDefaultWidthForHeaderViewController;
        } else {
            return kDefaultWidthForViewController;
        }
    }else{
        if (isHeaderViewController) {
            if(UIInterfaceOrientationIsPortrait(orientation)){
                return kDefaultWidthForHeaderViewControllerPortraitIPhone;
            }else{
                return kDefaultWidthForHeaderViewControllerLandscapeIPhone;
            }
        } else {
            if(UIInterfaceOrientationIsPortrait(orientation)){
                return kDefaultWidthForViewControllerPortraitIPhone;
            }else{
                return kDefaultWidthForViewControllerLandscapeIPhone;
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(CGFloat) headerViewControllerWidthInSlidingNavigationControllerForOrientation:(UIInterfaceOrientation)orientation{
    if(UIInterfaceOrientationIsPortrait(orientation)){
        return kDefaultPortraitWidthForHeaderViewControllerInSlidingNavigationController;
    } else {
        return kDefaultLandscapeWidthForHeaderViewControllerInSlidingNavigationController;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setShowsNavigationBarInCascadeNavigation:(BOOL)showsNavigationBarInCascadeNavigation {
    
    NSValue *showsNavigationBarValue = 
    @(showsNavigationBarInCascadeNavigation);
    objc_setAssociatedObject(self,(void*)showsNavigationBarInCascadeNavigationKey,
                             showsNavigationBarValue,OBJC_ASSOCIATION_RETAIN);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(BOOL) showsNavigationBarInCascadeNavigation {
    
    BOOL showsNavigationBar;
    id showsNavigationBarValue = objc_getAssociatedObject(self,(void*)showsNavigationBarInCascadeNavigationKey);
    
    if (!showsNavigationBarValue) {
        
        //default value is NO
        showsNavigationBar = NO;
        [self setShowsNavigationBarInCascadeNavigation:showsNavigationBar];
        
    } else {
        showsNavigationBar = [showsNavigationBarValue boolValue];
    }
    return showsNavigationBar;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setCascadeNavigationBarTintColor:(UIColor *)cascadeNavigationBarTintColor {
    
    objc_setAssociatedObject(self,(void*)cascadeNavigationBarTintColorKey,
                             cascadeNavigationBarTintColor,OBJC_ASSOCIATION_RETAIN);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UIColor*) cascadeNavigationBarTintColor {
    UIColor* cascadeNavigationBarTintColor = objc_getAssociatedObject(self,(void*)cascadeNavigationBarTintColorKey);
    if (!cascadeNavigationBarTintColor) {
        
        //default value is HIRGBCOLOR(109, 132, 162);
        cascadeNavigationBarTintColor = HIRGBCOLOR(109, 132, 162);
        [self setCascadeNavigationBarTintColor:cascadeNavigationBarTintColor];
    }
    
    return cascadeNavigationBarTintColor;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setCascadeNavigationBarTitleViewInset:(CGPoint)cascadeNavigationBarTitleViewInset {
    
    NSValue *titleViewInsetValue = 
    [NSValue valueWithCGPoint:cascadeNavigationBarTitleViewInset];
    objc_setAssociatedObject(self,(void*)cascadeNavigationBarTitleViewInsetKey,
                             titleViewInsetValue,OBJC_ASSOCIATION_RETAIN);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(CGPoint) cascadeNavigationBarTitleViewInset {
    
    CGPoint cascadeNavigationBarTitleViewInset;
    id titleViewInsetValue = objc_getAssociatedObject(self,(void*)cascadeNavigationBarTitleViewInsetKey);
    
    if (!titleViewInsetValue) {
        
        //default value is NO
        cascadeNavigationBarTitleViewInset = CGPointMake(kDefaultTitleViewHorizontalInset, kDefaultTitleViewVerticalInset);
        [self setCascadeNavigationBarTitleViewInset:cascadeNavigationBarTitleViewInset];
        
    } else {
        cascadeNavigationBarTitleViewInset = [titleViewInsetValue CGPointValue];
    }
    return cascadeNavigationBarTitleViewInset;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) willBeRemovedFromTableNavigationController{
    //To be overridden
    //Default Implementation Does nothing
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void)setBackButtonTitle:(NSString *)backButtonTitle{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil action:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(NSString*)backButtonTitle{
    return self.navigationItem.backBarButtonItem.title;
}

@end
