//
//  HIKitUICommon.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 12/23/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import <unistd.h>
#import <sys/sysctl.h>

static NSString* kBundleName = @"HIKit.bundle";

#import "HIKit/HIKitUICommon.h"

const CGFloat kGroupedTableCellInset = 10.0f;
const CGFloat kGroupedPadTableCellInset = 45.0f;

const CGFloat kDefaultRowWidth = 44.0f;
const CGFloat kDefaultRowHeight = 44.0f;
const CGFloat kDefaultPortraitToolbarHeight   = 44.0f;
const CGFloat kDefaultLandscapeToolbarHeight  = 33.0f;

const CGFloat kDefaultPortraitKeyboardHeight      = 216.0f;
const CGFloat kDefaultLandscapeKeyboardHeight     = 160.0f;
const CGFloat kDefaultPadPortraitKeyboardHeight   = 264.0f;
const CGFloat kDefaultPadLandscapeKeyboardHeight  = 352.0f;

///////////////////////////////////////////////////////////////////////////////////////////////////
UIImage* HIImageFromBundle(NSString* imagePath) {
    NSString* fullImagePath = [NSString stringWithFormat:@"%@/%@",kBundleName,imagePath];
    return [UIImage imageNamed:fullImagePath];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
int IsInDebugger(void) {
    int                 mib[4];
    struct kinfo_proc   info;
    size_t              size;

    // Initialize the flags so that, if sysctl fails for some bizarre
    // reason, we get a predictable result.

    info.kp_proc.p_flag = 0;

    // Initialize mib, which tells sysctl the info we want, in this case
    // we're looking for information about a specific process ID.

    mib[0] = CTL_KERN;
    mib[1] = KERN_PROC;
    mib[2] = KERN_PROC_PID;
    mib[3] = getpid();

    // Call sysctl.

    size = sizeof(info);
    sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);

    // We're being debugged if the P_TRACED flag is set.

    return (info.kp_proc.p_flag & P_TRACED) != 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
float HIOSVersion() {
    return [[[UIDevice currentDevice] systemVersion] floatValue];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL HIRuntimeOSVersionIsAtLeast(float version) {
    
    static const CGFloat kEpsilon = 0.0000001f;
    return HIOSVersion() - version >= -kEpsilon;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
CGRect HIRectInset(CGRect rect, UIEdgeInsets insets) {
    return CGRectMake(rect.origin.x + insets.left, rect.origin.y + insets.top,
                      rect.size.width - (insets.left + insets.right),
                      rect.size.height - (insets.top + insets.bottom));
}

///////////////////////////////////////////////////////////////////////////////////////////////////
CGRect HIRectInvert(CGRect rect) {

    return CGRectMake(rect.origin.x,rect.origin.y,rect.size.height,rect.size.width);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL HIIsPad(void) {
#ifdef __IPHONE_3_2
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
#else
    return NO;
#endif
}

///////////////////////////////////////////////////////////////////////////////////////////////////
CGFloat HIGroupedTableCellInset(void) {
    return HIIsPad() ? kGroupedPadTableCellInset : kGroupedTableCellInset;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
NSString* NSStringFromCFloatArray(CGFloat* array,int length) {
    if (!array)
        return nil;

    NSMutableString* arrayString = [NSMutableString stringWithString:@"{ "];
    for (int index=0; index<length; index++) {
        [arrayString appendFormat:@"%.1f  ",array[index]];
    }
    [arrayString appendString:@" }"];
    return [NSString stringWithString:arrayString];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
UIDeviceOrientation HIDeviceOrientation() {
    UIDeviceOrientation orient = [[UIDevice currentDevice] orientation];
    if (UIDeviceOrientationUnknown == orient) {
        return UIDeviceOrientationPortrait;
        
    } else {
        return orient;
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL HIDeviceOrientationIsPortrait() {
    UIDeviceOrientation orient = HIDeviceOrientation();
    
    switch (orient) {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            return YES;
        default:
            return NO;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
CGFloat HIKeyboardHeightForOrientation(UIInterfaceOrientation orientation) {
    if (HIIsPad()) {
        return UIInterfaceOrientationIsPortrait(orientation) ? HI_IPAD_KEYBOARD_HEIGHT
        : HI_IPAD_LANDSCAPE_KEYBOARD_HEIGHT;
        
    } else {
        return UIInterfaceOrientationIsPortrait(orientation) ? HI_KEYBOARD_HEIGHT
        : HI_LANDSCAPE_KEYBOARD_HEIGHT;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
CGFloat HIKeyboardHeight() {
    return HIKeyboardHeightForOrientation(HIInterfaceOrientation());
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL HIDeviceOrientationIsLandscape() {
    UIDeviceOrientation orient = HIDeviceOrientation();
    
    switch (orient) {
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            return YES;
        default:
            return NO;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
UIInterfaceOrientation HIInterfaceOrientation() {
    UIInterfaceOrientation orient = [UIApplication sharedApplication].statusBarOrientation;
    return orient;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
UIInterfaceOrientation HIInterfaceOrientationInverse() {

    UIInterfaceOrientation orient = [UIApplication sharedApplication].statusBarOrientation;
    switch (orient) {
        case UIInterfaceOrientationLandscapeLeft:
            return UIInterfaceOrientationPortraitUpsideDown;

        case UIInterfaceOrientationLandscapeRight:
            return UIInterfaceOrientationPortrait;

        case UIInterfaceOrientationPortrait:
            return UIInterfaceOrientationLandscapeRight;

        case UIInterfaceOrientationPortraitUpsideDown:
            return UIInterfaceOrientationLandscapeLeft;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL HIIsSupportedOrientation(UIInterfaceOrientation orientation) {
    if (HIIsPad()) {
        return YES;

    } else {
        switch (orientation) {
            case UIInterfaceOrientationPortrait:
            case UIInterfaceOrientationLandscapeLeft:
            case UIInterfaceOrientationLandscapeRight:
                return YES;
            default:
                return NO;
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
CGFloat HIToolbarHeight() {
    return HIToolbarHeightForOrientation(HIInterfaceOrientation());
}

///////////////////////////////////////////////////////////////////////////////////////////////////
CGFloat HIToolbarHeightForOrientation(UIInterfaceOrientation orientation) {
    if (UIInterfaceOrientationIsPortrait(orientation) || HIIsPad()) {
        return HI_ROW_HEIGHT;
    } else {
        return HI_LANDSCAPE_TOOLBAR_HEIGHT;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
CGRect HIScreenBounds() {
    CGRect bounds = [UIScreen mainScreen].bounds;
    if (UIInterfaceOrientationIsLandscape(HIInterfaceOrientation())) {
        CGFloat width = bounds.size.width;
        bounds.size.width = bounds.size.height;
        bounds.size.height = width;
    }
    return bounds;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
CGRect HIToolbarNavigationFrame() {
    CGRect frame = [UIScreen mainScreen].applicationFrame;
    return CGRectMake(0, 0, frame.size.width, frame.size.height - HIToolbarHeight()*2);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL HIOSVersionIsAtLeast(float version) {
// Floating-point comparison is pretty bad, so let's cut it some slack with an epsilon.
static const CGFloat kEpsilon = 0.0000001f;

//First level of defense to be analyzed later
if(!HIRuntimeOSVersionIsAtLeast(version))
    return NO;

#ifdef __IPHONE_6_0
    return 6.0 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_5_1
    return 5.1 - version >= -kEpsilon;    
#endif    
#ifdef __IPHONE_5_0
    return 5.0 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_3
    return 4.3 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_2
    return 4.2 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_1
    return 4.1 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_0
    return 4.0 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_3_2
    return 3.2 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_3_1
    return 3.1 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_3_0
    return 3.0 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_2_2
    return 2.2 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_2_1
    return 2.1 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_2_0
    return 2.0 - version >= -kEpsilon;
#endif
    return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
NSString* HIDescriptionForError(NSError* error) {
    if ([error.domain isEqualToString:NSURLErrorDomain]) {
        // Note: If new error codes are added here, be sure to document them in the header.
        switch (error.code) {
           
            case NSURLErrorUnsupportedURL:
                return NSLocalizedString(@"Unsupported URL", @"");
                
            case NSURLErrorBadURL:
                return NSLocalizedString(@"Bad URL", @"");
           
            case NSURLErrorNotConnectedToInternet:
                return NSLocalizedString(@"No Internet Connection", @"");
                
            case NSURLErrorTimedOut:
                return NSLocalizedString(@"Connection Timed Out", @"");
                
            case NSURLErrorCannotFindHost:
                return NSLocalizedString(@"Cannot Find Host", @"");
                
            case NSURLErrorCannotConnectToHost:
                return NSLocalizedString(@"Cannot Connect to Host", @"");
                
            case NSURLErrorNetworkConnectionLost:
                return NSLocalizedString(@"Network Connection Lost", @"");
                
            case NSURLErrorDNSLookupFailed:
                return NSLocalizedString(@"DNS Lookup Failed", @"");
                
            case NSURLErrorHTTPTooManyRedirects:
                return NSLocalizedString(@"Too Many Redirects", @"");
                
            case NSURLErrorResourceUnavailable:
                return NSLocalizedString(@"Resource Unavailable", @"");
                
            case NSURLErrorRedirectToNonExistentLocation:
                return NSLocalizedString(@"Redirect to Nonexistent Location", @"");
            
            case NSURLErrorBadServerResponse:
                return NSLocalizedString(@"Bad Server Response", @"");
            
            case NSURLErrorUserCancelledAuthentication:
                return NSLocalizedString(@"User Cancelled Authentication", @"");
                
            case NSURLErrorUserAuthenticationRequired:
                return NSLocalizedString(@"User Authentication Required", @"");
             
            case NSURLErrorZeroByteResource:
                return NSLocalizedString(@"Zero Byte Resource", @"");
                
            case NSURLErrorCannotDecodeRawData:
                return NSLocalizedString(@"Cannot Decode Content Data", @"");
                
            case NSURLErrorCannotDecodeContentData:
                return NSLocalizedString(@"Cannot Decode Raw Data", @"");
                
            case NSURLErrorCannotParseResponse:
                return NSLocalizedString(@"Cannot Parse Response", @"");
            
            case NSURLErrorFileDoesNotExist:
                return NSLocalizedString(@"File Does Not Exist", @"");
                
            default:
                return NSLocalizedString(@"Connection Error", @"");;
        }
    }
    return NSLocalizedString(@"Error", @"");
}

