//
//  UITableViewCellAdditions.m
//  HIKit
//
//  Created by Sumeru Chatterjee on 1/31/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

#import <objc/runtime.h>

//HIKit
#import "HIKit/UITableViewCellAdditions.h"

static const char* orientationKey = "_orientationKey";

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation UITableViewCell (HICategory)

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UITableViewCellOrientation)orientation {

    UITableViewCellOrientation orientation;
    id orientationValue = objc_getAssociatedObject(self,(void*)orientationKey);

    if (!orientationValue) {
        //default value is horizontal
        orientation = UITableViewCellOrientationHorizontal;
        [self setOrientation:orientation];

    } else {
        orientation = [orientationValue intValue];
    }
    return orientation;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) setOrientation:(UITableViewCellOrientation)orientation {

    NSValue *orientationValue = [NSNumber numberWithInt:orientation];
    objc_setAssociatedObject(self,(void*)orientationKey,orientationValue,OBJC_ASSOCIATION_RETAIN);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UITableView*)containerTableView {

    if ([self.superview isKindOfClass:[UITableView class]]) {
        return (UITableView*)self.superview;

    } else
        return nil;
}

@end
