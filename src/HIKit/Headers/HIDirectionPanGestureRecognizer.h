//
//  HIDirectionPanGestureRecognizer.h
//  HIKit
//
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import <UIKit/UIGestureRecognizerSubclass.h>

typedef enum {
    HIDirectionPangestureRecognizerVertical,
    HIDirectionPanGestureRecognizerHorizontal
} HIDirectionPangestureRecognizerDirection;

@interface HIDirectionPanGestureRecognizer : UIPanGestureRecognizer {
    BOOL _drag;
    int _moveX;
    int _moveY;
    HIDirectionPangestureRecognizerDirection _direction;
}

@property (nonatomic, assign) HIDirectionPangestureRecognizerDirection direction;
@end
