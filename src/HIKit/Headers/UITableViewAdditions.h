//
//  UITableViewAdditions.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 1/31/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

typedef enum {
    UITableViewOrientationVertical=0,
    UITableViewOrientationHorizontal,
} UITableViewOrientation;

typedef enum {
    UITableViewScrollStylePlain=0,
    UITableViewScrollStyleCascaded,
} UITableViewScrollStyle;

@interface UITableView (HICategory)
@property (nonatomic,assign) UITableViewOrientation orientation;
@property (nonatomic,assign) UITableViewScrollStyle scrollStyle;

-(void) scrollToBottom:(BOOL)animated;
@end

