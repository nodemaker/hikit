//
//  UITableViewControllerAdditions.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 12/30/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

//HIKit
#import "UITableViewAdditions.h"

@interface UITableViewController (HICategory)

- (id)initWithOrientation:(UITableViewOrientation)orientation;
- (id)initWithStyle:(UITableViewStyle)style orientation:(UITableViewOrientation)orientation;

- (id)initWithScrollStyle:(UITableViewScrollStyle)scrollStyle;
- (id)initWithStyle:(UITableViewStyle)style scrollStyle:(UITableViewScrollStyle)scrollStyle;
- (id)initWithOrientation:(UITableViewOrientation)orientation scrollStyle:(UITableViewScrollStyle)scrollStyle;
- (id)initWithStyle:(UITableViewStyle)style orientation:(UITableViewOrientation)orientation scrollStyle:(UITableViewScrollStyle)scrollStyle;

@property (nonatomic,readonly) UITableViewOrientation tableViewOrientation;
@property (nonatomic,readonly) UITableViewScrollStyle tableViewScrollStyle;
@end
