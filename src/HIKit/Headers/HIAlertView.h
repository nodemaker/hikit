//
//  HIAlertView.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 5/4/12.
//  Inspired by UIAlertView,TSAlertView and CODialog(https://github.com/eaigner/CODialog)
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    HIAlertViewStyleDefault = 0,
    HIAlertViewStyleActivity,
    HIAlertViewStyleProgress,
    HIAlertViewStyleSuccess,
    HIAlertViewStyleError,
    HIAlertViewStyleCustom,
} HIAlertViewStyle;

typedef void (^CompletionBlock)(int buttonIndex);

@interface HIAlertView : UIView {
    NSString* _title;
    NSString* _message;
    id _delegate;
    UIView* _customView;
    UIFont* _titleFont;
    UIColor* _tintColor;
    UIColor* _textColor;
    UIColor* _borderColor;
    UIFont* _subtitleFont;
    UIColor* _backgroundPatterImage;
    HIAlertViewStyle _alertViewStyle;
    BOOL _supportsLandscape;
    CompletionBlock _completionBlock;
}
- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id /*<HIAlertViewDelegate>*/)delegate;

- (NSInteger)addButtonWithTitle:(NSString *)title;
- (NSInteger)addButtonWithTitle:(NSString *)title whitened:(BOOL)whitened;    // returns index of button. 0 based.
- (NSInteger)addTextFieldWithPlaceholder:(NSString *)placeholder secure:(BOOL)secure;

- (UIButton*)buttonAtIndex:(NSInteger)buttonIndex;

@property(nonatomic,assign) id /*<HIAlertViewDelegate>*/ delegate;    // weak reference
@property(nonatomic,copy) NSString *title;
@property(nonatomic, retain) UIFont *titleFont;
@property(nonatomic, retain) UIView *customView;
@property(nonatomic, retain) UIFont *subtitleFont;
@property(nonatomic, retain) UIColor *tintColor;
@property(nonatomic, retain) UIColor *textColor;
@property(nonatomic, retain) UIColor *borderColor;
@property(nonatomic,copy) NSString *message;// secondary explanation text
@property(nonatomic,retain) UIImage* backgroundPatternImage;
@property (nonatomic,retain) UIView* accessoryView;

- (NSString *)buttonTitleAtIndex:(NSInteger)index;
- (NSString *)textForTextFieldAtIndex:(NSUInteger)index;
@property(nonatomic,readonly) NSInteger numberOfButtons;
@property(nonatomic,readonly) NSInteger numberOfTextFields;

@property(nonatomic,assign) BOOL supportsLandscape;

-(void) removeAllButtons;
-(void) removeAllTextFields;

// shows popup alert animated.
- (void)show;
- (void)reset;

// hides alert sheet or popup. use this method when you need to explicitly dismiss the alert.
// it does not need to be called if the user presses on a button
- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex;

// Alert view style - defaults to HIAlertViewStyleDefault
@property(nonatomic,assign) HIAlertViewStyle alertViewStyle;

/* Retrieve a text field at an index - raises NSRangeException when textFieldIndex is out-of-bounds. 
 The field at index 0 will be the first text field (the single field or the login field), the field at index 1 will be the password field. */
- (UITextField *)textFieldAtIndex:(NSInteger)textFieldIndex;

//Block Support
- (id)initWithTitle:(NSString *)title message:(NSString *)message onCompletion:(CompletionBlock)completion;
@property (nonatomic, copy) CompletionBlock completionBlock;
@end


@protocol HIAlertViewDelegate <NSObject>
@optional

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(HIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

- (void)willPresentAlertView:(HIAlertView *)alertView;  // before animation and showing view
- (void)didPresentAlertView:(HIAlertView *)alertView;  // after animation

- (BOOL)alertView:(HIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex; // before animation and hiding view
- (void)alertView:(HIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;  // after animation

@end

