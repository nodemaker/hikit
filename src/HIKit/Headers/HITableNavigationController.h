//
//  UITableNavigationController.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 1/28/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

//Uses a table to be used as a container for view navigation
//Currently DOES NOT support vertical table navigation.
//The navigation table can be cascaded,plain,grouped

typedef enum {
    UITableNavigationViewStylePlain=0,
    UITableNavigationViewStyleGrouped,
    UITableNavigationViewStyleCascaded,
} UITableNavigationViewStyle;

@interface HITableNavigationController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,assign) BOOL detachEnabled;
@property (nonatomic,strong) UIImage* detachImage;

@property (weak, nonatomic,readonly) UIViewController* activeController;
// Recommended initializer.If init is called directly then the style is set to UITableNavigationViewStylePlain
- (id)initWithStyle:(UITableNavigationViewStyle)style;
// Convenience method pushes the root view controller without animation.
- (id)initWithHeaderViewController:(UIViewController *)headerViewController style:(UITableNavigationViewStyle)style;

// Pushes a view controller at the end of the stack
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated; 

// Pushes a view controller after the existing view controller.All the view controllers until the existing view controller are popped
// If the existing view controller is the header view controller then the view controller is set as the root view controller and all the view 
// existing view controllers are removed.
// If the exisiting view controller does not already exist in the stack then both the existingViewController and the viewController are
// pushed to the end of the stack.
- (void)pushViewController:(UIViewController *)viewController fromViewController:(UIViewController*)existingViewController animated:(BOOL)animated; 
- (void)pushViewControllerFromActiveController:(UIViewController *)viewController animated:(BOOL)animated;
// Pops the view controller at the end of the stack if one exists and returns the popped controller
- (UIViewController *)popViewControllerAnimated:(BOOL)animated; 
// Pops view controllers until the one specified is on top. Returns the popped controllers.
// if view controller is nil all the view controllers are popped
- (NSArray *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated;

//The table navigation view style
@property(assign,nonatomic,readonly) UITableNavigationViewStyle tableNavigationViewStyle;

// The current view controller stack.
@property(strong,nonatomic,readonly) NSArray *viewControllers;

//Gets or sets the first view controller of the stack.
//All existing view controllers are removed if this is set.
@property (nonatomic,strong) UIViewController* rootViewController;

//The view controller at the top of the navigation stack
@property (nonatomic,readonly) UIViewController *topViewController; // The top view controller on the stack.

//The View controller displayed as the header of the table view
@property (nonatomic,strong) UIViewController* headerViewController;

//Methods to override
-(void)controllerBecameActive:(UIViewController*)controller;

@end
