//
//  UITableViewCellAdditions.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 1/31/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

typedef enum {
    UITableViewCellOrientationHorizontal=0,
    UITableViewCellOrientationVertical,
} UITableViewCellOrientation;


@interface UITableViewCell (HICategory)
@property (nonatomic,readonly) UITableView* containerTableView;
@property (nonatomic,assign) UITableViewCellOrientation orientation;
@end
