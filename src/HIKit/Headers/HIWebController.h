//
//  HIWebViewController.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 1/9/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

@protocol HIWebControllerDelegate;
@protocol HIWebControllerDataSource;

@interface HIWebNavigationItem : NSObject
@property (nonatomic,strong) NSString* title;
@property (nonatomic,strong) NSURL* URL;
@property (nonatomic,assign) NSInteger index;
@property (nonatomic,strong) NSString* hashtag;
@property (nonatomic,strong) NSURL* nextURL;
@property (nonatomic,strong) NSURL* previousURL;
@end

@interface HIWebController : UIViewController <UIWebViewDelegate,UIActionSheetDelegate>
/**
 * The current web view URL. If the web view is currently loading a URL, then the loading URL is
 * returned instead.
 */
@property (nonatomic, readonly) NSURL*  currentURL;
/**
 *  return the current title
 */
@property (nonatomic, readonly) NSString* currentTitle;
/**
 *  return the current title
 */
@property (nonatomic, readonly) NSString* currentHashtag;
/**
 * The web view base URL
 */
@property (strong, nonatomic, readonly) NSURL*  baseURL;
/**
 * The web controller delegate
 */
@property (nonatomic, weak)  id<HIWebControllerDelegate> delegate;
/**
 * The web controller data source
 */
@property (nonatomic, strong)  id<HIWebControllerDataSource> dataSource;
/**
 * Opens the given URL in the web view.
 */
- (void)openURL:(NSURL*)URL;
/**
 * Load the given request using UIWebView's loadRequest:.
 *
 * @param request  A URL request identifying the location of the content to load.
 */
- (void)openRequest:(NSURLRequest*)request;
/**
 * Sets the bottom toolbar to the given color.
 */
- (void)setBottomToolbarTintColor:(UIColor*)color;
/**
 * Sets the top navigation bar to the given color.
 */
- (void)setTitleFont:(UIFont*)font;


@property (strong,nonatomic,readonly) UIToolbar*        bottomBar;

@property (nonatomic,strong) UIBarButtonItem*           backButton;
@property (nonatomic,strong) UIBarButtonItem*           forwardButton;
@property (nonatomic,strong) UIBarButtonItem*           refreshButton;
@property (nonatomic,strong) UIBarButtonItem*           stopButton;
@property (nonatomic,strong) UIBarButtonItem*           actionButton;
@property (nonatomic,strong) UIBarButtonItem*           twitterButton;
@property (nonatomic,strong) UIBarButtonItem*           nextButton;
@property (nonatomic,strong) UIBarButtonItem*           previousButton;

@end

@protocol HIWebControllerDelegate <NSObject>

@optional
- (BOOL)webController:(HIWebController *)controller webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
       navigationType:(UIWebViewNavigationType)navigationType;
- (void)webController:(HIWebController *)controller webViewDidStartLoad:(UIWebView *)webView;
- (void)webController:(HIWebController *)controller webViewDidFinishLoad:(UIWebView *)webView;
- (void)webController:(HIWebController *)controller webView:(UIWebView *)webView
 didFailLoadWithError:(NSError *)error;
@end

@protocol HIWebControllerDataSource <NSObject>
@optional
- (HIWebNavigationItem*)webController:(HIWebController*)controller navigationItemForURL:(NSURL*)URL;
@end
