//
//  HIHorizontalTableView.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 12/20/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

@protocol HIHorizontalTableViewDelegate <UITableViewDelegate>
@optional
//Variable Width Support 
- (CGFloat)tableView:(UITableView *)tableView widthForRowAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface HIHorizontalTableView : UITableView
@property (nonatomic, weak) id<HIHorizontalTableViewDelegate> delegate;
@end
