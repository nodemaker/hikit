//
//  HISlidingNavigationController.h
//  HIKit
//
//  Created by samyzee on 2/9/13.
//
//

#import <UIKit/UIKit.h>

@interface HISlidingNavigationController : UIViewController
- (id)initWithLeftHeaderViewController:(UIViewController *)leftHeaderViewController;

// Pushes a view controller at the end of the stack
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated;

// Pops the view controller at the end of the stack if one exists and returns the popped controller
- (UIViewController *)popViewControllerAnimated:(BOOL)animated; // Returns the popped controller.

// Pops view controllers until the one specified is on top. Returns the popped controllers.
// if view controller is nil all the view controllers are popped
- (NSArray *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated;

// Pops until there's only a single view controller left on the stack. Returns the popped controllers.
- (NSArray *)popToRootViewControllerAnimated:(BOOL)animated;

// The current view controller stack.
@property(nonatomic,readonly) NSArray *viewControllers;

//Gets or sets the first view controller of the stack.
@property (nonatomic,strong) UIViewController* rootViewController;

// The top view controller on the stack
@property(nonatomic,readonly) UIViewController *topViewController;

//The View controller displayed at the left
@property (nonatomic,strong) UIViewController* leftHeaderViewController;

//The View controller displayed at the right
@property (nonatomic,strong) UIViewController* rightHeaderViewController;

@end
