//
//  HIAnimationManager.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 12/23/11.
//

typedef void (^PostAnimationBlock)(void);

extern NSString *const HIAnimationStartedNotification;
extern NSString *const HIAnimationEndedNotification;

void HIPerformBlockAfterAnimationsEnd(PostAnimationBlock block);

@interface HIAnimationManager: NSObject
+ (HIAnimationManager*)sharedManager;
@property (atomic,assign) BOOL animating;
-(void)performBlockAfterAnimationsEnd:(PostAnimationBlock)block;
@end
