//
//  UIViewControllerAdditions.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 2/1/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

@class HITableNavigationController;
@class HISlidingNavigationController;

@interface UIViewController (HICategory)

// Back Button Title
@property (nonatomic,copy) NSString* backButtonTitle;

// If this view controller has been pushed onto a table navigation controller, return it.
@property(nonatomic,assign) HITableNavigationController *tableNavigationController;

//Table Navigation Controller

//Override this method to provide customized widths for different orientations.
//The default values is 476 for normal controllers and 272 for the header view controllers;
-(CGFloat) viewControllerWidthForOrientation:(UIInterfaceOrientation)orientation
                      isHeaderViewController:(BOOL)isHeaderViewController;
-(void) willBeRemovedFromTableNavigationController;
@property(nonatomic,assign) BOOL showsNavigationBarInCascadeNavigation;
@property(nonatomic,retain) UIColor* cascadeNavigationBarTintColor;
@property(nonatomic,assign) CGPoint cascadeNavigationBarTitleViewInset;

//Sliding Navigation Controller
@property(nonatomic,assign) HISlidingNavigationController *slidingNavigationController;
-(CGFloat) headerViewControllerWidthInSlidingNavigationControllerForOrientation:(UIInterfaceOrientation)orientation;
@end
