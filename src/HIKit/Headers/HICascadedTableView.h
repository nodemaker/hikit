//
//  HICascadeTableView.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 12/28/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import "HIHorizontalTableView.h"

@protocol HICascadedTableViewDelegate <NSObject>
@optional
//Customizing cells when adding them to background
- (void)tableView:(UITableView *)tableView willAddCellToBackground:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView willReloadRowsAtIndexPaths:(NSArray*)indexPaths;
@end

@protocol HICascadedTableViewDataSource <UITableViewDataSource>
@required
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath reuse:(BOOL)reuse;
@end

@interface HICascadedTableView : UITableView 

@property (nonatomic,weak) id<HICascadedTableViewDataSource> dataSource;
@property (nonatomic,weak) id<HICascadedTableViewDelegate> cascadeDelegate;
/*
 * The part of the headerView that cannot be scrolled by the cells.
 * As it is a ratio,its value can be only between 0 and 1 all other values are discarded.
 * By default this is 0,which means the entire header view is covered by the cells when we scroll upwards
 * If the tableView has no headerview then this property will be ignored
 */
@property (nonatomic,assign) CGFloat headerViewVisibilityRatio;
@property (nonatomic,readonly) NSInteger staticCellOffset;
@end

@interface HIHorizontalCascadedTableView : HICascadedTableView
@property (nonatomic,weak) id<HIHorizontalTableViewDelegate> delegate;
@end

@interface HICascadedTableViewStaticBackgroundView : UIView
@property (nonatomic,weak) UIView* headerView;
@property (nonatomic,weak) UIView* cellView;
@property (nonatomic,assign) CGFloat cellViewHeight;
@property (nonatomic,assign) CGFloat headerViewVisibilityRatio;
@property (nonatomic,readonly) CGFloat headerOffset;
@end

@interface HITransparentView : UIView
@end

@interface HITransparentTableViewCell : UITableViewCell
@end
