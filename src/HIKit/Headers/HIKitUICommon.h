//
//  HIKitUICommon.h
//  HIKit
//
//  Created by Sumeru Chatterjee on 12/23/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

// Debug-only assertions.
#ifdef DEBUG

#import <TargetConditionals.h>

// The general purpose logger. This ignores logging levels.
#ifdef DEBUG
#define DPRINT(xx, ...)  NSLog(@"%s(%d): " xx, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define DPRINT(xx, ...)  ((void)0)
#endif // #ifdef DEBUG

// Prints the current method's name.
#define DPRINTMETHODNAME() TTDPRINT(@"%s", __PRETTY_FUNCTION__)

#if TARGET_IPHONE_SIMULATOR
// We leave the __asm__ in this macro so that when a break occurs, we don't have to step out of
// a "breakInDebugger" function.
#define DASSERT(xx) { if (!(xx)) { DPRINT(@"TTDASSERT failed: %s", #xx); \
if (IsInDebugger()) { __asm__("int $3\n" : : ); }; } \
} ((void)0)
#else
#define DASSERT(xx) { if (!(xx)) { DPRINT(@"TTDASSERT failed: %s", #xx); } } ((void)0)
#endif // #if TARGET_IPHONE_SIMULATOR

#else
#define DASSERT(xx) ((void)0)
#endif // #ifdef DEBUG

#if __has_feature(objc_arc)
#define RELEASE_SAFELY(__POINTER) { __POINTER = nil; }
#else
#define RELEASE_SAFELY(__POINTER) { [__POINTER release]; __POINTER = nil; }
#endif

#define FREE_SAFELY(__POINTER) { free(__POINTER);__POINTER = nil; }

int IsInDebugger(void);
/**
 * The standard height of a row in a vertical table view controller.
 * @const 44 pixels
 */

extern const CGFloat kDefaultRowHeight;
extern const CGFloat kDefaultRowWidth;
extern const CGFloat kDefaultPortraitToolbarHeight;
extern const CGFloat kDefaultLandscapeToolbarHeight;

#define HI_ROW_HEIGHT                 kDefaultRowHeight
#define HI_TOOLBAR_HEIGHT             kDefaultPortraitToolbarHeight
#define HI_LANDSCAPE_TOOLBAR_HEIGHT   kDefaultLandscapeToolbarHeight

extern const CGFloat kDefaultPortraitToolbarHeight;
extern const CGFloat kDefaultLandscapeToolbarHeight;
extern const CGFloat kDefaultPortraitKeyboardHeight;
extern const CGFloat kDefaultLandscapeKeyboardHeight;

#define HIRGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]

#define HI_KEYBOARD_HEIGHT                 kDefaultPortraitKeyboardHeight
#define HI_LANDSCAPE_KEYBOARD_HEIGHT       kDefaultLandscapeKeyboardHeight
#define HI_IPAD_KEYBOARD_HEIGHT            kDefaultPadPortraitKeyboardHeight
#define HI_IPAD_LANDSCAPE_KEYBOARD_HEIGHT  kDefaultPadLandscapeKeyboardHeight

extern const CGFloat kDefaultRowHeight;
/**
 * The standard width of a row in a horizontal table view controller.
 * @const 44 pixels
 */
extern const CGFloat kDefaultRowWidth;
/**
 * The space between the edge of the screen and the cell edge in grouped table views.
 * @const 10 pixels for iPhone and 43 pixels for iPad
 */
CGFloat HIGroupedTableCellInset(void);
/**
 * @return the inverse rectangle of the given rectangle i.e the width and the height are interchanged
 */
CGRect HIRectInvert(CGRect rect);
/**
 * @return TRUE if the device is iPad.
 */

CGRect HIScreenBounds();

CGRect HIRectInset(CGRect rect, UIEdgeInsets insets);
BOOL HIIsPad(void);
/**
 *  Convert a C array of CGFloats into an NSString to print the description of the array
 */
NSString* NSStringFromCFloatArray(CGFloat* array,int length); 

CGFloat HIToolbarHeight();

CGFloat HIToolbarHeightForOrientation(UIInterfaceOrientation orientation);

UIInterfaceOrientation HIInterfaceOrientation();
UIInterfaceOrientation HIInterfaceOrientationInverse();

CGRect HIToolbarNavigationFrame();

BOOL HIIsSupportedOrientation(UIInterfaceOrientation orientation);

UIDeviceOrientation HIDeviceOrientation();

BOOL HIDeviceOrientationIsPortrait();

BOOL HIDeviceOrientationIsLandscape();

UIImage* HIImageFromBundle(NSString* imagePath);

CGFloat HIKeyboardHeight();

CGFloat HIKeyboardHeightForOrientation(UIInterfaceOrientation orientation);
/**
 * @return the current runtime version of the iPhone OS.
 */
float HIOSVersion();

/**
 * Checks if the run-time version of the OS is at least a certain version.
 */
BOOL HIRuntimeOSVersionIsAtLeast(float version);

BOOL HIOSVersionIsAtLeast(float version);

NSString* HIDescriptionForError(NSError* error);

