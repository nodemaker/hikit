//
//  SimpleTableViewCell.m
//  HICatalog
//
//  Created by Sumeru Chatterjee on 12/19/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import "SimpleTableViewCell.h"

//HIKit
#import <HIKit/HIKitUICommon.h>

//Additions
#import "UIView+Helpers.h"
#import "UIFont+Fit.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation SimpleTableViewCell

const CGFloat kMarkerLabelInset = 10.0f;

///////////////////////////////////////////////////////////////////////////////////////////////////
-(id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
    }   
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) layoutSubviews {

    [super layoutSubviews];

    self.markerLabel.frame = CGRectInset(self.contentView.bounds,kMarkerLabelInset,kMarkerLabelInset);
    self.markerLabel.font = [UIFont maximumBoldSystemFontThatFitsString:self.markerLabel.text
                                                                 inSize:CGSizeMake(self.boundWidth-30,self.boundHeight-30)];
}

#pragma mark -
#pragma mark Accessors

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UILabel*) markerLabel {

    if (!_markerLabel) {

        _markerLabel = [[UILabel alloc] init];
        _markerLabel.textColor = [UIColor whiteColor];
        _markerLabel.textAlignment  = UITextAlignmentCenter;
        _markerLabel.backgroundColor = [UIColor blackColor];
        _markerLabel.numberOfLines = 0;
        _markerLabel.lineBreakMode = UILineBreakModeWordWrap;
        _markerLabel.textAlignment = UITextAlignmentCenter;
        [self.contentView addSubview:_markerLabel];
    }
    return _markerLabel;
}

@end
