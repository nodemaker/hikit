//
//  SimpleHeaderView.h
//  HICatalog
//
//  Created by Sumeru Chatterjee on 1/12/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

@interface SimpleHeaderView : UIView {
    
    UILabel* textLabel;
}

@property (nonatomic,strong) UIColor* labelBackgroundColor;
@property (weak, nonatomic,readonly) UILabel* textLabel;
@end
