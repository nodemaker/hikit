//
//  SimpleHeaderView.m
//  HICatalog
//
//  Created by Sumeru Chatterjee on 1/12/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

#import "SimpleHeaderView.h"

//Additions
#import "UIView+Helpers.h"
#import "UIFont+Fit.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation SimpleHeaderView
@synthesize labelBackgroundColor = _labelBackgroundColor;

const CGFloat kHeaderMarkerLabelInset = 10.0f;

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) layoutSubviews {

    [super layoutSubviews];

    self.textLabel.backgroundColor = self.labelBackgroundColor;
    self.textLabel.frame = CGRectInset(self.bounds,kHeaderMarkerLabelInset,kHeaderMarkerLabelInset);
}

#pragma mark -
#pragma mark Accessors

///////////////////////////////////////////////////////////////////////////////////////////////////
-(UILabel*) textLabel {

    if (!textLabel) {

        textLabel = [[UILabel alloc] init];
        textLabel.textColor = [UIColor whiteColor];
        textLabel.textAlignment  = UITextAlignmentCenter;
        textLabel.font=[UIFont boldSystemFontOfSize:20];
        textLabel.text = @"HEADER";

        [self addSubview:textLabel];
    }
    return textLabel;
}
@end
