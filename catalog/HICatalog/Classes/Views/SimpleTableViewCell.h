//
//  SimpleTableViewCell.h
//  HICatalog
//
//  Created by Sumeru Chatterjee on 12/19/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

@interface SimpleTableViewCell : UITableViewCell {
    
    UIColor* _labelBackgroundColor;
    UILabel* _markerLabel;
}

@property (weak, nonatomic,readonly) UILabel* markerLabel;
@end
