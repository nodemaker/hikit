//
//  HICatalogAppDelegate.m
//  HICatalog
//
//  Created by Sumeru Chatterjee on 12/16/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import "HICatalogAppDelegate.h"
#import "CatalogTableViewController.h"

@implementation HICatalogAppDelegate

@synthesize window = _window;
@synthesize mainNavigationController = _mainNavigationController;

///////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];

    UITableViewController* catalogTableViewController =  [[CatalogTableViewController alloc] init];
    self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:catalogTableViewController];

    [self.window setRootViewController:self.mainNavigationController];
    [self.window makeKeyAndVisible];
    return YES;
}

@end
