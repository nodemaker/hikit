//
//  SimpleColoredTableViewController.m
//  HICatalog
//
//  Created by Sumeru Chatterjee on 12/19/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import "SimpleTableViewCell.h"
#import "SimpleHeaderView.h"
#import "SimpleNumberTableViewController.h"

//HIKit
#import <HIKit/HIKitUICommon.h>
#import <HIKit/HIKit+Additions.h>

//Additions
#import "UIView+Helpers.h"

#define FIXED_ROW_LENGTH_PLAIN 100

#define FIXED_ROW_LENGTH_CASCADED_IPAD 476
#define HEADER_VIEW_LENGTH_IPAD 272

#define FIXED_ROW_LENGTH_CASCADED_IPHONE 190
#define HEADER_VIEW_LENGTH_IPHONE 100

static const NSInteger heights[] = {50,75,100,150,200};

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation SimpleNumberTableViewController

@synthesize variableCellLengths = _variableCellLengths;
@synthesize cellColors = _cellColors;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        NSArray* colors = @[[UIColor orangeColor],
                [UIColor purpleColor],
                [UIColor magentaColor],
                [UIColor greenColor],
                [UIColor cyanColor],
                [UIColor darkGrayColor],
                [UIColor lightGrayColor],
                [UIColor yellowColor],
                [UIColor redColor],
                [UIColor blackColor]];


        self.cellColors = [NSMutableArray array];
        for (int index=0;index<20;index++) {
            [self.cellColors addObject:colors[arc4random()%colors.count]];
        }

        self.variableCellLengths = [NSMutableArray array];
        for (int index=0;index<10;index++) {
            [self.variableCellLengths addObject:@(heights[arc4random()%5])];
        }

        self.tableView.separatorColor = [UIColor whiteColor];

        //Set the header view
        SimpleHeaderView* headerView = [[SimpleHeaderView alloc] init];
        CGFloat headerViewLength = (HIIsPad())?HEADER_VIEW_LENGTH_IPAD:HEADER_VIEW_LENGTH_IPHONE;

        if (self.tableViewOrientation==UITableViewOrientationVertical) {
            headerView.frame = CGRectMake(0,0,self.tableView.width,headerViewLength);

        } else {
            headerView.frame = CGRectMake(0,0,headerViewLength,self.tableView.height);
        }


        [headerView setLabelBackgroundColor:colors[arc4random()%colors.count]];
        [self.tableView setTableHeaderView:headerView];
    }
    return self;
}

#pragma mark -
#pragma mark Table view data source

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    switch (tableView.scrollStyle) {
        case UITableViewScrollStylePlain:
            return 2;

        case UITableViewScrollStyleCascaded:
            return 1;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (tableView.scrollStyle) {
        case UITableViewScrollStylePlain:
            return 10;

        case UITableViewScrollStyleCascaded:
            return 10;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    switch (section) {
        case 0:
            return @"Fixed Length";

        case 1:
            return @"Variable Length";

        default:
            return nil;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    switch (indexPath.section) {
        case 0:
            switch (tableView.scrollStyle) {

                case UITableViewScrollStylePlain:
                    return FIXED_ROW_LENGTH_PLAIN;

                case UITableViewScrollStyleCascaded:
                    return (HIIsPad())?FIXED_ROW_LENGTH_CASCADED_IPAD:FIXED_ROW_LENGTH_CASCADED_IPHONE;
            }

        case 1:
            return [(self.variableCellLengths)[indexPath.row] floatValue];

        default:
            return 0.0;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)tableView:(UITableView *)tableView widthForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            switch (tableView.scrollStyle) {

                case UITableViewScrollStylePlain:
                    return FIXED_ROW_LENGTH_PLAIN;

                case UITableViewScrollStyleCascaded:
                    return (HIIsPad())?FIXED_ROW_LENGTH_CASCADED_IPAD:FIXED_ROW_LENGTH_CASCADED_IPHONE;
            }

        case 1:
            return [(self.variableCellLengths)[indexPath.row] floatValue];

        default:
            return 0.0;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath reuse:(BOOL)reuse
{
    NSString *CellIdentifier = @"Cell";
    
    SimpleTableViewCell *cell = nil;
    if (reuse){
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    } else {
        CellIdentifier = nil;
    }
    
    if (cell == nil) {
        cell = [[SimpleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.markerLabel.backgroundColor = (self.cellColors)[indexPath.row];
    cell.markerLabel.text = [NSString stringWithFormat:@"%d",10*indexPath.section + indexPath.row+1];
    return cell;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableView:tableView cellForRowAtIndexPath:indexPath reuse:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark - UIViewController

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return HIIsSupportedOrientation(interfaceOrientation);
}


@end
