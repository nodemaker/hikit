//
//  SimpleTextTableViewController.m
//  HICatalog
//
//  Created by Sumeru Chatterjee on 1/9/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

#import "SimpleTableViewCell.h"
#import "SimpleTextTableViewController.h"

#define LINKTEXTCOLOR [UIColor colorWithRed:51/255.0f green:102/255.0f blue:187/255.0f alpha:1]

//HIKit
#import <HIKit/HIKitUICommon.h>
#import <HIKit/HIWebController.h>

static const NSInteger heights[] = {50,100,200,300 };
static const char* languages[] = {"English","French","German","Russian","Spanish","Japanese","Italian","Urdu","Hindi"};
static const char* placeholders[] = {
                                     "The quick brown fox jumped over the lazy dog",
                                     "Monsieur Jack, vous dactylographiez bien mieux que votre ami Wolf",
                                     "Franz jagt im komplett verwahrlosten Taxi quer durch Bayern",
                                     "Эй, жлоб! Где туз? Прячь юных съёмщиц в шкаф",
                                     "Es extraño mojar queso en la cerveza o probar whisky de garrafa",
                                     "鳥啼く声す　夢覚ませ　見よ明け渡る　東を　空色栄えて　沖つ辺に　帆船群れゐぬ　靄の中",
                                     "Ma la volpe, col suo balzo, ha raggiunto il quieto Fidos",
                                     "ر‌ضائی کو غلط اوڑھے بیٹھی ژوب کی قرۃ العین اور عظمٰی کے پاس گھر کے ذخیرے سے آناً فاناً ڈش میں ثابت جو، صراحی میں چائے اور پلیٹ میں زردہ آیا۔",
                                     "दुष्ट राक्षसों के राजा रावण का सर्वनाश करने वाले विष्णुवतार भगवान श्रीराम,अयोध्या के महाराज दशरथ के बड़े सपुत्र थे।"};

static const char* websites[] = {"Wikipedia","Facebook","Google","Yahoo"};
static const char* websiteURLs[] = {
                                "en.wikipedia.org","www.facebook.com","www.google.com","www.yahoo.com",
                                "fr.wikipedia.org","fr-fr.facebook.com","www.google.fr","fr.yahoo.com",
                                "de.wikipedia.org","de-de.facebook.com","www.google.de","de.yahoo.com",
                                "ru.wikipedia.org","ru-ru.facebook.com","www.google.ru","ru.yahoo.com",
                                "es.wikipedia.org","es-es.facebook.com","www.google.es","es.yahoo.com",
                                "ja.wikipedia.org","ja-jp.facebook.com","www.google.co.jp","www.yahoo.co.jp",
                                "it.wikipedia.org","it-it.facebook.com","www.google.it","it.yahoo.com",
                                "ur.wikipedia.org","ar-ar.facebook.com","www.google.com/intl/ur","www.yahoo.com",
                                "hi.wikipedia.org","hi-in.facebook.com","www.google.co.in/intl/hi/","in.yahoo.com/"};

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation SimpleTextTableViewController

@synthesize language = _language;
@synthesize placeholder = _placeholder;
@synthesize textColors = _textColors;
@synthesize backgroundColors = _backgroundColors;
@synthesize websites = _websites;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithLanguage:(NSString*)language style:(UITableViewStyle)style {

    self = [super initWithStyle:style];
    if (self) {        
        
        //Select a language a get its placeholder
        NSInteger languageSelection = -1;
        for (int i=0;i<9;i++) {

            if ([[language lowercaseString] isEqualToString:[@(languages[i]) lowercaseString]]) {
                languageSelection = i;
                break;
            }
        }

        if (languageSelection<0) {
            return self;
        }

        _language = language;
        _placeholder = @(placeholders[languageSelection]);

        NSArray* backgroundColors = @[[UIColor blackColor],
                                     [UIColor blackColor],
                                     [UIColor redColor],
                                     [UIColor lightGrayColor],
                                     [UIColor darkGrayColor]];

        NSArray* textColors = @[[UIColor yellowColor],
                               [UIColor cyanColor],
                               [UIColor yellowColor],
                               [UIColor cyanColor],
                               [UIColor orangeColor]];

        _textColors = [NSMutableArray array];
        _backgroundColors = [NSMutableArray array];

        for (int i=0;i<16;i++) {
            NSInteger colorSelection = arc4random()%backgroundColors.count;
            [_textColors addObject:textColors[colorSelection]];
            [_backgroundColors addObject:backgroundColors[colorSelection]];
        }

        _websites = [NSMutableArray array];
        for (int i=0;i<4;i++) {
            [_websites addObject:@(websiteURLs[4*languageSelection+i])];
        }
        
        
        self.showsNavigationBarInCascadeNavigation = YES;
        self.title = language;
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithStyle:(UITableViewStyle)style
{
    NSString* randomLanguage = @(languages[arc4random()%9]);
    return [self initWithLanguage:randomLanguage style:style];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
-(void) viewDidLoad {
    
    [super viewDidLoad];
    self.tableView.showsVerticalScrollIndicator = NO;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Table view data source

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 16;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    return self.language;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return heights[indexPath.row%4];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";

    SimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SimpleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    if (indexPath.row%4==0) {

        cell.markerLabel.text = @(websites[(int)(indexPath.row/4)]);
        cell.markerLabel.textColor = LINKTEXTCOLOR;
        cell.markerLabel.backgroundColor = [UIColor whiteColor];

    } else {
        cell.markerLabel.text = self.placeholder;
        cell.markerLabel.backgroundColor = (self.backgroundColors)[indexPath.row];
        cell.markerLabel.textColor = (self.textColors)[indexPath.row];
    }
    return cell;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row%4==0) {

        HIWebController* webviewController = [[HIWebController alloc] init];
        [self.tableNavigationController pushViewController:webviewController fromViewController:self animated:YES];
        [webviewController openURL:[NSURL URLWithString:(self.websites)[(int)(indexPath.row/4)]]];

    } else {

        UIViewController* viewController = [[SimpleTextTableViewController alloc] initWithLanguage:self.language style:UITableViewStylePlain];
        [self.tableNavigationController pushViewController:viewController fromViewController:self animated:YES];
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark - UIViewController

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return HIIsSupportedOrientation(interfaceOrientation);
}

@end
