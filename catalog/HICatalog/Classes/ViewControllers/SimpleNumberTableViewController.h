//
//  SimpleNumberTableViewController.h
//  HICatalog
//
//  Created by Sumeru Chatterjee on 12/19/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

@interface SimpleNumberTableViewController : UITableViewController {
    
    NSMutableArray* _cellColors;
    NSMutableArray* _variableCellLengths;
}

@property (nonatomic,strong) NSMutableArray* variableCellLengths;
@property (nonatomic,strong) NSMutableArray* cellColors;
@end
