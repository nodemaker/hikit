//
//  SimpleTextTableViewController.h
//  HICatalog
//
//  Created by Sumeru Chatterjee on 1/9/12.
//  Copyright (c) 2012 Sumeru Chatterjee. All rights reserved.
//

@interface SimpleTextTableViewController : UITableViewController {
    
    NSString* _language;
    NSString* _placeholder;
    NSMutableArray* _textColors;
    NSMutableArray* _backgroundColors;
    NSMutableArray* _websites;

}

@property (nonatomic, readonly) NSString* language;
@property (nonatomic, readonly) NSString* placeholder;
@property (nonatomic, readonly) NSArray* textColors;
@property (nonatomic, readonly) NSArray* backgroundColors;
@property (nonatomic, readonly) NSArray* websites;


- (id)initWithLanguage:(NSString*)language style:(UITableViewStyle)style;
@end
