//
//  CatalogTableViewController.m
//  HICatalog
//
//  Created by Sumeru Chatterjee on 12/16/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

#import "SimpleNumberTableViewController.h"
#import "SimpleCategoriesViewController.h"
#import "SimpleTextTableViewController.h"
#import "CatalogTableViewController.h"

//HIKit

#import <HIKit/HIKitUICommon.h>
#import <HIKit/HIWebController.h>
#import <HIKit/HITableNavigationController.h>
#import <HIKit/HIAlertView.h>
#import <HIKit/HISlidingNavigationController.h>

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation CatalogTableViewController

@synthesize catalogItems = _catalogItems;
@synthesize catalogSections = _catalogSections;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        self.title = @"HIKit Catalog";
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Catalog" style:UIBarButtonItemStylePlain target:nil action:nil];

        self.catalogSections = @[@"Simple Tables",@"Cascaded Tables",@"Table Navigation",@"Miscellaneous"];

        self.catalogItems =  @[@[@"Vertical Plain",@"Vertical Grouped",@"Horizontal Plain", @"Horizontal Grouped"],

                             //Cascaded Tables
                             @[@"Vertical Plain",@"Horizontal Plain"],

                             //Table Navigation
                             @[@"Plain",@"Grouped",@"Cascaded",@"Sliding"],

                             //Miscellaneous
                             @[@"WebView",@"AlertView"]];
    }
    return self;
}

#pragma mark -
#pragma mark Table view data source

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.catalogSections.count;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(self.catalogItems)[section] count];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return (self.catalogSections)[section];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell* cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    NSString *text = (self.catalogItems)[indexPath.section][indexPath.row];

    CGSize labelSize = [text sizeWithFont:cell.textLabel.font
                        constrainedToSize:CGSizeMake(tableView.bounds.size.width-60,MAXFLOAT)
                            lineBreakMode:UILineBreakModeWordWrap];

    return labelSize.height + 20;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.textLabel.numberOfLines = 0;
        cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        [cell.textLabel sizeToFit];
    }

    cell.textLabel.text = (self.catalogItems)[indexPath.section][indexPath.row];
    return cell;
}

#pragma mark -
#pragma mark Table view Delegate

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString* sectionName = (self.catalogSections)[indexPath.section];
    NSString* itemName = (self.catalogItems)[indexPath.section][indexPath.row];

    UIViewController* exampleViewController=nil;

    //Simple Tables
    if ([sectionName isEqualToString:@"Simple Tables"]) {


        if ([itemName isEqualToString:@"Vertical Plain"]) {

            exampleViewController = [[SimpleNumberTableViewController alloc] init];

        }  else if ([itemName isEqualToString:@"Horizontal Plain"]) {

            exampleViewController = [[SimpleNumberTableViewController alloc] initWithOrientation:UITableViewOrientationHorizontal];

        } else if ([itemName isEqualToString:@"Vertical Grouped"]) {

            exampleViewController = [[SimpleNumberTableViewController alloc] initWithStyle:UITableViewStyleGrouped];

        } else if ([itemName isEqualToString:@"Horizontal Grouped"]) {

            exampleViewController = [[SimpleNumberTableViewController alloc] initWithStyle:UITableViewStyleGrouped orientation:UITableViewOrientationHorizontal];
        }

    //Cascaded Tables
    } else if ([sectionName isEqualToString:@"Cascaded Tables"]) {

        if ([itemName isEqualToString:@"Vertical Plain"]) {

            exampleViewController = [[SimpleNumberTableViewController alloc] initWithScrollStyle:UITableViewScrollStyleCascaded];

        } else if ([itemName isEqualToString:@"Horizontal Plain"]) {

            exampleViewController = [[SimpleNumberTableViewController alloc] initWithOrientation:UITableViewOrientationHorizontal scrollStyle:UITableViewScrollStyleCascaded];
        }

    } else if ([sectionName isEqualToString:@"Table Navigation"]) {

        UIViewController* categoriesViewController = [[SimpleCategoriesViewController alloc] init];

        if ([itemName isEqualToString:@"Plain"]) {

            exampleViewController =
            [[HITableNavigationController alloc] initWithHeaderViewController:categoriesViewController
                                                                        style:UITableNavigationViewStylePlain];
            
            [(HITableNavigationController*)exampleViewController setDetachEnabled:YES];
            [(HITableNavigationController*)exampleViewController setDetachImage:HIImageFromBundle(@"detachImage.png")];
            
        } else if ([itemName isEqualToString:@"Grouped"]) {

            exampleViewController =
            [[HITableNavigationController alloc] initWithHeaderViewController:categoriesViewController
                                                                        style:UITableNavigationViewStyleGrouped];
            
            [(HITableNavigationController*)exampleViewController setDetachEnabled:YES];
            [(HITableNavigationController*)exampleViewController setDetachImage:HIImageFromBundle(@"detachImage.png")];

        } else if ([itemName isEqualToString:@"Cascaded"]) {

            exampleViewController =
            [[HITableNavigationController alloc] initWithHeaderViewController:categoriesViewController
                                                                        style:UITableNavigationViewStyleCascaded];
            
            [(HITableNavigationController*)exampleViewController setDetachEnabled:YES];
            [(HITableNavigationController*)exampleViewController setDetachImage:HIImageFromBundle(@"detachImage.png")];
            
        } else if ([itemName isEqualToString:@"Sliding"]) {
            
            exampleViewController =
            [[HISlidingNavigationController alloc] initWithLeftHeaderViewController:categoriesViewController];
        }


    } else if ([sectionName isEqualToString:@"Miscellaneous"]) {

        if ([itemName isEqualToString:@"WebView"]) {

            exampleViewController = [[HIWebController alloc] init];
            [(HIWebController*)exampleViewController openURL:[NSURL URLWithString:@"http://www.google.com"]];
        } else if ([itemName isEqualToString:@"AlertView"]) {
            
        
                HIAlertView *alert = [[HIAlertView alloc]
                                      initWithTitle: @"AlertView"
                                      message: @"Plain alertview"
                                      delegate: nil];
                [alert addButtonWithTitle:@"Cancel" whitened:NO];
                [alert addButtonWithTitle:@"Next" whitened:YES];
                
                [alert setDelegate:self];
                [alert show];
                [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
    }

    if (exampleViewController) {
        [self.navigationController pushViewController:exampleViewController animated:YES];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark - HIAlertViewDelegate

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)alertView:(HIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex==1){
        if(alertView.alertViewStyle==HIAlertViewStyleDefault){
            if(alertView.numberOfTextFields==0){
                alertView.message = @"This is a fake alert view with textfields";
               
                [alertView addTextFieldWithPlaceholder:@"username" secure:NO];
                [alertView addTextFieldWithPlaceholder:@"password" secure:YES];
                [alertView reset];
                
            } else {
                
                alertView.message = @"You can show an activity too!";
                [alertView removeAllTextFields];
                alertView.alertViewStyle = HIAlertViewStyleActivity;
                [alertView reset];
            }
            return NO;
            
        } else if(alertView.alertViewStyle==HIAlertViewStyleActivity){
            
            alertView.message = @"Loading";
            alertView.alertViewStyle = HIAlertViewStyleProgress;
            [alertView reset];
            
            return NO;
            
        }else if(alertView.alertViewStyle==HIAlertViewStyleProgress){
            
            alertView.message = @"Yes success yaay!";
            alertView.alertViewStyle = HIAlertViewStyleSuccess;
            [alertView reset];
        
            return NO;
            
        }else if(alertView.alertViewStyle==HIAlertViewStyleSuccess){
            
            alertView.message = @"Oh no!";
            alertView.alertViewStyle = HIAlertViewStyleError;
            [alertView reset];
            
            return NO;
        }else if(alertView.alertViewStyle==HIAlertViewStyleError){
            
            alertView.message = @"Custom View";
            UIImageView *customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Icon@2x.png"]];            
            alertView.alertViewStyle = HIAlertViewStyleCustom;
            [alertView setCustomView:customView];
            [alertView reset];
            
            return NO;
        } else {
            return YES;
        }
    }
    return YES;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark - UIViewController

///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return HIIsSupportedOrientation(interfaceOrientation);
}

@end
