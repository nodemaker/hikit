//
//  CatalogTableViewController.h
//  HICatalog
//
//  Created by Sumeru Chatterjee on 12/16/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//


@interface CatalogTableViewController : UITableViewController {
    
    NSArray* _catalogSections;
    NSArray* _catalogItems;
}

@property (nonatomic, strong) NSArray* catalogSections;
@property (nonatomic, strong) NSArray* catalogItems;

@end
