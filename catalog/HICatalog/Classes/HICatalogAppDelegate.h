//
//  HICatalogAppDelegate.h
//  HICatalog
//
//  Created by Sumeru Chatterjee on 12/16/11.
//  Copyright (c) 2011 Sumeru Chatterjee. All rights reserved.
//

@interface HICatalogAppDelegate : UIResponder <UIApplicationDelegate> {
    
    UIWindow* _window;
    UINavigationController* mainNavigationController;
    
}

@property (nonatomic,strong) UIWindow *window;
@property (nonatomic,strong) UINavigationController* mainNavigationController;

@end
